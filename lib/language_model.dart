class LanguageModel {
  String name;
  String code;
  bool isRTL;
  String bundledFileCode;

  LanguageModel({required this.name, required this.code, required this.isRTL, required this.bundledFileCode});

  LanguageModel.fromNative(
    Map<dynamic, dynamic> language,
  )   : name = language['name'],
        code = language['code'],
        isRTL = language['isRTL'],
        bundledFileCode = language['bundledFileCode'];
}
