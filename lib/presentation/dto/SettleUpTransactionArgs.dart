import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';

class SettleUpTransactionArgs {
  final Customer customer;
  final CustomerStatus status;
  final double overallBalance;

  const SettleUpTransactionArgs({
    required this.customer,
    required this.status,
    required this.overallBalance,
  });
}
