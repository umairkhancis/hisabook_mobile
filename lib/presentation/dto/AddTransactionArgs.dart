import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';

class AddTransactionArgs {
  final Customer? customer;
  final Transaction? transaction;
  final TransactionType type;

  AddTransactionArgs({this.customer, this.transaction, required this.type});
}
