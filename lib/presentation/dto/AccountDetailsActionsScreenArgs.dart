import 'package:splitwise/domain/entity/customer.dart';

class CustomerDetailsActionsScreenArgs {
  final Customer customer;

  CustomerDetailsActionsScreenArgs({required this.customer});
}