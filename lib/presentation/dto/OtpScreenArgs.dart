class OtpScreenArgs {
  final String fullName;
  final String phoneNumber;

  OtpScreenArgs({required this.fullName, required this.phoneNumber});
}