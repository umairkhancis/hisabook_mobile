import 'package:contacts_service/contacts_service.dart';

class VerifyContactInfoScreenArgs {
  final Contact contact;

  VerifyContactInfoScreenArgs({required this.contact});
}
