class TransactionDetailsScreenArgs {
  final int id;

  TransactionDetailsScreenArgs({required this.id});
}
