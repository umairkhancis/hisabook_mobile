import 'package:splitwise/domain/entity/customer.dart';

class CustomerDetailsScreenArgs {
  Customer customer;

  CustomerDetailsScreenArgs({required this.customer});
}
