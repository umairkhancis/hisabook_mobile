import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/dto/settle_up_report.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';
import 'package:splitwise/domain/usecase/settle_up_report_usecase.dart';

part 'settle_up_transaction_state.dart';

class SettleUpTransactionCubit extends Cubit<SettleUpTransactionState> {
  final SettleUpReportUseCase settleUpReportUseCase;
  final LedgerRepository repository;

  SettleUpTransactionCubit({
    required this.settleUpReportUseCase,
    required this.repository,
  }) : super(SettleUpTransactionInitial());

  void onForeground(Customer customer) async {
    final SettleUpReport settleUpReport =
        await settleUpReportUseCase.getSettleUpReport(customer);

    emit(SettleUpReportLoaded(settleUpReport: settleUpReport));
  }

  Future<void> onSubmitButtonPressed(
    String accountHolderName,
    String description,
    String amount,
    TransactionType type,
    TransactionStatus status,
    DateTime date,
    File? image,
  ) async {
    await repository.addTransaction(
      accountHolderName,
      amount,
      type,
      status,
      description,
      date,
      image,
    );
    emit(SettleUpTransactionAdded());
  }
}
