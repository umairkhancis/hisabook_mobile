part of 'settle_up_transaction_cubit.dart';

abstract class SettleUpTransactionState extends Equatable {
  const SettleUpTransactionState();
}

class SettleUpTransactionInitial extends SettleUpTransactionState {
  @override
  List<Object> get props => [];
}

class SettleUpReportLoading extends SettleUpTransactionState {
  @override
  List<Object> get props => [];
}

class SettleUpReportLoaded extends SettleUpTransactionState {
  final SettleUpReport settleUpReport;

  const SettleUpReportLoaded({required this.settleUpReport});
  @override
  List<Object> get props => [settleUpReport];
}

class SettleUpTransactionAdded extends SettleUpTransactionState {
  @override
  List<Object> get props => [];
}
