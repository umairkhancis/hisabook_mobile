import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/dto/activity_item_dto.dart';
import 'package:splitwise/domain/usecase/activity_usecase.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';

part 'activity_state.dart';

class ActivityCubit extends Cubit<ActivityState> {
  final ActivityUseCase activityUseCase;

  ActivityCubit({required this.activityUseCase}) : super(ActivityInitial());

  void onForeground() async {
    final List<ActivityItemDto> activityList =
        await activityUseCase.getActivityItems();

    if (activityList.isEmpty) {
      LogManager.info("activityList is Empty");
      emit(ActivityIsEmpty());
    }

    emit(ActivityLoaded(activityList: activityList));
  }
}
