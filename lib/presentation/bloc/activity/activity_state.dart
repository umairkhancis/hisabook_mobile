part of 'activity_cubit.dart';

abstract class ActivityState extends Equatable {
  const ActivityState();
}

class ActivityInitial extends ActivityState {
  @override
  List<Object> get props => [];
}

class ActivityLoaded extends ActivityState {

  final List<ActivityItemDto> activityList;
  const ActivityLoaded({required this.activityList});

  @override
  List<Object> get props => [activityList];
}

class ActivityIsEmpty extends ActivityState {
  @override
  List<Object> get props => [];
}
