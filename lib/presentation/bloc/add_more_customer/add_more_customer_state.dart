part of 'add_more_customer_cubit.dart';

abstract class AddMoreCustomerState extends Equatable {}

class AddMoreCustomerInitial extends AddMoreCustomerState {
  @override
  List<Object?> get props => [];
}

class NewContactSelected extends AddMoreCustomerState {
  final Contact selectedContact;

  NewContactSelected(this.selectedContact);

  @override
  List<Object?> get props => [selectedContact];
}

class NewContactAdded extends AddMoreCustomerState {
  final Contact selectedContact;

  NewContactAdded(this.selectedContact);

  @override
  List<Object?> get props => [selectedContact];
}