import 'package:bloc/bloc.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

part 'add_more_customer_state.dart';

class AddMoreCustomerCubit extends Cubit<AddMoreCustomerState> {
  final LedgerRepository repository;

  AddMoreCustomerCubit({required this.repository})
      : super(AddMoreCustomerInitial());

  Future<List<Contact>> onSearch(String pattern) async {
    final List<Contact> contacts = await repository.getPhoneContacts();
    // return contacts.where((contact) => contact.name.contains(pattern)).toList();
    return contacts
        .where((contact) =>
            contact.displayName!.toLowerCase().contains(pattern.toLowerCase()))
        .toList();
  }

  void onContactSelected(Contact selectedContact) {
    // repository.addContact(selectedContact);

    emit(NewContactSelected(selectedContact));
  }

  Future<void> addContactAsCustomer(Contact selectedContact) async {
    await repository.addContact(selectedContact);
    emit(NewContactAdded(selectedContact));
  }
}
