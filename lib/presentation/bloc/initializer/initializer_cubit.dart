import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';

part 'initializer_state.dart';

class InitializerCubit extends Cubit<InitializerState> {
  final LedgerRepository repository;
  InitializerCubit({required this.repository}) : super(InitializerInitial());

  Future checkUser(String phoneNumber) async {
    LogManager.info("checkUser::phoneNumber = $phoneNumber");
    final bool userExists = await repository.doesUserExist(phoneNumber);
    LogManager.info("checkUser::userExists = $userExists");
    if (userExists) {
      emit(UserExistsInInitializer());
    } else {
      emit(UserDoesNotExistInInitializer());
    }
  }
}
