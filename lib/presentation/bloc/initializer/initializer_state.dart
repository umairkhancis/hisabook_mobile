part of 'initializer_cubit.dart';

abstract class InitializerState extends Equatable {
  const InitializerState();
}

class InitializerInitial extends InitializerState {
  @override
  List<Object> get props => [];
}

class UserExistsInInitializer extends InitializerState {
  @override
  List<Object?> get props => [];
}

class UserDoesNotExistInInitializer extends InitializerState {
  @override
  List<Object?> get props => [];
}
