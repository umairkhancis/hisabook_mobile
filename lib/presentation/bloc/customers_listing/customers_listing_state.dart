part of 'customers_listing_cubit.dart';

abstract class CustomersListingState extends Equatable {
  const CustomersListingState();
}

class AccountsListingInitial extends CustomersListingState {
  @override
  List<Object> get props => [];
}

class AccountsListingLoaded extends CustomersListingState {
  final AccountsBalanceReport report;

  const AccountsListingLoaded(this.report);

  @override
  List<Object> get props => [report];
}

class AccountsListingError extends CustomersListingState {
  @override
  List<Object> get props => [];
}
