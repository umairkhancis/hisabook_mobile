import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/dto/accounts_balance_report.dart';
import 'package:splitwise/domain/usecase/customers_listing_report_usecase.dart';

part 'customers_listing_state.dart';

class CustomersListingCubit extends Cubit<CustomersListingState> {
  final CustomersListingReportUseCase customersListingReportUseCase;

  CustomersListingCubit({required this.customersListingReportUseCase})
      : super(AccountsListingInitial());

  Future onForeground(String customerName) async {
    AccountsBalanceReport report =
        await customersListingReportUseCase.getBalanceReport(customerName);
    emit(AccountsListingLoaded(report));
  }

  void error() {
    emit(AccountsListingError());
  }
}
