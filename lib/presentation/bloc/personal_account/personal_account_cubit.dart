import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/dto/personal_account_report_dto.dart';
import 'package:splitwise/domain/usecase/personal_account_report_usecase.dart';

part 'personal_account_state.dart';

class PersonalAccountCubit extends Cubit<PersonalAccountState> {
  final PersonalAccountReportUseCase personalAccountReportUseCase;

  PersonalAccountCubit({required this.personalAccountReportUseCase})
      : super(PersonalAccountInitial());

  void onForeground() async {
    final PersonalAccountReportDto personalAccount =
        await personalAccountReportUseCase.getPersonalAccountDetailsReport();

    emit(PersonalAccountDetailsLoaded(personalAccount: personalAccount));
  }
}
