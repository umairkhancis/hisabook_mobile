part of 'personal_account_cubit.dart';

abstract class PersonalAccountState extends Equatable {
  const PersonalAccountState();
}

class PersonalAccountInitial extends PersonalAccountState {
  @override
  List<Object> get props => [];
}

class PersonalAccountDetailsLoaded extends PersonalAccountState {

  final PersonalAccountReportDto personalAccount;
  const PersonalAccountDetailsLoaded({required this.personalAccount});

  @override
  List<Object> get props => [personalAccount];

}
