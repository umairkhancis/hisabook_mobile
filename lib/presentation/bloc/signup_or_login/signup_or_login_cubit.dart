import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

part 'signup_or_login_state.dart';

class SignupOrLoginCubit extends Cubit<SignupOrLoginState> {
  final LedgerRepository repository;

  SignupOrLoginCubit({required this.repository}) : super(SignupOrLoginInitial());

  Future onSubmitButtonPressed(String fullName, String accountName, String phoneNumber) async {
    await repository.loginOrSignupUser(fullName, accountName, phoneNumber);
    emit(SignupOrLoginSuccess());
  }
}
