part of 'signup_or_login_cubit.dart';

abstract class SignupOrLoginState extends Equatable {
  const SignupOrLoginState();
}

class SignupOrLoginInitial extends SignupOrLoginState {
  @override
  List<Object> get props => [];
}

class SignupOrLoginSuccess extends SignupOrLoginState {
  @override
  List<Object> get props => [];
}
