part of 'customer_details_cubit.dart';

abstract class CustomerDetailsState extends Equatable {
  const CustomerDetailsState();
}

class AccountDetailsInitial extends CustomerDetailsState {
  @override
  List<Object> get props => [];
}

class AccountDetailsLoaded extends CustomerDetailsState {
  final AccountDetailsReport accountDetailsReport;

  const AccountDetailsLoaded({required this.accountDetailsReport});

  @override
  List<Object?> get props => [accountDetailsReport];
}

class AccountDetailsError extends CustomerDetailsState {
  const AccountDetailsError();

  @override
  List<Object?> get props => [];
}
