import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/dto/account_details_report_dto.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/usecase/customer_details_report_usecase.dart';

part 'customer_details_state.dart';

class CustomerDetailsCubit extends Cubit<CustomerDetailsState> {
  final AccountDetailsReportUseCase accountDetailsReportUseCase;

  CustomerDetailsCubit({required this.accountDetailsReportUseCase})
      : super(AccountDetailsInitial());

  void onForeground(Customer customer) async {
    final AccountDetailsReport accountDetailsReport =
        await accountDetailsReportUseCase.getCustomerDetailsReport(customer);

    emit(AccountDetailsLoaded(accountDetailsReport: accountDetailsReport));
  }
}
