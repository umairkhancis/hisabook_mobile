part of 'phone_number_cubit.dart';

abstract class PhoneNumberState extends Equatable {
  const PhoneNumberState();
}

class PhoneNumberInitial extends PhoneNumberState {
  @override
  List<Object> get props => [];
}

class UserExists extends PhoneNumberState {
  @override
  List<Object?> get props => [];
}

class UserDoesNotExist extends PhoneNumberState {
  @override
  List<Object?> get props => [];
}
