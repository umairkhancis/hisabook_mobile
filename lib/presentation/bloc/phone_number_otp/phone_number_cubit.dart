import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';

part 'phone_number_state.dart';

class PhoneNumberCubit extends Cubit<PhoneNumberState> {
  final LedgerRepository repository;
  PhoneNumberCubit({required this.repository}) : super(PhoneNumberInitial());

  Future checkUser(String phoneNumber) async {
    LogManager.info("checkUser::phoneNumber = $phoneNumber");
    final bool userExists = await repository.doesUserExist(phoneNumber);
    LogManager.info("checkUser::userExists = $userExists");
    if (userExists) {
      emit(UserExists());
    } else {
      emit(UserDoesNotExist());
    }
  }
}
