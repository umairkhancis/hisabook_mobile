part of 'transaction_details_cubit.dart';

@immutable
abstract class TransactionDetailsState extends Equatable {}

class TransactionDetailsInitial extends TransactionDetailsState {
  @override
  List<Object> get props => [];
}

class TransactionDetailsLoaded extends TransactionDetailsState {
  final TransactionDetailsReport transactionDetailsReport;

  TransactionDetailsLoaded({required this.transactionDetailsReport});

  @override
  List<Object> get props => [transactionDetailsReport];
}

class TransactionDeletedSuccessfully extends TransactionDetailsState {
  @override
  List<Object> get props => [];
}
