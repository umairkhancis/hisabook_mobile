import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:splitwise/domain/dto/transaction_details_report_dto.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';
import 'package:splitwise/domain/usecase/transaction_details_report_usecase.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';

part 'transaction_details_state.dart';

class TransactionDetailsCubit extends Cubit<TransactionDetailsState> {
  final TransactionDetailsReportUseCase transactionDetailsReportUseCase;
  final LedgerRepository repository;

  TransactionDetailsCubit({
    required this.transactionDetailsReportUseCase,
    required this.repository,
  }) : super(TransactionDetailsInitial());

  void onForeground(int id) async {
    LogManager.info('transactionDetails::onForground');
    final TransactionDetailsReport transactionDetailsReport =
        await transactionDetailsReportUseCase.getTransactionDetailsReport(id);

    emit(
      TransactionDetailsLoaded(
        transactionDetailsReport: transactionDetailsReport,
      ),
    );
  }

  Future<void> onDeleteButtonPressed(int id, DateTime dateTime) async {
    await repository.deleteTransaction(id, dateTime);
    emit(TransactionDeletedSuccessfully());
  }
}
