import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

part 'account_details_actions_state.dart';

class CustomerDetailsActionsCubit extends Cubit<CustomerDetailsActionsState> {
  final LedgerRepository repository;

  CustomerDetailsActionsCubit(this.repository)
      : super(AccountDetailsActionsInitial());

  void onForeground() {
    emit(UserIsLoaded());
  }

  Future<void> deleteAccount(int accountId) async {
    await repository.deleteAccount(accountId);

    emit(CustomerIsDeleted());
  }
}
