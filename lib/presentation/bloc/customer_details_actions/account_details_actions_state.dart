part of 'account_details_actions_cubit.dart';

abstract class CustomerDetailsActionsState extends Equatable {
  const CustomerDetailsActionsState();
}

class AccountDetailsActionsInitial extends CustomerDetailsActionsState {
  @override
  List<Object> get props => [];
}

class UserIsLoaded extends CustomerDetailsActionsState {
  @override
  List<Object> get props => [];
}

class CustomerIsDeleted extends CustomerDetailsActionsState {
  @override
  List<Object> get props => [];
}
