part of 'add_transaction_cubit.dart';

abstract class AddTransactionState extends Equatable {}

class AddTransactionInitial extends AddTransactionState {
  AddTransactionInitial();

  @override
  List<Object> get props => [];
}

class TransactionAddedSuccessfuly extends AddTransactionState {
  TransactionAddedSuccessfuly();

  @override
  List<Object> get props => [];
}

class TransactionUpdatedSuccessfuly extends AddTransactionState {
  TransactionUpdatedSuccessfuly();

  @override
  List<Object> get props => [];
}

class TransactionUserNotSelected extends AddTransactionState {
  TransactionUserNotSelected();

  @override
  List<Object> get props => [];
}

class TransactionUserSelected extends AddTransactionState {
  final Customer customer;

  TransactionUserSelected({required this.customer});

  @override
  List<Object> get props => [customer];
}

class TransactionIsAdding extends AddTransactionState {
  TransactionIsAdding();

  @override
  List<Object> get props => [];
}

class TransactionIsUpdating extends AddTransactionState {
  TransactionIsUpdating();

  @override
  List<Object> get props => [];
}
