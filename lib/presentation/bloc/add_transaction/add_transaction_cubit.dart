import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

part 'add_transaction_state.dart';

class AddTransactionCubit extends Cubit<AddTransactionState> {
  final LedgerRepository repository;

  AddTransactionCubit({required this.repository})
      : super(AddTransactionInitial());

  void onInitialState(String currentUserName) {}

  Future onTransactionSubmitButtonPressed(
    String name,
    String description,
    String amount,
    TransactionType type,
    TransactionStatus status,
    DateTime date,
    File? image,
  ) async {
    await repository.addTransaction(
      name,
      amount,
      type,
      status,
      description,
      date,
      image,
    );
    emit(TransactionAddedSuccessfuly());
  }

  Future onTransactionUpdateButtonPressed(
    int id,
    String name,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  ) async {
    status = TransactionStatus.UPDATED;
    await repository.updateTransaction(
      id,
      name,
      amount,
      type,
      status,
      description,
      date,
      image,
    );
    emit(TransactionUpdatedSuccessfuly());
  }

  Future<List<Customer>> onSearch(String pattern) async {
    final customers = await repository.getCustomers();
    return customers.where((user) => user.customerName.contains(pattern)).toList();
  }

  void onCustomerSelected(Customer customer) {
    emit(TransactionUserSelected(customer: customer));
  }

  void selectUser() {
    emit(TransactionUserNotSelected());
  }

  void addTransaction() {
    emit(TransactionIsAdding());
  }

  void updateTransaction() {
    emit(TransactionIsUpdating());
  }
}
