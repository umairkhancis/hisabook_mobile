import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

part 'request_otp_state.dart';

class RequestOtpCubit extends Cubit<RequestOtpState> {
  final LedgerRepository repository;
  RequestOtpCubit({required this.repository}) : super(RequestOtpInitial());

  Future onSendOTPButtonPressed(String phoneNumber) async {
    await repository.sendPhoneNumberRequest(phoneNumber);
  }
}
