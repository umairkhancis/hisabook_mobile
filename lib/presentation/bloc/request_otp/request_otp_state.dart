part of 'request_otp_cubit.dart';

abstract class RequestOtpState extends Equatable {
  const RequestOtpState();
}

class RequestOtpInitial extends RequestOtpState {
  @override
  List<Object> get props => [];
}
