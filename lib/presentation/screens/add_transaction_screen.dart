import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:image_picker/image_picker.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:splitwise/presentation/bloc/add_transaction/add_transaction_cubit.dart';
import 'package:splitwise/presentation/widgets/add_amount_form.dart';
import 'package:splitwise/presentation/widgets/add_description_form.dart';

class AddTransaction extends StatefulWidget {
  final Customer? customer;
  final Transaction? transaction;
  final TransactionType type;

  const AddTransaction({
    Key? key,
    this.customer,
    this.transaction,
    required this.type,
  }) : super(key: key);

  @override
  _AddTransactionState createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {
  late AddTransactionCubit cubit;

  late String _accountName;
  late String _description;
  late String _amount;
  late TransactionType _type;
  late TransactionStatus _status;
  late int? _id;
  DateTime _dateTime = DateTime.now();

  final amountController = TextEditingController();
  final descriptionController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  File? _image;
  final imagePicker = ImagePicker();

  @override
  void initState() {
    super.initState();
    cubit = BlocProvider.of<AddTransactionCubit>(context);

    _id = widget.transaction?.id;
    _accountName = widget.customer?.customerName ?? "";
    _description = widget.transaction?.description ?? "";
    _amount = widget.transaction?.amount.toString() ?? "";
    _type = widget.type;
    _status = _mapTransactionStatusByType(_type);
    _image = widget.transaction?.image;

    if (_id == null) {
      cubit.addTransaction();
    } else {
      cubit.updateTransaction();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AddTransactionCubit, AddTransactionState>(
      builder: _onStateBuilder,
      listener: _onStateListener,
    );
  }

  Widget _onStateBuilder(BuildContext context, AddTransactionState state) {
    if (state is TransactionUserSelected) {
      _accountName = state.customer.customerName;
      _buildAccountNameWidget(context, _accountName);
      cubit.addTransaction();
    }

    if (state is TransactionUserNotSelected) {
      _accountName = "";
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          _type == TransactionType.DEBIT ? "I gave" : "I got",
          style: Theme.of(context).textTheme.headline5,
        ),
        leading: IconButton(
          onPressed: () => _goBack(context),
          icon: const Icon(Icons.arrow_back),
          color: Colors.black,
        ),
      ),
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            flex: 5,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: (_accountName.isEmpty)
                            ? _buildAccountNameForm(context)
                            : _buildAccountNameWidget(context, _accountName),
                      ),
                      const Divider(color: Colors.grey, height: 8),
                      const SizedBox(height: 24),
                      Form(
                        key: formKey,
                        child: Column(
                          children: [
                            AddAmountForm(
                              amountController: amountController,
                              amountInitialValue: _amount,
                              onAmountChanged: (amount) => _amount = amount,
                              validator: (text) {
                                if (text == null) {
                                  return "Enter an amount";
                                } else if (text.isEmpty) {
                                  return "Enter an amount";
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (text) => _amount = text ?? "",
                            ),
                            const SizedBox(height: 24),
                            AddDescriptionForm(
                              descriptionController: descriptionController,
                              descriptionInitialValue: _description,
                              onDescriptionChanged: (text) =>
                                  _description = text,
                              /*validator: (text) {
                                if (text == null) {
                                  return "Enter a description";
                                } else if (text.isEmpty) {
                                  return "Enter a description";
                                } else {
                                  return null;
                                }
                              },*/
                              onSaved: (text) => _description = text ?? "",
                            ),
                            const SizedBox(height: 24),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 36.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 50,
                                    height: 50,
                                    child: IconButton(
                                      icon: const Icon(
                                        Icons.calendar_today_outlined,
                                        color: Colors.green,
                                      ),
                                      iconSize: 24,
                                      onPressed: () => _selectDate(context),
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black26),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(4),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 50,
                                    height: 50,
                                    child: IconButton(
                                      icon: const Icon(
                                        Icons.camera_alt_outlined,
                                        color: Colors.deepPurple,
                                      ),
                                      iconSize: 24,
                                      onPressed: getImage,
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black26),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(4),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: MaterialButton(
              onPressed: () async {
                final bool isValid = formKey.currentState?.validate() ?? false;
                FocusScope.of(context).unfocus();
                if (isValid) {
                  formKey.currentState?.save();
                }
                if (state is TransactionIsAdding) {
                  print("state is TransactionIsAdding");
                  await cubit.onTransactionSubmitButtonPressed(
                    _accountName,
                    _description,
                    _amount,
                    _type,
                    _status,
                    _dateTime,
                    _image,
                  );
                } else if (state is TransactionIsUpdating) {
                  print("state is TransactionIsUpdating");
                  await cubit.onTransactionUpdateButtonPressed(
                    _id!,
                    _accountName,
                    _amount,
                    _type,
                    _status,
                    _description,
                    _dateTime,
                    _image,
                  );
                }
              },
              height: 50,
              minWidth: MediaQuery.of(context).size.width - 80,
              color: Colors.red[400],
              child: const Text(
                "SAVE",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 4,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  TransactionStatus _mapTransactionStatusByType(TransactionType type) {
    TransactionStatus status;
    if (type == TransactionType.DEBIT) {
      status = TransactionStatus.DEBT;
    } else if (type == TransactionType.CREDIT) {
      status = TransactionStatus.SURPLUS;
    } else {
      status = TransactionStatus.SETTLED;
    }
    return status;
  }

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1950),
      lastDate: DateTime.now(),
      helpText: "Add Transaction Date",
      confirmText: "OK",
      cancelText: "CANCEL",
    );
    _dateTime = selected ?? DateTime.now();
  }

  convertToString(dynamic enumItem) => enumItem.toString().split('.')[1];

  Widget _buildAccountNameForm(BuildContext context) {
    return SizedBox(
      width: 350,
      height: 50,
      child: TypeAheadField(
        textFieldConfiguration: TextFieldConfiguration(
          autofocus: false,
          autocorrect: true,
          keyboardType: TextInputType.text,
          enableSuggestions: false,
          onChanged: (text) {
            _accountName = text;
          },
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Type the name of your customer",
            hintStyle: Theme.of(context).textTheme.caption,
          ),
        ),
        itemBuilder: (BuildContext context, Customer customer) {
          return ListTile(
            title: Text(customer.customerName),
            subtitle: Text(customer.phoneNumber),
          );
        },
        onSuggestionSelected: (Customer selectedCustomer) {
          print("selectedUser: ${selectedCustomer.customerName}");
          _accountName = selectedCustomer.customerName;
          cubit.onCustomerSelected(selectedCustomer);
        },
        suggestionsCallback: (String pattern) async {
          return await cubit.onSearch(pattern);
        },
      ),
    );
  }

  Text _buildDebitText() {
    return const Text(
      "to ",
      textScaleFactor: 1.1,
      style: TextStyle(color: Colors.black87),
    );
  }

  Text _buildCreditText() {
    return const Text(
      "from ",
      textScaleFactor: 1.1,
      style: TextStyle(color: Colors.black87),
    );
  }

  Widget _buildAccountNameWidget(BuildContext context, String accountName) {
    return Row(
      children: [
        _type == TransactionType.DEBIT
            ? _buildDebitText()
            : _buildCreditText(),
        InkWell(
          onTap: () {
            cubit.selectUser();
            // _buildAccountNameForm(context);
          },
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(32)),
            ),
            child: Row(
              children: [
                _buildAccountAvatar(),
                const SizedBox(width: 12.0),
                _buildAccountNameText(context, accountName),
                const SizedBox(width: 12.0),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Text _buildAccountNameText(BuildContext context, String accountName) {
    return Text(
      accountName,
      style: Theme.of(context).textTheme.bodyText1,
    );
  }

  Container _buildAccountAvatar() {
    return Container(
      width: 32,
      height: 32,
      decoration:
          const BoxDecoration(shape: BoxShape.circle, color: Colors.green),
    );
  }

  Future getImage() async {
    final image = await imagePicker.pickImage(source: ImageSource.camera);
    _image = File(image?.path ?? "");
    LogManager.info("_image = $_image");
  }

  void _onStateListener(BuildContext context, AddTransactionState state) {
    if (state is TransactionAddedSuccessfuly) {
      _goBack(context);
    } else if (state is TransactionUpdatedSuccessfuly) {
      _goBack(context);
    }
  }

  void _goBack(BuildContext context) => Navigator.pop(context);
}
