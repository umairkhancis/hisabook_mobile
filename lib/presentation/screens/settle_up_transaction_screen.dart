import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/presentation/bloc/settle_up_transaction/settle_up_transaction_cubit.dart';
import 'package:splitwise/presentation/widgets/add_amount_form.dart';
import 'package:splitwise/routes.dart';

class SettleUpTransactionScreen extends StatefulWidget {
  final Customer customer;
  final CustomerStatus status; // put AccountStatus in Account entity
  final double accountBalance;

  const SettleUpTransactionScreen({
    Key? key,
    required this.customer,
    required this.status,
    required this.accountBalance,
  }) : super(key: key);

  @override
  _SettleUpTransactionScreenState createState() =>
      _SettleUpTransactionScreenState();
}

class _SettleUpTransactionScreenState extends State<SettleUpTransactionScreen> {
  late SettleUpTransactionCubit cubit;
  late String _accountName;
  late String _description;
  late String _amount;
  late TransactionType _type;
  final TransactionStatus _status = TransactionStatus.SETTLED;

  final amountController = TextEditingController();

  // late int? _id;

  final DateTime _dateTime = DateTime.now();

  final formKey = GlobalKey<FormState>();

  File? _image;

  @override
  void initState() {
    super.initState();
    cubit = BlocProvider.of<SettleUpTransactionCubit>(context);
    cubit.onForeground(widget.customer);
    // transaction = cubit.getTransactionById(widget.id);
    _accountName = widget.customer.customerName;
    _amount = widget.accountBalance.toString();
    _description = "settlement";
    _type = (widget.status == CustomerStatus.SURPLUS)
        ? TransactionType.DEBIT
        : TransactionType.CREDIT;
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SettleUpTransactionCubit, SettleUpTransactionState>(
      builder: _onStateBuilder,
      listener: _onStateListener,
    );
  }

  Widget _onStateBuilder(BuildContext context, SettleUpTransactionState state) {
    if (state is SettleUpReportLoaded) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            "Record Transaction",
            style: Theme.of(context).textTheme.headline6,
          ),
          actions: [
            IconButton(
              onPressed: () async {
                final bool isValid = formKey.currentState?.validate() ?? false;
                FocusScope.of(context).unfocus();
                if (isValid) {
                  formKey.currentState?.save();
                }
                await cubit.onSubmitButtonPressed(
                  _accountName,
                  _description,
                  _amount,
                  _type,
                  _status,
                  _dateTime,
                  _image,
                );
              },
              icon: const Icon(Icons.done, color: Colors.black),
            )
          ],
        ),
        body: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircleAvatar(backgroundColor: Colors.green, radius: 30),
                  SizedBox(width: 16),
                  Icon(Icons.east, size: 40),
                  SizedBox(width: 16),
                  CircleAvatar(radius: 30),
                ],
              ),
              const SizedBox(height: 30),
              Text(
                state.settleUpReport.info,
                style: Theme.of(context).textTheme.subtitle1,
              ),
              const SizedBox(height: 30.0),
              AddAmountForm(
                amountController: amountController,
                amountInitialValue: state.settleUpReport.amount,
                onAmountChanged: (amount) {
                  _amount = amount;
                },
                validator: (text) {
                  if (text == null) {
                    return "Enter an amount";
                  } else if (text.isEmpty) {
                    return "Enter an amount";
                  } else {
                    return null;
                  }
                },
                onSaved: (text) {
                  _amount = text ?? "";
                },
              )
            ],
          ),
        ),
      );
    } else {
      return const Scaffold(body: Center(child: CircularProgressIndicator()));
    }
  }

  void _onStateListener(
    BuildContext context,
    SettleUpTransactionState state,
  ) {
    if (state is SettleUpTransactionAdded) {
      Navigator.pushNamed(context, MyRoutes.homePageRoute);
    }
  }
}
