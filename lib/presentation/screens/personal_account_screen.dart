import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/presentation/bloc/personal_account/personal_account_cubit.dart';
import 'package:url_launcher/url_launcher.dart';

class PersonalAccountScreen extends StatefulWidget {
  const PersonalAccountScreen({Key? key}) : super(key: key);

  @override
  _PersonalAccountScreenState createState() => _PersonalAccountScreenState();
}

class _PersonalAccountScreenState extends State<PersonalAccountScreen> {
  late PersonalAccountCubit personalAccountCubit;

  @override
  void initState() {
    super.initState();
    personalAccountCubit = BlocProvider.of<PersonalAccountCubit>(context);
    personalAccountCubit.onForeground();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PersonalAccountCubit, PersonalAccountState>(
      builder: _onStateBuilder,
      listener: _onStateListener,
    );
  }

  Widget _onStateBuilder(BuildContext context, PersonalAccountState state) {
    if (state is PersonalAccountDetailsLoaded) {
      return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.store_outlined,
                              color: Colors.green.withOpacity(0.9),
                              size: 32.0,
                            ),
                            /*Container(
                              width: 70,
                              height: 70,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.green,
                              ),
                            ),*/
                            const SizedBox(width: 28.0),
                            Text(
                              state.personalAccount.accountName,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            /*const SizedBox(height: 4.0),
                                Text(
                                  state.personalAccount.userName,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                const SizedBox(height: 4.0),
                                Text(
                                  state.personalAccount.phoneNumber,
                                  style: Theme.of(context).textTheme.caption,
                                ),*/
                          ],
                        ),
                        const SizedBox(height: 24.0),
                        Row(
                          children: [
                            Icon(
                              Icons.person_outlined,
                              color: Colors.green.withOpacity(0.9),
                              size: 32.0,
                            ),
                            const SizedBox(width: 28.0),
                            Text(
                              state.personalAccount.userName,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ],
                        ),
                        const SizedBox(height: 24.0),
                        Row(
                          children: [
                            Icon(
                              Icons.smartphone_outlined,
                              color: Colors.green.withOpacity(0.9),
                              size: 32.0,
                            ),
                            const SizedBox(width: 28.0),
                            Text(
                              state.personalAccount.phoneNumber,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const Divider(height: 20.0),
                  const SizedBox(height: 8.0),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Feedback",
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                        const SizedBox(height: 24.0),
                        Row(
                          children: [
                            const Icon(Icons.star_rate, size: 28),
                            const SizedBox(width: 28),
                            Text(
                              "Rate Hisabook",
                              style: Theme.of(context).textTheme.subtitle1,
                            )
                          ],
                        ),
                        const SizedBox(height: 24.0),
                        InkWell(
                          onTap: () => launchEmail(
                            toEmail: "support@hisabook.com",
                            subject: "Hisabook for Android",
                            message: "Thank you for reaching out to Hisabook.",
                          ),
                          child: Row(
                            children: [
                              const Icon(
                                Icons.contact_support_outlined,
                                size: 28,
                              ),
                              const SizedBox(width: 28),
                              Text(
                                "Contact Hisabook Support",
                                style: Theme.of(context).textTheme.subtitle1,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  const Divider(height: 20.0),
                  const SizedBox(height: 16),
                ],
              ),
              Text(
                "Copyright © 2022 Hisabook",
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          ),
        ),
      );
    } else {
      return const Scaffold(body: Center(child: CircularProgressIndicator()));
    }
  }

  Future launchEmail({
    required String toEmail,
    required String subject,
    required message,
  }) async {
    final url =
        'mailto:$toEmail?subject=${Uri.encodeFull(subject)}&body=${Uri.encodeFull(message)}';

    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  void _onStateListener(BuildContext context, PersonalAccountState state) {}
}
