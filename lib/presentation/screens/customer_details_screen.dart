import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:share/share.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/presentation/bloc/customer_details/customer_details_cubit.dart';
import 'package:splitwise/presentation/dto/AccountDetailsActionsScreenArgs.dart';
import 'package:splitwise/presentation/dto/AddTransactionArgs.dart';
import 'package:splitwise/presentation/dto/SettleUpTransactionArgs.dart';
import 'package:splitwise/presentation/widgets/RouteAwareness.dart';
import 'package:splitwise/presentation/widgets/profile_picture.dart';
import 'package:splitwise/presentation/widgets/transactions_listing.dart';
import 'package:splitwise/routes.dart';

class CustomerDetailsScreen extends StatefulWidget {
  final Customer customer;

  const CustomerDetailsScreen({
    Key? key,
    required this.customer,
  }) : super(key: key);

  @override
  State<CustomerDetailsScreen> createState() => _CustomerDetailsScreenState();
}

class _CustomerDetailsScreenState extends State<CustomerDetailsScreen> {
  late CustomerDetailsCubit customerDetailsCubit;
  late final Customer customer;

  @override
  void initState() {
    super.initState();
    customer = widget.customer;
    customerDetailsCubit = BlocProvider.of<CustomerDetailsCubit>(context);
    customerDetailsCubit.onForeground(widget.customer);
  }

  @override
  Widget build(BuildContext context) {
    return RouteAwareness(
      onBackground: () {},
      onForeground: () => customerDetailsCubit.onForeground(widget.customer),
      child: BlocConsumer<CustomerDetailsCubit, CustomerDetailsState>(
        builder: _onStateBuilder,
        listener: _onStateListener,
      ),
    );
  }

  Widget _onStateBuilder(BuildContext context, CustomerDetailsState state) {
    if (state is AccountDetailsLoaded) {
      return Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Container(height: 120, color: Colors.green),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Icon(Icons.arrow_back),
                              color: Colors.white,
                            ),
                            IconButton(
                              onPressed: () {
                                Navigator.pushNamed(
                                  context,
                                  MyRoutes.accountDetailsActionsRoute,
                                  arguments: CustomerDetailsActionsScreenArgs(
                                    customer: widget.customer,
                                  ),
                                );
                              },
                              icon: const Icon(Icons.settings_outlined),
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 36.0,
                            bottom: 16.0,
                            top: 16.0,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const ProfilePicture(),
                              const SizedBox(height: 24.0),
                              Text(
                                widget.customer.customerName,
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              const SizedBox(height: 8.0),
                              Text(
                                state.accountDetailsReport.balanceStatusText,
                                style: TextStyle(
                                  fontSize: 16.0,
                                  color: state.accountDetailsReport
                                      .balanceStatusTextColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    padding: const EdgeInsets.only(left: 16.0),
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              MyRoutes.settleUpTransactionRoute,
                              arguments: SettleUpTransactionArgs(
                                customer: widget.customer,
                                status:
                                    state.accountDetailsReport.customerStatus,
                                overallBalance: state
                                    .accountDetailsReport.overallAccountBalance,
                              ),
                            );
                          },
                          child: const Text(
                            "Settle up",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          style: ButtonStyle(
                            minimumSize: MaterialStateProperty.all(
                              const Size(100.0, 40.0),
                            ),
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red[400]),
                          ),
                        ),
                        const SizedBox(width: 8.0),
                        state.accountDetailsReport.customerStatus ==
                                CustomerStatus.SURPLUS
                            ? Container()
                            : ElevatedButton(
                                onPressed: () {
                                  share(
                                    context,
                                    state.accountDetailsReport
                                        .overallAccountBalance,
                                    state.accountDetailsReport.customerStatus,
                                    state.accountDetailsReport.currentAccount,
                                  );
                                },
                                child: const Text(
                                  "Remind...",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54,
                                  ),
                                ),
                                style: ButtonStyle(
                                  minimumSize: MaterialStateProperty.all(
                                    const Size(100.0, 40.0),
                                  ),
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.white),
                                ),
                              ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  Flexible(
                    child: TransactionsListing(
                      transactions: state.accountDetailsReport.transactions,
                    ),
                  ),
                  Container(
                    height: 70,
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: (MediaQuery.of(context).size.width / 2) - 24.0,
                          height: 50.0,
                          child: MaterialButton(
                            shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            color: Colors.red[400],
                            onPressed: () {
                              Navigator.pushNamed(
                                context,
                                MyRoutes.addTransactionRoute,
                                arguments: AddTransactionArgs(
                                  customer: customer,
                                  type: TransactionType.DEBIT,
                                ),
                              );
                            },
                            textColor: Colors.white,
                            child: const Text(
                              "I GAVE",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: (MediaQuery.of(context).size.width / 2) - 24.0,
                          height: 50.0,
                          child: MaterialButton(
                            shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            color: Colors.green,
                            onPressed: () {
                              Navigator.pushNamed(
                                context,
                                MyRoutes.addTransactionRoute,
                                arguments: AddTransactionArgs(
                                  customer: customer,
                                  type: TransactionType.CREDIT,
                                ),
                              );
                            },
                            textColor: Colors.white,
                            child: const Text(
                              "I GOT",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    } else if (state is AccountDetailsError) {
      return const Scaffold(
        body: Center(
          child: Text("Error"),
        ),
      );
    } else {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  void _onStateListener(BuildContext context, CustomerDetailsState state) {}

  void share(BuildContext context, double balance, CustomerStatus status,
      Account account) {
    final RenderBox box = context.findRenderObject() as RenderBox;
    // final String text =
    //     "Hello ${widget.customer.customerName}! This is a reminder to please visit Hisabook \${appLink}";

    final String text = "Hello ${widget.customer.customerName}! "
        "You have to settle ${balance.abs()} RS to ${account.title}.\n\${appLink}";

    final String subject = "Reminder to ${widget.customer.customerName}";
    Share.share(
      text,
      subject: subject,
      sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }
}
