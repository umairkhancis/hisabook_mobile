import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:splitwise/presentation/bloc/add_more_customer/add_more_customer_cubit.dart';
import 'package:splitwise/presentation/dto/VerifyContactInfoScreenArgs.dart';
import 'package:splitwise/routes.dart';

class AddCustomerScreen extends StatefulWidget {
  const AddCustomerScreen({Key? key}) : super(key: key);

  @override
  _AddCustomerScreenState createState() => _AddCustomerScreenState();
}

class _AddCustomerScreenState extends State<AddCustomerScreen> {
  late AddMoreCustomerCubit cubit;

  @override
  initState() {
    super.initState();
    cubit = BlocProvider.of<AddMoreCustomerCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AddMoreCustomerCubit, AddMoreCustomerState>(
      listener: _onStateChangeListener,
      child: Scaffold(appBar: _buildAppBar(context)),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.5,
      automaticallyImplyLeading: false,
      centerTitle: true,
      // excludeHeaderSemantics: true,
      // titleSpacing: 32,
      /*leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
        onPressed: () => Navigator.pop(context),
      ),*/
      title: TypeAheadField(
          getImmediateSuggestions: true,
          textFieldConfiguration: TextFieldConfiguration(
            autofocus: true,
            autocorrect: true,
            keyboardType: TextInputType.text,
            enableSuggestions: false,
            onChanged: (text) {},
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Type the name of your customer",
              hintStyle: Theme.of(context).textTheme.caption,
            ),
          ),
          errorBuilder: (context, error) => Text("$error"),
          hideSuggestionsOnKeyboardHide: false,
          suggestionsBoxDecoration:
              const SuggestionsBoxDecoration(elevation: 0),
          itemBuilder: (BuildContext context, Contact contact) {
            return ListTile(
              leading: CircleAvatar(child: Text(contact.initials())),
              title: Text("${contact.displayName}"),
              subtitle: Text(getPhoneNumber(contact)),
            );
            // 966547337956
          },
          onSuggestionSelected: (Contact selectedContact) {
            cubit.onContactSelected(selectedContact);
          },
          suggestionsCallback: (String pattern) async {
            return await cubit.onSearch(pattern);
          },
        ),
      );
  }

  String getPhoneNumber(Contact contact) {
    List phones = contact.phones ?? [];
    if (contact.phones == null) {
      return "contact.phones list is null";
    } else {
      if (phones.isEmpty) {
        return "Phone Number is not found!";
      } else {
        return phones.first.value;
      }
    }
  }

  void _onStateChangeListener(
      BuildContext context, AddMoreCustomerState state) {
    if (state is NewContactSelected) {
      Navigator.pop(context);
      Navigator.pushNamed(
        context,
        MyRoutes.verifyContactInfoRoute,
        arguments: VerifyContactInfoScreenArgs(contact: state.selectedContact),
      );
    }
  }
}
