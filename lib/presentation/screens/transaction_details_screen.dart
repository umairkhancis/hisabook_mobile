import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:splitwise/presentation/bloc/add_transaction/add_transaction_cubit.dart';
import 'package:splitwise/presentation/bloc/transaction_details/transaction_details_cubit.dart';
import 'package:splitwise/presentation/dto/AddTransactionArgs.dart';
import 'package:splitwise/presentation/widgets/RouteAwareness.dart';
import 'package:splitwise/routes.dart';

class TransactionDetailsScreen extends StatefulWidget {
  final int id;

  const TransactionDetailsScreen({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  _TransactionDetailsScreenState createState() =>
      _TransactionDetailsScreenState();
}

class _TransactionDetailsScreenState extends State<TransactionDetailsScreen> {
  late TransactionDetailsCubit transactionDetailsCubit;

  @override
  void initState() {
    super.initState();
    transactionDetailsCubit = BlocProvider.of<TransactionDetailsCubit>(context);
    transactionDetailsCubit.onForeground(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return RouteAwareness(
      onBackground: () {},
      onForeground: () => transactionDetailsCubit.onForeground(widget.id),
      child: BlocConsumer<TransactionDetailsCubit, TransactionDetailsState>(
        builder: _onStateBuilder,
        listener: _onStateListener,
      ),
    );
  }

  void _onStateListener(BuildContext context, Object? state) {
    if (state is TransactionDeletedSuccessfully) {
      _goBack(context);
    }
    if (state is TransactionUpdatedSuccessfuly) {
      _goBack(context);
    }
  }

  Widget _onStateBuilder(BuildContext context, TransactionDetailsState state) {
    if (state is TransactionDetailsLoaded) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white70,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            color: Colors.black87,
            onPressed: () => _goBack(context),
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.delete_outlined, color: Colors.black87),
              onPressed: () async {
                await transactionDetailsCubit.onDeleteButtonPressed(
                  widget.id,
                  state.transactionDetailsReport.transaction.timeStamp,
                );
              },
            ),
            IconButton(
              icon: const Icon(Icons.edit_outlined, color: Colors.black87),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  MyRoutes.addTransactionRoute,
                  arguments: AddTransactionArgs(
                    customer:
                        state.transactionDetailsReport.transaction.customer,
                    transaction: state.transactionDetailsReport.transaction,
                    type: state.transactionDetailsReport.transaction.type,
                  ),
                );
              },
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 16.0),
                    width: 60,
                    height: 60,
                    color: Colors.black26,
                    child: _loadImageOrEmptyContainer(state),
                  ),
                  SizedBox(
                    width: 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          state.transactionDetailsReport.descriptionText,
                          // widget.transactionItemDetails.description,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        const SizedBox(height: 8),
                        Text(
                          state.transactionDetailsReport.amountText,
                          // "PKR 100.00",
                          textScaleFactor: 1.8,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black87,
                          ),
                        ),
                        const SizedBox(height: 16),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 16),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 50,
                    height: 50,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.green,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      state.transactionDetailsReport.breakDownText,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      );
    } else {
      return const Center(child: CircularProgressIndicator());
    }
  }

  Widget _loadImageOrEmptyContainer(TransactionDetailsLoaded state) {
    LogManager.info("Image file = ${state.transactionDetailsReport.image}");
    if (state.transactionDetailsReport.image == null ||
        state.transactionDetailsReport.image?.path == '') {
      return Container();
    } else {
      return Image.file(
        state.transactionDetailsReport.image!,
        fit: BoxFit.fill,
      );
    }
  }

  void _goBack(BuildContext context) => Navigator.pop(context);
}
