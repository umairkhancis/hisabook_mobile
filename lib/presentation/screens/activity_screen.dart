import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/domain/dto/activity_item_dto.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/presentation/bloc/activity/activity_cubit.dart';

class ActivityScreen extends StatefulWidget {
  const ActivityScreen({Key? key}) : super(key: key);

  @override
  _ActivityScreenState createState() => _ActivityScreenState();
}

class _ActivityScreenState extends State<ActivityScreen> {
  late ActivityCubit activityCubit;

  @override
  initState() {
    super.initState();
    activityCubit = BlocProvider.of<ActivityCubit>(context);
    activityCubit.onForeground();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ActivityCubit, ActivityState>(
      builder: _onStateBuilder,
      listener: _onStateListener,
    );
  }

  Widget _onStateBuilder(BuildContext context, ActivityState state) {
    if (state is ActivityIsEmpty) {
      return const Center(
        child: Text("You have no transactions yet."),
      );
    } else if (state is ActivityLoaded) {
      if (state.activityList.isNotEmpty) {
        return Scaffold(
          body: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16.0,
              vertical: 8.0,
            ),
            child: ListView(
              children: _buildActivityList(state.activityList),
            ),
          ),
        );
      } else {
        return Scaffold(
          body: Center(
            child: Text(
              "No Transactions Added.",
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
        );
      }
    } else {
      return const Scaffold(body: Center(child: CircularProgressIndicator()));
    }
  }

  List<Widget> _buildActivityList(List<ActivityItemDto> activities) {
    return activities
        .map((item) => _buildActivityListItem(context, item))
        .toList();
  }

  Widget _buildActivityListItem(BuildContext context, ActivityItemDto item) {
    if (item.transactionStatus == TransactionStatus.SETTLED) {
      return _buildSettledActivityItem(item, context);
    } else if (item.transactionStatus == TransactionStatus.DELETED) {
      return _buildDeletedActivityItem(item, context);
    } else {
      return _buildNormalActivityItem(item, context);
    }
  }

  Widget _buildNormalActivityItem(ActivityItemDto item, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 50,
            height: 50,
            child: const Icon(Icons.notes, color: Colors.black),
            decoration: const BoxDecoration(color: Colors.black12),
          ),
          Container(
            padding: const EdgeInsets.only(left: 16.0),
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*RichText(
                  textScaleFactor: 1.1,
                  text: TextSpan(
                    text: item.transactionAmountText,
                    style: TextStyle(color: item.transactionAmountColor),
                  ),
                ),*/
                item.activityRichText,
                const SizedBox(height: 4.0),
                RichText(
                  text: TextSpan(
                    text: item.dateText.toString(),
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDeletedActivityItem(ActivityItemDto item, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 50,
            height: 50,
            child: const Icon(Icons.notes, color: Colors.black),
            decoration: const BoxDecoration(color: Colors.black26),
          ),
          Container(
            padding: const EdgeInsets.only(left: 16.0),
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 4.0),
                /*RichText(
                  textScaleFactor: 1.1,
                  text: TextSpan(
                    text: item.activityRichText,
                    style: TextStyle(
                      color: item.transactionAmountColor,
                      decoration: TextDecoration.lineThrough,
                      decorationColor: Colors.red,
                      decorationStyle: TextDecorationStyle.solid,
                      decorationThickness: 2.0,
                    ),
                  ),
                ),*/
                item.activityRichText,
                const SizedBox(height: 4.0),
                RichText(
                  text: TextSpan(
                    text: item.dateText.toString(),
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSettledActivityItem(ActivityItemDto item, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            width: 50,
            height: 50,
            child: Icon(Icons.payments_outlined, color: Colors.green, size: 30),
          ),
          Container(
            padding: const EdgeInsets.only(left: 16.0),
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*RichText(
                  textScaleFactor: 1.1,
                  text: TextSpan(
                    text: item.activityRichText,
                    style: TextStyle(color: item.transactionAmountColor),
                  ),
                ),*/
                item.activityRichText,
                const SizedBox(height: 4.0),
                RichText(
                  text: TextSpan(
                    text: item.dateText.toString(),
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _onStateListener(BuildContext context, ActivityState state) {}
}
