import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:splitwise/presentation/screens/home_screen_1.dart';


enum MobileVerificationState {
  showMobileFormState,
  showOtpFormState,
}

class LoginScreen1 extends StatefulWidget {
  const LoginScreen1({Key? key}) : super(key: key);

  @override
  _LoginScreen1State createState() => _LoginScreen1State();
}

class _LoginScreen1State extends State<LoginScreen1> {
  MobileVerificationState currentState =
      MobileVerificationState.showMobileFormState;

  final phoneController = TextEditingController();
  final otpController = TextEditingController();

  final FirebaseAuth _auth = FirebaseAuth.instance;

  late String verificationId;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  bool showLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
          padding: const EdgeInsets.all(16.0),
          child: showLoading
              ? const Center(child: CircularProgressIndicator())
              : currentState == MobileVerificationState.showMobileFormState
                  ? getMobileFormWidget(context)
                  : getOtpFormWidget(context)),
    );
  }

  getMobileFormWidget(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        TextField(
          controller: phoneController,
          decoration: const InputDecoration(
            hintText: "Phone Number",
          ),
          onChanged: (value) {
            print("phoneNumber = ${phoneController.text}");
          },
        ),
        const SizedBox(height: 16.0),
        MaterialButton(
          onPressed: () async {
            setState(() {
              showLoading = true;
            });
            await _auth.verifyPhoneNumber(
              phoneNumber: phoneController.text,
              verificationCompleted: (phoneAuthCredential) async {
                setState(() {
                  showLoading = false;
                });
                print("verification Success");
                // signInWithPhoneAuthCredential(phoneAuthCredential);
              },
              verificationFailed: (verificationFailed) async {
                setState(() {
                  showLoading = false;
                });
                print("verification Failed");
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                      verificationFailed.message ?? "verification Failed",
                    ),
                  ),
                );
              },
              codeSent: (verificationId, resendingToken) async {
                setState(() {
                  showLoading = false;
                  currentState = MobileVerificationState.showOtpFormState;
                  this.verificationId = verificationId;
                  print("verificationId = $verificationId");
                  print("resendingToken = $resendingToken");
                  print("code Sent");
                });
              },
              codeAutoRetrievalTimeout: (verificationId) async {
                print("verification Timeout");
                // 700350
              },
            );
          },
          child: const Text("SEND"),
          color: Colors.green,
          textColor: Colors.white,
        ),
        const Spacer(),
      ],
    );
  }

  getOtpFormWidget(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        TextField(
          controller: otpController,
          decoration: const InputDecoration(
            hintText: "Enter OTP",
          ),
        ),
        const SizedBox(height: 16.0),
        MaterialButton(
          onPressed: () async {
            final phoneAuthCredential = PhoneAuthProvider.credential(
              verificationId: verificationId,
              smsCode: otpController.text,
            );

            signInWithPhoneAuthCredential(phoneAuthCredential);
          },
          child: const Text("VERIFY"),
          color: Colors.green,
          textColor: Colors.white,
        ),
        const Spacer(),
      ],
    );
  }

  Future<void> signInWithPhoneAuthCredential(
    PhoneAuthCredential phoneAuthCredential,
  )  async {
    setState(() {
      showLoading = true;
    });

    try {
      final UserCredential authCredential =
          await _auth.signInWithCredential(phoneAuthCredential);

      setState(() {
        showLoading = false;
      });

      if (authCredential.user != null) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const HomeScreen1()),
        );
      }
    } on FirebaseAuthException catch (e) {
      setState(() {
        showLoading = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(e.message ?? "Sign In With PhoneAuthCredential Failed"),
        ),
      );
    }
  }
}
