import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/presentation/bloc/customer_details_actions/account_details_actions_cubit.dart';
import 'package:splitwise/routes.dart';

class CustomerDetailsActionsScreen extends StatefulWidget {
  final Customer customer;

  const CustomerDetailsActionsScreen({Key? key, required this.customer})
      : super(key: key);

  @override
  State<CustomerDetailsActionsScreen> createState() =>
      _CustomerDetailsActionsScreenState();
}

class _CustomerDetailsActionsScreenState
    extends State<CustomerDetailsActionsScreen> {
  late CustomerDetailsActionsCubit cubit;

  @override
  initState() {
    super.initState();
    cubit = BlocProvider.of<CustomerDetailsActionsCubit>(context);
    cubit.onForeground();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CustomerDetailsActionsCubit,
        CustomerDetailsActionsState>(
      builder: _onStateBuilder,
      listener: _onStateListener,
    );
  }

  Widget _onStateBuilder(
    BuildContext context,
    CustomerDetailsActionsState state,
  ) {
    if (state is UserIsLoaded) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.5,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back, color: Colors.black),
          ),
          title: Text(
            "Customer details",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        body: ListView(
          children: [
            // const SizedBox(height: 16),
            ListTile(
              contentPadding: const EdgeInsets.symmetric(horizontal: 16.0),
              leading: Icon(
                Icons.store_outlined,
                color: Colors.green.withOpacity(0.9),
                size: 32.0,
              ),
              title: Text(
                widget.customer.customerName,
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
            const SizedBox(height: 8.0),
            ListTile(
              contentPadding: const EdgeInsets.symmetric(horizontal: 16.0),
              leading: Icon(
                Icons.smartphone_outlined,
                color: Colors.green.withOpacity(0.9),
                size: 32.0,
              ),
              title: Text(
                widget.customer.phoneNumber,
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ],
        ),
      );
    } else {
      return const Scaffold(body: Center(child: CircularProgressIndicator()));
    }
  }

  void _onStateListener(
    BuildContext context,
    CustomerDetailsActionsState state,
  ) {
    if (state is CustomerIsDeleted) {
      Navigator.pushNamed(context, MyRoutes.homePageRoute);
    }
  }
}
