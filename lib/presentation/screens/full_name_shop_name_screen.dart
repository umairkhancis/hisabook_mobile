import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/presentation/bloc/signup_or_login/signup_or_login_cubit.dart';
import 'package:splitwise/routes.dart';

class FullNameAndShopNameScreen extends StatefulWidget {
  final String phoneNumber;

  const FullNameAndShopNameScreen({Key? key, required this.phoneNumber})
      : super(key: key);

  @override
  _FullNameAndShopNameScreenState createState() =>
      _FullNameAndShopNameScreenState();
}

class _FullNameAndShopNameScreenState extends State<FullNameAndShopNameScreen> {
  final _formKey = GlobalKey<FormState>();

  final fullNameController = TextEditingController();
  final accountNameController = TextEditingController();

  late final String _phoneNumber;

  late SignupOrLoginCubit cubit;

  @override
  void initState() {
    cubit = BlocProvider.of<SignupOrLoginCubit>(context);
    _phoneNumber = widget.phoneNumber;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _buildFullNameField(),
                  const Divider(height: 30, thickness: 0.1),
                  _buildAccountNameField(),
                  const Divider(height: 30, thickness: 0.1),
                  _buildDoneButton(),
                  const Divider(height: 30, thickness: 0.1),
                  Text(
                    "By signing up, you accept the Hisabook Terms of Service.",
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  TextFormField _buildFullNameField() {
    return TextFormField(
      controller: fullNameController,
      autofocus: true,
      cursorWidth: 1.0,
      validator: (value) {
        if (value == null) {
          return "Enter your full name";
        } else if (value.length < 5) {
          return "Enter your full name";
        } else {
          return null;
        }
      },
      /*onSaved: (value) {
        _fullName = value ?? "";
      },*/
      decoration: const InputDecoration(
        hintText: "Full name",
        hintStyle: TextStyle(color: Colors.grey),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.green),
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
      ),
    );
  }

  TextFormField _buildAccountNameField() {
    return TextFormField(
      controller: accountNameController,
      autofocus: true,
      cursorWidth: 1.0,
      validator: (value) {
        if (value == null) {
          return "Enter your shop name";
        } else if (value.length < 5) {
          return "Enter your shop name";
        } else {
          return null;
        }
      },
      /*onSaved: (value) {
        _shopName = value ?? "";
      },*/
      decoration: const InputDecoration(
        hintText: "Shop name",
        hintStyle: TextStyle(color: Colors.grey),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.green),
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
      ),
    );
  }

  Widget _buildDoneButton() {
    return Column(
      children: [
        MaterialButton(
          onPressed: () async {
            final bool isValid = _formKey.currentState?.validate() ?? false;
            FocusScope.of(context).unfocus();
            if (isValid) {
              _formKey.currentState?.save();
              print(
                "Full Name: ${fullNameController.text}\n"
                "Account Name: ${accountNameController.text}\n"
                "Phone Number: $_phoneNumber",
              );
              await cubit.onSubmitButtonPressed(
                fullNameController.text,
                accountNameController.text,
                _phoneNumber,
              );
              Navigator.pushReplacementNamed(context, MyRoutes.homePageRoute);
            }
          },
          child: const Text(
            "SUBMIT",
            textScaleFactor: 1.1,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          height: 48.0,
          minWidth: 130,
          color: Colors.green,
        ),
      ],
    );
  }
}
