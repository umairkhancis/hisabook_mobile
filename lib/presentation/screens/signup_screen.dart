import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/otp_field_style.dart';
import 'package:otp_text_field/style.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:splitwise/presentation/bloc/phone_number_otp/phone_number_cubit.dart';
import 'package:splitwise/presentation/dto/FullNameAndShopNameScreenArgs.dart';
import 'package:splitwise/routes.dart';

enum MobileVerificationState {
  showMobileFormState,
  showOtpFormState,
}

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  MobileVerificationState currentState =
      MobileVerificationState.showMobileFormState;

  final fullNameController = TextEditingController();
  final phoneController = TextEditingController();
  final accountNameController = TextEditingController();
  final otpController = OtpFieldController();

  // ignore: unused_field
  final String _fullName = "";
  String _phoneNumber = "";

  final FirebaseAuth _auth = FirebaseAuth.instance;

  late String verificationId;
  late final PhoneNumberCubit cubit;

  String otpCode = "";

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  bool showLoading = false;

  final _formKey = GlobalKey<FormState>();

  int start = 60;
  bool wait = false;

  Timer? _timer;

  @override
  void initState() {
    cubit = BlocProvider.of<PhoneNumberCubit>(context);
    super.initState();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PhoneNumberCubit, PhoneNumberState>(
      listener: (context, state) {
        if (state is UserExists) {
          _timer?.cancel();
          LogManager.info("listener::state = ${state.toString()}");
          Navigator.pushReplacementNamed(context, MyRoutes.homePageRoute);
        } else {
          _timer?.cancel();
          LogManager.info("listener::state = ${state.toString()}");
          Navigator.pushReplacementNamed(
            context,
            MyRoutes.fullNameAndShopNameScreen,
            arguments: FullNameAndShopNameScreenArgs(phoneNumber: _phoneNumber),
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          // backgroundColor: Colors.white70,
          key: _scaffoldKey,
          body: Container(
            padding: const EdgeInsets.all(16.0),
            child: showLoading
                ? const Center(child: CircularProgressIndicator())
                : currentState == MobileVerificationState.showMobileFormState
                    ? getMobileFormWidget(context)
                    : getOtpFormWidget(context),
          ),
        );
      },
    );
  }

  Widget getMobileFormWidget(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Text(
                  "Welcome to Hisabook",
                  style: TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 50.0),
                _buildPhoneNumberField(),
                const Divider(height: 50.0, thickness: 0.1),
                _buildVerifyButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getOtpFormWidget(context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 120.0),
              Text(
                "Enter your SMS code",
                style: Theme.of(context).textTheme.headline5,
              ),
              const SizedBox(height: 20.0),
              Text(
                "OTP code will be sent via SMS to\n$_phoneNumber",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle1,
              ),
              const SizedBox(height: 50.0),
              otpField(),
              const SizedBox(height: 40.0),
              RichText(
                text: TextSpan(
                  children: [
                    const TextSpan(
                      text: "Send OTP again in",
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    ),
                    TextSpan(
                      text: " 00:$start",
                      style: const TextStyle(fontSize: 16, color: Colors.green),
                    ),
                    const TextSpan(
                      text: " sec ",
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 50.0),
              MaterialButton(
                onPressed: () async {
                  final phoneAuthCredential = PhoneAuthProvider.credential(
                    verificationId: verificationId,
                    smsCode: otpCode.toString(),
                  );
                  signInWithPhoneAuthCredential(phoneAuthCredential);
                },
                child: const Text(
                  "SUBMIT",
                  textScaleFactor: 1.1,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                height: 48.0,
                minWidth: 130,
                color: Colors.green,
              ),
              const SizedBox(height: 30.0),
              InkWell(
                onTap: wait
                    ? null
                    : () async {
                        await verifyPhoneNumber();
                        await startTimer();
                        setState(() {
                          start = 60;
                          wait = true;
                        });
                      },
                child: Container(
                  width: 130,
                  height: 48,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(
                      color: wait ? Colors.grey : Colors.green,
                    ),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    "Resend",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: wait ? Colors.grey : Colors.green,
                      fontSize: 16,
                      // decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
    /*return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Enter your OTP",
            style: Theme.of(context).textTheme.headline4,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: PinFieldAutoFill(
              autoFocus: true,
              controller: otpController,
              codeLength: 6,
              onCodeChanged: (val) {
                LogManager.info("val = $val");
              },
            ),
          ),
          const SizedBox(height: 50),
          MaterialButton(
            onPressed: () async {
              final phoneAuthCredential = PhoneAuthProvider.credential(
                verificationId: verificationId,
                smsCode: otpController.text,
              );
              signInWithPhoneAuthCredential(phoneAuthCredential);
            },
            child: const Text(
              "SIGN UP",
              textScaleFactor: 1.1,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            height: 48.0,
            minWidth: 130,
            color: Colors.green,
          )
        ],
      ),
    );*/
  }

  Future startTimer() async {
    const onSec = Duration(seconds: 1);
    _timer = Timer.periodic(onSec, (timer) {
      if (start == 0) {
        setState(() {
          timer.cancel();
          wait = false;
        });
      } else {
        setState(() {
          start--;
        });
      }
    });
  }

  Widget otpField() {
    return OTPTextField(
      controller: otpController,
      length: 6,
      width: MediaQuery.of(context).size.width - 34,
      fieldWidth: 50,
      otpFieldStyle: OtpFieldStyle(
        focusBorderColor: Colors.green,
        // backgroundColor: Colors.white30,
        borderColor: Colors.black,
      ),
      onChanged: (value) {
        otpCode = value;
        LogManager.info("value: " + value);
      },
      style: const TextStyle(fontSize: 17),
      textFieldAlignment: MainAxisAlignment.spaceAround,
      fieldStyle: FieldStyle.box,
      onCompleted: (pin) {
        otpCode = pin;
        print("Completed: " + pin);
      },
    );
  }

  Widget _buildPhoneNumberField() {
    return Flex(
      direction: Axis.horizontal,
      children: [
        Row(
          children: [
            Flag.fromCode(
              FlagsCode.PK,
              width: 30,
              height: 30,
              fit: BoxFit.fitWidth,
            ),
            const SizedBox(width: 8),
            const Text("+92"),
          ],
        ),
        const SizedBox(width: 16),
        Expanded(
          flex: 5,
          child: TextFormField(
            controller: phoneController,
            keyboardType: TextInputType.number,
            onSaved: (value) {
              _phoneNumber = value ?? "";
              if (_phoneNumber.contains("0", 0)) {
                _phoneNumber = _phoneNumber.replaceFirst("0", "+92", 0);
              } else {
                _phoneNumber = _phoneNumber.replaceAll(
                  _phoneNumber,
                  "+92$_phoneNumber",
                );
              }
              LogManager.info("phoneNumber: $_phoneNumber");
            },
            decoration: const InputDecoration(
              hintText: "Phone Number",
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green),
              ),
            ),
          ),
        ),
      ],
    );
  }

  MaterialButton _buildVerifyButton() {
    return MaterialButton(
      onPressed: () async {
        final bool isValid = _formKey.currentState?.validate() ?? false;
        FocusScope.of(context).unfocus();
        if (isValid) {
          _formKey.currentState?.save();
          setState(() {
            showLoading = true;
          });
          LogManager.info("Phone Number: $_phoneNumber\n");
          await verifyPhoneNumber();
        }
      },
      child: const Text(
        "VERIFY",
        textScaleFactor: 1.1,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      height: 48.0,
      minWidth: 130,
      color: Colors.green,
    );
  }

  Future<void> verifyPhoneNumber() async {
    return await _auth.verifyPhoneNumber(
      phoneNumber: _phoneNumber,
      timeout: const Duration(seconds: 60),
      verificationCompleted: (phoneAuthCredential) async {
        /*setState(() {
              showLoading = false;
            });*/
        LogManager.info("verification Success");
        // signInWithPhoneAuthCredential(phoneAuthCredential);
      },
      verificationFailed: (verificationFailed) async {
        setState(() {
          showLoading = false;
        });
        LogManager.info("Verification Failed");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              verificationFailed.message ?? "verification Failed",
            ),
          ),
        );
      },
      codeSent: (verificationId, resendingToken) async {
        setState(() async {
          showLoading = false;
          currentState = MobileVerificationState.showOtpFormState;
          this.verificationId = verificationId;
          wait = true;
          await startTimer();
          LogManager.info("VerificationId = $verificationId");
          LogManager.info("ResendingToken = $resendingToken");
          LogManager.info("Code Sent");
        });
      },
      codeAutoRetrievalTimeout: (verificationId) async {
        LogManager.info("Verification Timeout");
      },
    );
  }

  Future<void> signInWithPhoneAuthCredential(
    PhoneAuthCredential phoneAuthCredential,
  ) async {
    setState(() {
      showLoading = true;
    });

    try {
      final UserCredential authCredential =
          await _auth.signInWithCredential(phoneAuthCredential);

      /*setState(() {
        showLoading = false;
      });*/

      if (authCredential.user != null) {
        await cubit.checkUser(_phoneNumber);
      }
    } on FirebaseAuthException catch (e) {
      setState(() {
        showLoading = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(e.message ?? "Sign In With PhoneAuthCredential Failed"),
        ),
      );
    }
  }
}
