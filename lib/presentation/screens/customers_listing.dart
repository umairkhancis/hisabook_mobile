import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:splitwise/presentation/bloc/customers_listing/customers_listing_cubit.dart';
import 'package:splitwise/presentation/widgets/RouteAwareness.dart';
import 'package:splitwise/presentation/widgets/account_status_widget.dart';
import 'package:splitwise/presentation/widgets/accounts_list_widget.dart';
import 'package:splitwise/presentation/widgets/add_more_customers_button.dart';

class CustomersListing extends StatefulWidget {
  final String customerName;

  const CustomersListing({Key? key, required this.customerName})
      : super(key: key);

  @override
  State<CustomersListing> createState() {
    return _CustomersListingState();
  }

  getState() => _CustomersListingState();
}

class _CustomersListingState extends State<CustomersListing> {
  late CustomersListingCubit customersListingCubit;

  @override
  void initState() {
    super.initState();
    customersListingCubit = BlocProvider.of<CustomersListingCubit>(context);
    customersListingCubit.onForeground(widget.customerName);
  }

  @override
  Widget build(BuildContext context) {
    return RouteAwareness(
      onForeground: () async {
        await customersListingCubit.onForeground(widget.customerName);
      },
      onBackground: () => LogManager.info('onBackground'),
      child: BlocConsumer<CustomersListingCubit, CustomersListingState>(
        builder: _onStateBuilder,
        listener: _onStateListener,
      ),
    );
  }

  refresh() {
    debugPrint('customersListing page refresh');
    setState(() async {
      await customersListingCubit.onForeground(widget.customerName);
    });
  }

  Widget _onStateBuilder(BuildContext context, CustomersListingState state) {
    if (state is AccountsListingInitial) {
      return const Scaffold(body: Center(child: CircularProgressIndicator()));
    } else if (state is AccountsListingLoaded) {
      return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: AccountStatusWidget(
                  overallSurplusAccountStatusText:
                      state.report.overallSurplusAccountStatusText,
                  overallDebtAccountStatusText:
                      state.report.overallDebtAccountStatusText,
                  overallSettledAcountStatusText:
                      state.report.overallSettledAccountStatusText,
                ),
              ),
              CustomersList(items: state.report.items),
              const SizedBox(height: 20.0),
              const AddMoreCustomersButton(),
            ],
          ),
        ),
      );
    } else {
      return const Scaffold(
        body: Center(
          child: Text("Oops! Something went wrong"),
        ),
      );
    }
  }

  void _onStateListener(BuildContext context, CustomersListingState state) {}
}
