import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:sms_autofill/sms_autofill.dart';
import 'package:splitwise/presentation/bloc/signup_or_login/signup_or_login_cubit.dart';
import 'package:splitwise/routes.dart';

class OtpScreen extends StatefulWidget {
  final String fullName;
  final String phoneNumber;

  const OtpScreen({Key? key, required this.fullName, required this.phoneNumber})
      : super(key: key);

  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  late SignupOrLoginCubit cubit;
  String? _otp = "";
  late String fullName;
  late String phoneNumber;

  @override
  void initState() {
    cubit = BlocProvider.of<SignupOrLoginCubit>(context);
    fullName = widget.fullName;
    phoneNumber = widget.phoneNumber;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Enter your OTP",
            style: Theme.of(context).textTheme.headline4,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            child: PinFieldAutoFill(
              codeLength: 4,
              onCodeChanged: (val) {
                _otp = val;
              },
            ),
          ),
          const SizedBox(height: 40),
          MaterialButton(
            onPressed: () async {
              final int statusCode = await _sendOTPRequest(_otp ?? "0");
              if (statusCode == HttpStatus.ok) {
                // await cubit.onOTPSubmitButtonPressed(fullName, phoneNumber);
                Navigator.pop(context);
                await Navigator.pushNamed(context, MyRoutes.homePageRoute);
              }
            },
            child: const Text(
              "Verify OTP",
              textScaleFactor: 1.1,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            height: 48.0,
            minWidth: 130,
            color: Colors.green,
          )
        ],
      ),
    );
  }

  Future<int> _sendOTPRequest(String otp) async {
    var url = "http://192.168.8.114:8081/otp";
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, dynamic>{
        "otp": int.parse(otp),
      }),
    );
    return response.statusCode;
  }
}
