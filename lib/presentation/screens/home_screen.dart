import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:splitwise/main.dart';
import 'package:splitwise/presentation/bloc/personal_account/personal_account_cubit.dart';
import 'package:splitwise/routes.dart';

enum CustomerListingState {
  buildTextField,
  buildTwoButtons,
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int bottomSelectedIndex = 0;

  CustomerListingState currentState = CustomerListingState.buildTwoButtons;

  final customerNameController = TextEditingController();
  String _customerName = "";
  late final PersonalAccountCubit personalAccountCubit;

  @override
  void initState() {
    super.initState();
    personalAccountCubit = BlocProvider.of<PersonalAccountCubit>(context);
    personalAccountCubit.onForeground();
    _askPermissions();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PersonalAccountCubit, PersonalAccountState>(
      builder: _onStateBuilder,
      listener: _onStateListener,
    );
  }

  Scaffold _onStateBuilder(BuildContext context, PersonalAccountState state) {
    if (state is PersonalAccountDetailsLoaded) {
      return Scaffold(
        appBar: currentState == CustomerListingState.buildTwoButtons
            ? _buildAppBar(state)
            : _buildTextFormField(),
        body: _buildPageView(_customerName),
        bottomNavigationBar: BottomNavigationBar(
          // backgroundColor: Colors.white70,
          currentIndex: bottomSelectedIndex,
          onTap: (index) {
            bottomTapped(index);
          },
          items: _buildBottomNavBarItems(),
        ),
      );
    } else {
      return const Scaffold(
        body: Center(child: CircularProgressIndicator()),
      );
    }
  }

  void _onStateListener(BuildContext context, PersonalAccountState state) {}

  AppBar _buildAppBar(PersonalAccountState state) {
    if (state is PersonalAccountDetailsLoaded) {
      return AppBar(
        backgroundColor: Colors.white,
        title: Text(
          state.personalAccount.accountName,
          style: const TextStyle(color: Colors.black),
        ),
        automaticallyImplyLeading: false,
        elevation: 0.5,
        /*actions: [
        IconButton(
          icon: const Icon(Icons.search_rounded, color: Colors.black),
          onPressed: () {
            setState(() {
              currentState = CustomerListingState.buildTextField;
            });
          },
        ),
        IconButton(
          icon: const Icon(Icons.person_add_alt, color: Colors.black),
          onPressed: () {
            Navigator.pushNamed(context, MyRoutes.addMoreCustomersRoute);
          },
        ),
      ],*/
      );
    } else {
      LogManager.info("_buildAppBar = couldn't fetch account");
      return AppBar(
        backgroundColor: Colors.white70,
        elevation: 0.1,
      );
    }
  }

  AppBar _buildTextFormField() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.5,
      title: TextField(
        controller: customerNameController,
        autofocus: true,
        autocorrect: true,
        keyboardType: TextInputType.text,
        enableSuggestions: true,
        onChanged: (text) {
          setState(() {
            _customerName = customerNameController.text;
          });
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Type the name of your customer",
          hintStyle: Theme.of(context).textTheme.caption,
        ),
        textInputAction: TextInputAction.search,
        onSubmitted: (value) {
          setState(() {
            _customerName = value;
            currentState = CustomerListingState.buildTwoButtons;
            LogManager.info("onSubmittedCustomerName = $_customerName");
            Navigator.of(context)
                .pushReplacementNamed(MyRoutes.homePageRoute)
                .whenComplete(() {});
          });
        },
      ),
    );
  }

  List<BottomNavigationBarItem> _buildBottomNavBarItems() {
    return [
      const BottomNavigationBarItem(
        icon: Icon(Icons.person_outlined, color: Colors.black54),
        label: "Customers",
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.photo_outlined, color: Colors.black54),
        label: "Activity",
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.account_circle_outlined, color: Colors.black54),
        label: "Account",
      ),
    ];
  }

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: false,
  );

  void pageChanged(int index) {
    setState(() {
      bottomSelectedIndex = index;
    });
  }

  PageView _buildPageView(String customerName) {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        pageChanged(index);
      },
      children: <Widget>[
        customersListingScreen(customerName),
        activityScreen(),
        personalAccountScreen(),
      ],
    );
  }

  void bottomTapped(int index) {
    setState(() {
      bottomSelectedIndex = index;
      currentState = CustomerListingState.buildTwoButtons;
      pageController.animateToPage(
        index,
        duration: const Duration(milliseconds: 300),
        curve: Curves.ease,
      );
    });
  }

  Future<void> _askPermissions() async {
    PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus != PermissionStatus.granted) {
      _handleInvalidPermissions(permissionStatus);
    }
  }

  Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.permanentlyDenied) {
      PermissionStatus permissionStatus = await Permission.contacts.request();
      return permissionStatus;
    } else {
      return permission;
    }
  }

  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      const snackBar = SnackBar(content: Text('Access to contact data denied'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else if (permissionStatus == PermissionStatus.permanentlyDenied) {
      const snackBar =
          SnackBar(content: Text('Contact data not available on device'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
