import 'package:flutter/material.dart';

class AccountSettingsScreen extends StatefulWidget {
  const AccountSettingsScreen({Key? key}) : super(key: key);

  @override
  _AccountSettingsScreenState createState() => _AccountSettingsScreenState();
}

class _AccountSettingsScreenState extends State<AccountSettingsScreen> {
  String fullName = "Usman Khan";
  String emailAddress = "ukhan.nedian@gmail.com";
  String phoneNumber = "+923462498393";
  String password = "";
  String timeZone = "(GMT-05:00) Eastern Time";
  String currency = "PKR";
  String language = "English";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back, color: Colors.black),
        ),
        title: Text(
          "Account settings",
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Full name", style: Theme.of(context).textTheme.bodyText2),
              _buildFullNameFormField(),
              _buildFullNameText(),
              const SizedBox(height: 16.0),
              Text(
                "Email address",
                style: Theme.of(context).textTheme.bodyText2,
              ),
              _buildEmailAddressFormField(),
              _buildEmailAddressText(),
              const SizedBox(height: 16.0),
              Text(
                "Phone number",
                style: Theme.of(context).textTheme.bodyText2,
              ),
              _buildPhoneNumberFormField(),
              _buildPhoneNumberText(),
              const SizedBox(height: 16.0),
              Text("Password", style: Theme.of(context).textTheme.bodyText2),
              _buildPasswordFormField(),
              _buildPasswordObsecureText(),
              const SizedBox(height: 16.0),
              Text("Time zone", style: Theme.of(context).textTheme.subtitle1),
              /*DropdownButtonFormField(
                items: const [
                  DropdownMenuItem(
                    child: Text("(GMT-05:00) Eastern Time"),
                  ),
                ],
                onChanged: (value) {},
              ),*/
              const Text(
                "(GMT-05:00) Eastern Time",
                textScaleFactor: 1.2,
              ),
              const SizedBox(height: 16.0),
              Text(
                "Default currency",
                style: Theme.of(context).textTheme.subtitle1,
              ),
              /*DropdownButtonFormField(
                items: const [
                  DropdownMenuItem(child: Text("PKR")),
                ],
                onChanged: (value) {},
              ),*/
              const Text(
                "PKR",
                textScaleFactor: 1.2,
              ),
              const SizedBox(height: 16.0),
              Text("Language (for emails and notifications)",
                  style: Theme.of(context).textTheme.subtitle1),
              /*DropdownButtonFormField(
                items: const [
                  DropdownMenuItem(child: Text("English")),
                ],
                onChanged: (value) {},
              ),*/
              const Text(
                "English",
                textScaleFactor: 1.2,
              ),
              const SizedBox(height: 16.0),
              Text(
                "Privacy settings",
                style: Theme.of(context).textTheme.subtitle1,
              ),
              const Text(
                "Allow other Splitwise users who have my email address or phone number in their contact book to see that I use Hisabook",
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFullNameFormField() {
    return SizedBox(
      width: 250,
      child: TextFormField(
        cursorHeight: 25,
        decoration: const InputDecoration(
          isDense: false,
          focusedBorder: OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(),
        ),
        onChanged: (value) => fullName = value,
      ),
    );
  }

  Widget _buildEmailAddressFormField() {
    return SizedBox(
      width: 250,
      child: TextFormField(
        cursorHeight: 25,
        decoration: const InputDecoration(
          isDense: false,
          focusedBorder: OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(),
        ),
        onChanged: (value) => emailAddress = value,
      ),
    );
  }

  Widget _buildPhoneNumberFormField() {
    return SizedBox(
      width: 250,
      child: TextFormField(
        cursorHeight: 25,
        decoration: const InputDecoration(
          isDense: false,
          focusedBorder: OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(),
        ),
        onChanged: (value) => phoneNumber = value,
      ),
    );
  }

  Widget _buildPasswordFormField() {
    return SizedBox(
      width: 250,
      child: TextFormField(
        cursorHeight: 25,
        decoration: const InputDecoration(
          isDense: false,
          focusedBorder: OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(),
        ),
        obscureText: true,
        onChanged: (value) => fullName = value,
      ),
    );
  }

  Row _buildPasswordObsecureText() {
    return Row(
      children: [
        const Text(
          "••••••••••••••",
          textScaleFactor: 1.5,
          style: TextStyle(fontWeight: FontWeight.w900),
        ),
        const SizedBox(width: 16.0),
        GestureDetector(
          child: const Text(
            "Edit",
            style: TextStyle(
              decoration: TextDecoration.underline,
              color: Colors.blue,
            ),
          ),
        ),
      ],
    );
  }

  Row _buildPhoneNumberText() {
    return Row(
      children: [
        const Text(
          "+923462498393",
          textScaleFactor: 1.2,
          style: TextStyle(fontWeight: FontWeight.w900),
        ),
        const SizedBox(width: 16.0),
        GestureDetector(
          child: const Text("Edit",
              style: TextStyle(
                  decoration: TextDecoration.underline, color: Colors.blue)),
        ),
      ],
    );
  }

  Row _buildEmailAddressText() {
    return Row(
      children: [
        const Text(
          "ukhan.nedian@gmail.com",
          textScaleFactor: 1.2,
          style: TextStyle(fontWeight: FontWeight.w900),
        ),
        const SizedBox(width: 16.0),
        GestureDetector(
          child: const Text(
            "Edit",
            style: TextStyle(
              decoration: TextDecoration.underline,
              color: Colors.blue,
            ),
          ),
        ),
      ],
    );
  }

  Row _buildFullNameText() {
    return Row(
      children: [
        const Text(
          "Usman Khan",
          textScaleFactor: 1.2,
          style: TextStyle(fontWeight: FontWeight.w900),
        ),
        const SizedBox(width: 16.0),
        GestureDetector(
          child: const Text("Edit",
              style: TextStyle(
                  decoration: TextDecoration.underline, color: Colors.blue)),
        ),
      ],
    );
  }
}
