import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/data/repository/local_db_ledger_repository.dart';
import 'package:splitwise/domain/entity/user.dart';
import 'package:splitwise/presentation/screens/temp_screens/user/delete_user.dart';
import 'package:splitwise/presentation/screens/temp_screens/user/update_user.dart';

class UsersListScreen extends StatefulWidget {
  const UsersListScreen({Key? key}) : super(key: key);

  @override
  State<UsersListScreen> createState() => _UsersListScreenState();
}

class _UsersListScreenState extends State<UsersListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Users"),
      ),
      body: FutureBuilder<List<User>>(
        future: _getUsers(),
        builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
          if (!snapshot.hasData) {
            return const Center(child: Text("No data to show"));
          } else {
            return ListView.builder(
              itemCount: snapshot.data?.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                title: Text(
                  "${snapshot.data?[index].name}",
                ),
                subtitle: Text(
                  "${snapshot.data?[index].phoneNumber}",
                ),
                leading: CircleAvatar(
                  backgroundColor: Colors.blueAccent,
                  child: Text(
                    "${snapshot.data?[index].initials}",
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          DetailsPage(user: snapshot.data![index]),
                    ),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }

  /*Future<List<User>> _getUsers() async {
    var url = Uri.parse("http://localhost:8081/user/");
    var data = await http.get(url);
    var jsonData = json.decode(data.body);

    List<User> users = [];

    for (var u in jsonData) {
      User user = User(
        id: u["id"],
        name: u["name"],
        phoneNumber: u["phoneNumber"],
        initials: u["initials"],
      );

      users.add(user);
    }
    return users;
  }*/

Future<List<User>>? _getUsers() async {
    final repository = LocalDbLedgerRepository();
    await repository.init();

    return await repository.getUsers();
  }
}

class DetailsPage extends StatefulWidget {
  final User user;

  const DetailsPage({Key? key, required this.user}) : super(key: key);

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.user.name),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return UpdateUser(user: widget.user);
                  },
                ),
              );
            },
            icon: const Icon(
              Icons.edit,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: Text(
        "Name: ${widget.user.name}\tPhone Number: ${widget.user.phoneNumber}\tInitials: ${widget.user.initials}",
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          deleteUser(widget.user);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const DeleteUser()),
          );
        },
        child: const Icon(Icons.delete),
        backgroundColor: Colors.pink,
      ),
    );
  }

  void deleteUser(User user) async {
    final url = Uri.parse("http://localhost:8081/users/${user.id}");
    final request = http.Request("DELETE", url);
    request.headers
        .addAll(<String, String>{"Content-type": "application/json"});
    request.body = jsonEncode(user);
    final response = await request.send();
  }
}
