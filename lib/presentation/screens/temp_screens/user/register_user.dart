import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/domain/entity/user.dart';

class RegisterUser extends StatefulWidget {
  const RegisterUser({Key? key}) : super(key: key);

  @override
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
  final double minimumPadding = 5.0;

  TextEditingController nameController = TextEditingController();
  TextEditingController initialsController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();

  User? user;

  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = Theme.of(context).textTheme.subtitle2;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register User"),
      ),
      body: Form(
        child: Padding(
          padding: EdgeInsets.all(minimumPadding * 2),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: nameController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Name";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Name",
                    hintText: "Enter your Name",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: phoneNumberController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Phone Number";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Phone Number",
                    hintText: "Enter your Phone Number",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: initialsController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Initials";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Initials",
                    hintText: "Enter your Initials",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                child: const Text("Submit"),
                onPressed: () async {
                  String name = nameController.text;
                  String phoneNumber = phoneNumberController.text;
                  String initials = initialsController.text;
                  User? u = await registerUser(
                    name,
                    phoneNumber,
                    initials,
                    context,
                  );
                  nameController.text = '';
                  phoneNumberController.text = '';
                  initialsController.text = '';
                  setState(() {
                    user = u;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Future<User?> registerUser(
  String name,
  String phoneNumber,
  String initials,
  BuildContext context,
) async {
  var url = "http://localhost:8081/user";
  var response = await http.post(
    Uri.parse(url),
    headers: <String, String>{"Content-Type": "application/json"},
    body: jsonEncode(<String, String>{
      "name": name,
      "phoneNumber": phoneNumber,
      "initials": initials,
    }),
  );

  String responseString = response.body;
  print("response Status Code = ${response.statusCode}");
  if (response.statusCode == 200) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return MyAlertDialog(
          title: "Backend Response",
          content: responseString,
        );
      },
    );
  }
}

class MyAlertDialog extends StatelessWidget {
  final String title;
  final String content;
  final List<Widget> actions;

  const MyAlertDialog({
    Key? key,
    required this.title,
    required this.content,
    this.actions = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title, style: Theme.of(context).textTheme.subtitle2),
      actions: actions,
      content: Text(content, style: Theme.of(context).textTheme.bodyText1),
    );
  }
}
