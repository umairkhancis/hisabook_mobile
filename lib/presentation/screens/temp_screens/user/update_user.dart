import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/domain/entity/user.dart';
import 'package:splitwise/presentation/screens/temp_screens/home_drawer.dart';
import 'package:splitwise/presentation/screens/temp_screens/user/register_user.dart';

class UpdateUser extends StatefulWidget {
  final User user;

  @override
  _UpdateUserState createState() => _UpdateUserState();

  const UpdateUser({Key? key, required this.user}) : super(key: key);
}

class _UpdateUserState extends State<UpdateUser> {
  final double minimumPadding = 5.0;

  late User? user;
  late TextEditingController nameController;
  late TextEditingController phoneNumberController;
  late TextEditingController initialsController;
  final bool _isEnabled = true;
  late Future<List<User>> employees;

  @override
  void initState() {
    super.initState();
    user = widget.user;
    nameController = TextEditingController(text: user?.name);
    phoneNumberController = TextEditingController(text: user?.phoneNumber);
    initialsController = TextEditingController(text: user?.initials);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = Theme.of(context).textTheme.subtitle2;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Update Employee"),
        leading: IconButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const HomeDrawer()),
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: Form(
        child: Padding(
          padding: EdgeInsets.all(minimumPadding * 2),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: nameController,
                  enabled: _isEnabled,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Name";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Name",
                    hintText: "Enter Your Name",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: phoneNumberController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Phone Number";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Phone Number",
                    hintText: "Enter your Phone Number",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: initialsController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Initials";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Initials",
                    hintText: "Enter your Initials",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                child: const Text("Update Details"),
                onPressed: () async {
                  String name = nameController.text;
                  String phoneNumber = phoneNumberController.text;
                  String initials = initialsController.text;
                  User u = User(
                    id: user?.id ?? 0,
                    name: name,
                    phoneNumber: phoneNumber,
                    initials: initials,
                  );
                  User? employeeModel = await updateUser(u, context);
                  setState(() {
                    user = employeeModel;
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

Future<User?> updateUser(
  User user,
  BuildContext context,
) async {
  var url = Uri.parse("http://localhost:8081/user/${user.id}");
  var response = await http.put(
    url,
    headers: <String, String>{"Content-Type": "application/json"},
    body: jsonEncode(user),
  );
  String responseString = response.body;
  if (response.statusCode == 200) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return MyAlertDialog(
          title: "Backend Response",
          content: responseString,
        );
      },
    );
  }
}
