import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/data/repository/local_db_ledger_repository.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/presentation/screens/temp_screens/accounts/delete_account.dart';
import 'package:splitwise/presentation/screens/temp_screens/accounts/update_account.dart';

class AccountsListScreen extends StatefulWidget {
  const AccountsListScreen({Key? key}) : super(key: key);

  @override
  State<AccountsListScreen> createState() => _AccountsListScreenState();
}

class _AccountsListScreenState extends State<AccountsListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Accounts"),
      ),
      body: FutureBuilder<List<Customer>>(
        future: _getCustomers(),
        builder: (BuildContext context, AsyncSnapshot<List<Customer>> snapshot) {
          if (!snapshot.hasData) {
            return const Center(child: Text("No data to show"));
          } else {
            return ListView.builder(
              itemCount: snapshot.data?.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                title: Text(
                  "${snapshot.data?[index].customerName}",
                ),
                subtitle: Text(
                  "${snapshot.data?[index].phoneNumber}",
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return DetailsPage(customer: snapshot.data![index]);
                      },
                    ),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }

  Future<List<Customer>>? _getCustomers() async {
    final repository = LocalDbLedgerRepository();
    await repository.init();

    return await repository.getCustomers();
  }
}

class DetailsPage extends StatefulWidget {
  final Customer customer;

  const DetailsPage({Key? key, required this.customer}) : super(key: key);

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.customer.customerName),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return UpdateAccount(customer: widget.customer);
                  },
                ),
              );
            },
            icon: const Icon(
              Icons.edit,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: Text(
        "Title: ${widget.customer.customerName} User: ${widget.customer.phoneNumber}",
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // deleteAccount(widget.customer);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const DeleteAccount()),
          );
        },
        child: const Icon(Icons.delete),
        backgroundColor: Colors.pink,
      ),
    );
  }

  void deleteAccount(Account account) async {
    final url = Uri.parse("http://localhost:8081/account/${account.id}");
    final request = http.Request("DELETE", url);
    request.headers
        .addAll(<String, String>{"Content-type": "application/json"});
    request.body = jsonEncode(account);
    final response = await request.send();
  }
}
