import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/custom_account_dto.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/presentation/screens/temp_screens/home_drawer.dart';
import 'package:splitwise/presentation/screens/temp_screens/user/register_user.dart';

class UpdateAccount extends StatefulWidget {
  final Customer customer;

  @override
  _UpdateAccountState createState() => _UpdateAccountState();

  const UpdateAccount({Key? key, required this.customer}) : super(key: key);
}

class _UpdateAccountState extends State<UpdateAccount> {
  final double minimumPadding = 5.0;

  late Customer? customer;
  late int accountId;
  late TextEditingController titleController;
  final bool _isEnabled = true;
  late Future<List<Account>> accounts;

  @override
  void initState() {
    super.initState();
    customer = widget.customer;
    accountId = customer?.id ?? 0;
    titleController = TextEditingController(text: customer?.customerName);
    print("accountId = $accountId");
  }

  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = Theme.of(context).textTheme.subtitle2;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Update Employee"),
        leading: IconButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const HomeDrawer()),
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: Form(
        child: Padding(
          padding: EdgeInsets.all(minimumPadding * 2),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: titleController,
                  enabled: _isEnabled,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Account Title";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Name",
                    hintText: "Enter Account Title",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                child: const Text("Update Details"),
                onPressed: () async {
                  String title = titleController.text;
                  CustomAccountDto a = CustomAccountDto(
                    accountId: accountId,
                    title: title,
                  );
                  Customer? cust = await updateCustomer(a, context);
                  setState(() {
                    customer = cust;
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

Future<Customer?> updateCustomer(
  CustomAccountDto account,
  BuildContext context,
) async {
  var url = Uri.parse(
    "http://localhost:8081/account/${account.accountId}",
  );
  var response = await http.put(
    url,
    headers: <String, String>{"Content-Type": "application/json"},
    body: jsonEncode(account),
  );
  String responseString = response.body;
  if (response.statusCode == 200) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return MyAlertDialog(
          title: "Backend Response",
          content: responseString,
        );
      },
    );
  }
}
