import 'package:flutter/material.dart';
import 'package:splitwise/presentation/screens/temp_screens/home_drawer.dart';
// import 'package:http/http.dart' as http;

class DeleteAccount extends StatefulWidget {
  const DeleteAccount({Key? key}) : super(key: key);

  @override
  _DeleteAccountState createState() => _DeleteAccountState();
}

class _DeleteAccountState extends State<DeleteAccount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Delete Account"),
        leading: IconButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const HomeDrawer(),
              ),
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
    );
  }
}
/*
Future<User?> deleteUser(String firstName, String lastName) async {
  var url = Uri.parse("http://localhost:8081/users/$id");
  var response = await http.delete(
    url,
    headers: <String, String>{"Content-Type": "application/json;charset=UTF-8"},
  );
  return User.fromJson(jsonDecode(response.body));
}*/
