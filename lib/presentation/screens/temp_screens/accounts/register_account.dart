import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/domain/entity/account.dart';

class RegisterAccount extends StatefulWidget {
  const RegisterAccount({Key? key}) : super(key: key);

  @override
  _RegisterAccountState createState() => _RegisterAccountState();
}

class _RegisterAccountState extends State<RegisterAccount> {
  final double minimumPadding = 5.0;

  TextEditingController userIdController = TextEditingController();
  TextEditingController titleController = TextEditingController();

  Account? account;

  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = Theme.of(context).textTheme.subtitle2;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register Account"),
      ),
      body: Form(
        child: Padding(
          padding: EdgeInsets.all(minimumPadding * 2),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: userIdController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter User Id";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "User Id",
                    hintText: "Enter User Id",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: titleController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Account Title";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Account Title",
                    hintText: "Enter Account Title",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                child: const Text("Submit"),
                onPressed: () async {
                  String title = titleController.text;
                  String userId = userIdController.text;
                  Account? acc = await registerAccount(
                    userId,
                    title,
                    context,
                  );
                  userIdController.text = '';
                  titleController.text = '';
                  setState(() {
                    account = acc;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Future<Account?> registerAccount(
    String userId,
    String title,
    BuildContext context,
    ) async {
  var url = "http://localhost:8081/account";
  var response = await http.post(
    Uri.parse(url),
    headers: <String, String>{"Content-Type": "application/json"},
    body: jsonEncode(<String, String>{
      "userId": userId,
      "title": title,
    }),
  );
  String responseString = response.body;
  if (response.statusCode == 200) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return MyAlertDialog(
          title: "Backend Response",
          content: responseString,
        );
      },
    );
  }
}

class MyAlertDialog extends StatelessWidget {
  final String title;
  final String content;
  final List<Widget> actions;

  const MyAlertDialog({
    Key? key,
    required this.title,
    required this.content,
    this.actions = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title, style: Theme.of(context).textTheme.subtitle2),
      actions: actions,
      content: Text(content, style: Theme.of(context).textTheme.bodyText1),
    );
  }
}
