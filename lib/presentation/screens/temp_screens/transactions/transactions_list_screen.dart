import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/entity/user.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:splitwise/presentation/screens/temp_screens/transactions/delete_transaction.dart';
import 'package:splitwise/presentation/screens/temp_screens/transactions/update_transaction.dart';

class TransactionsListScreen extends StatelessWidget {
  const TransactionsListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Transactions"),
      ),
      body: FutureBuilder<List<Transaction>>(
        future: _getTransactions(),
        builder:
            (BuildContext context, AsyncSnapshot<List<Transaction>> snapshot) {
          if (!snapshot.hasData) {
            return const Center(child: CircularProgressIndicator());
          } else {
            return ListView.builder(
              itemCount: snapshot.data?.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                title: Text(
                  "${snapshot.data?[index].customer.customerName}",
                ),
                subtitle: Text(
                  "${snapshot.data?[index].description} - ${snapshot.data?[index].amount}",
                ),
                leading: const CircleAvatar(
                  backgroundColor: Colors.blueAccent,
                ),
                trailing: Text(snapshot.data?[index].type.name ?? ""),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return DetailsPage(transaction: snapshot.data![index]);
                      },
                    ),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }

  Future<List<Transaction>> _getTransactions() async {
    var url = Uri.parse("http://localhost:8081/transaction/");
    var response = await http.get(url);
    var jsonData = json.decode(response.body);

    LogManager.info("jsonData = $jsonData");

    List<Transaction> transactions = [];

    for (var t in jsonData) {
      Transaction transaction = Transaction(
        id: t["id"],
        amount: t["amount"],
        description: t["description"],
        type: mapToTransactionType(t["type"]),
        status: mapToTransactionStatus(t["status"]),
        timeStamp: DateTime.parse(t["timeStamp"]),
        account: Account(
          id: t["srcAccount"]["id"],
          title: t["srcAccount"]["title"],
          user: User(
            id: t["srcAccount"]["user"]["id"],
            name: t["srcAccount"]["user"]["name"],
            phoneNumber: t["srcAccount"]["user"]["phoneNumber"],
            initials: t["srcAccount"]["user"]["initials"],
          ),
          status: t["srcAccount"]["status"],
          signedUp: _mapToSignedUpBool(t["srcAccount"]["signedUp"]),
        ),
        customer: Customer(
          id: t["customer"]["id"],
          account: Account(
            id: t["customer"]["account"]["id"],
            title: t["customer"]["account"]["title"],
            user: User(
              id: t["customer"]["account"]["user"]["id"],
              name: t["customer"]["account"]["user"]["name"],
              phoneNumber: t["customer"]["account"]["user"]["phoneNumber"],
              initials: t["customer"]["account"]["user"]["initials"],
            ),
            status: t["customer"]["account"]["status"],
            signedUp: t["customer"]["account"]["signedUp"],
          ),
          customerName: t["customer"]["customerName"],
          phoneNumber: t["customer"]["phoneNumber"],
        ),
      );

      transactions.add(transaction);
    }

    LogManager.info("transactions = $transactions");
    return transactions;
  }

  bool _mapToSignedUpBool(int value) {
    if (value == 1) {
      return true;
    } else {
      return false;
    }
  }
}

class DetailsPage extends StatefulWidget {
  final Transaction transaction;

  const DetailsPage({Key? key, required this.transaction}) : super(key: key);

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.transaction.description),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return UpdateTransaction(transaction: widget.transaction);
                  },
                ),
              );
            },
            icon: const Icon(
              Icons.edit,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: Text(
        "Description: ${widget.transaction.description} Amount: ${widget.transaction.amount} Type: ${widget.transaction.type.name}\n"
        "SrcAccount = ${widget.transaction.account.title} DestAccount = ${widget.transaction.customer.customerName}",
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          deleteTransaction(widget.transaction);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const DeleteTransaction()),
          );
        },
        child: const Icon(Icons.delete),
        backgroundColor: Colors.pink,
      ),
    );
  }

  void deleteTransaction(Transaction transaction) async {
    final url =
        Uri.parse("http://localhost:8081/transaction/${transaction.id}");
    final request = http.Request("DELETE", url);
    request.headers
        .addAll(<String, String>{"Content-type": "application/json"});
    request.body = jsonEncode(transaction);
    final response = await request.send();
  }
}
