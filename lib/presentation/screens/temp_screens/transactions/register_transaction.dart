import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';

class RegisterTransaction extends StatefulWidget {
  const RegisterTransaction({Key? key}) : super(key: key);

  @override
  _RegisterTransactionState createState() => _RegisterTransactionState();
}

class _RegisterTransactionState extends State<RegisterTransaction> {
  final double minimumPadding = 5.0;

  TextEditingController destAccountIdController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController amountController = TextEditingController();
  TextEditingController typeController = TextEditingController();

  Transaction? transaction;

  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = Theme.of(context).textTheme.subtitle2;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register Transaction"),
      ),
      body: Form(
        child: Padding(
          padding: EdgeInsets.all(minimumPadding * 2),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: destAccountIdController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter your customer Id";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Your Customer's account Id",
                    hintText: "Your Customer's account Id",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: descriptionController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter your Description";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Description",
                    hintText: "Description",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: amountController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Amount";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Amount",
                    hintText: "Amount",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: typeController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Transaction Type";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Transaction Type",
                    hintText: "Transaction Type",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                child: const Text("Submit"),
                onPressed: () async {
                  String destAccountId = destAccountIdController.text;
                  String description = descriptionController.text;
                  String amount = amountController.text;
                  String type = typeController.text;
                  Transaction? t = await registerTransaction(
                    int.parse(destAccountId),
                    description,
                    double.parse(amount),
                    mapToTransactionType(type),
                    context,
                  );
                  destAccountIdController.text = '';
                  descriptionController.text = '';
                  amountController.text = '';
                  typeController.text = '';
                  setState(() {
                    transaction = t;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Future<Transaction?> registerTransaction(
  int destAccountId,
  String description,
  double amount,
  TransactionType type,
  BuildContext context,
) async {
  var url = "http://localhost:8081/transaction";
  var response = await http.post(
    Uri.parse(url),
    headers: <String, String>{"Content-Type": "application/json"},
    body: jsonEncode(<String, dynamic>{
      "description": description,
      "amount": amount,
      "type": type.name,
      "status": _mapTransactionStatusByType(type).name,
      "notes": "notes",
      "imagePath": "imagePath",
      "srcAccountId": 1,
      "destAccountId": destAccountId,
    }),
  );
  String responseString = response.body;
  if (response.statusCode == 200) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return MyAlertDialog(
          title: "Backend Response",
          content: responseString,
        );
      },
    );
  }
}

TransactionStatus _mapTransactionStatusByType(TransactionType type) {
  TransactionStatus status;
  if (type == TransactionType.DEBIT) {
    status = TransactionStatus.DEBT;
  } else if (type == TransactionType.CREDIT) {
    status = TransactionStatus.SURPLUS;
  } else {
    status = TransactionStatus.SETTLED;
  }
  return status;
}

class MyAlertDialog extends StatelessWidget {
  final String title;
  final String content;
  final List<Widget> actions;

  const MyAlertDialog({
    Key? key,
    required this.title,
    required this.content,
    this.actions = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title, style: Theme.of(context).textTheme.subtitle2),
      actions: actions,
      content: Text(content, style: Theme.of(context).textTheme.bodyText1),
    );
  }
}
