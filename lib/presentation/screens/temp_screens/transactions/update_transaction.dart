import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/domain/entity/UpdateCustomTransactionDto.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/entity/user.dart';
import 'package:splitwise/presentation/screens/temp_screens/home_drawer.dart';
import 'package:splitwise/presentation/screens/temp_screens/user/register_user.dart';

class UpdateTransaction extends StatefulWidget {
  final Transaction transaction;

  @override
  _UpdateTransactionState createState() => _UpdateTransactionState();

  const UpdateTransaction({Key? key, required this.transaction})
      : super(key: key);
}

class _UpdateTransactionState extends State<UpdateTransaction> {
  final double minimumPadding = 5.0;

  late Transaction? transaction;
  late TextEditingController descriptionController;
  late TextEditingController amountController;
  late TextEditingController typeController;
  final bool _isEnabled = true;
  late Future<List<User>> employees;

  @override
  void initState() {
    super.initState();
    transaction = widget.transaction;
    descriptionController =
        TextEditingController(text: transaction?.description);
    amountController =
        TextEditingController(text: transaction?.amount.toString());
    typeController = TextEditingController(text: transaction?.type.name);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = Theme.of(context).textTheme.subtitle2;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Update Transaction"),
        leading: IconButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const HomeDrawer()),
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: Form(
        child: Padding(
          padding: EdgeInsets.all(minimumPadding * 2),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: descriptionController,
                  enabled: _isEnabled,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Description";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Description",
                    hintText: "Enter Your Description",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: amountController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Amount";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Amount",
                    hintText: "Enter your Amount",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: typeController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Your Transaction Type";
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "Transaction Type",
                    hintText: "Enter your Transaction Type",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                child: const Text("Update Details"),
                onPressed: () async {
                  String description = descriptionController.text;
                  String amount = amountController.text;
                  TransactionType type =
                      mapToTransactionType(typeController.text);

                  UpdateCustomTransactionDto t = UpdateCustomTransactionDto(
                    transactionId: transaction?.id ?? 0,
                    description: description,
                    amount: double.parse(amount),
                    type: type,
                    status: _mapTransactionStatusByType(type),
                    imagePath: 'imagePath',
                    notes: 'notes',
                  );

                  Transaction? tr = await updateTransaction(t, context);
                  setState(() {
                    transaction = tr;
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  TransactionStatus _mapTransactionStatusByType(TransactionType type) {
    TransactionStatus status;
    if (type == TransactionType.DEBIT) {
      status = TransactionStatus.DEBT;
    } else if (type == TransactionType.CREDIT) {
      status = TransactionStatus.SURPLUS;
    } else {
      status = TransactionStatus.SETTLED;
    }
    return status;
  }
}

Future<Transaction?> updateTransaction(
  UpdateCustomTransactionDto transaction,
  BuildContext context,
) async {
  var url = Uri.parse("http://localhost:8081/transaction/${transaction.transactionId}");
  var response = await http.put(
    url,
    headers: <String, String>{"Content-Type": "application/json"},
    body: jsonEncode(<String, dynamic>{
      "description": transaction.description,
      "amount": transaction.amount,
      "type": transaction.type.name,
      "status": _mapTransactionStatusByType(transaction.type).name,
      "notes": transaction.notes,
      "imagePath": transaction.imagePath,
    }),
  );
  String responseString = response.body;
  if (response.statusCode == 200) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return MyAlertDialog(
          title: "Backend Response",
          content: responseString,
        );
      },
    );
  }
}

TransactionStatus _mapTransactionStatusByType(TransactionType type) {
  TransactionStatus status;
  if (type == TransactionType.DEBIT) {
    status = TransactionStatus.DEBT;
  } else if (type == TransactionType.CREDIT) {
    status = TransactionStatus.SURPLUS;
  } else {
    status = TransactionStatus.SETTLED;
  }
  return status;
}

