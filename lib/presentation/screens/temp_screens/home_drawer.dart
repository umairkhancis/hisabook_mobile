import 'package:flutter/material.dart';
import 'package:splitwise/presentation/screens/temp_screens/accounts/accounts_list_screen.dart';
import 'package:splitwise/presentation/screens/temp_screens/accounts/register_account.dart';
import 'package:splitwise/presentation/screens/temp_screens/transactions/register_transaction.dart';
import 'package:splitwise/presentation/screens/temp_screens/transactions/transactions_list_screen.dart';
import 'package:splitwise/presentation/screens/temp_screens/user/register_user.dart';
import 'package:splitwise/presentation/screens/temp_screens/user/users_list_screen.dart';

class HomeDrawer extends StatefulWidget {
  const HomeDrawer({Key? key}) : super(key: key);

  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  final double minimumPadding = 5.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("User Management"),
      ),
      body: const Center(
        child: Text("Welcome to User Management App"),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: minimumPadding),
          children: [
            const DrawerHeader(
              child: Text("Hisabook"),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: const Text("Register User"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const RegisterUser(),
                  ),
                );
              },
            ),
            ListTile(
              title: const Text("Get Users"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const UsersListScreen(),
                  ),
                );
              },
            ),

            ListTile(
              title: const Text("Register Account"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const RegisterAccount(),
                  ),
                );
              },
            ),
            ListTile(
              title: const Text("Get Accounts"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const AccountsListScreen(),
                  ),
                );
              },
            ),

            ListTile(
              title: const Text("Register Transaction"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const RegisterTransaction(),
                  ),
                );
              },
            ),
            ListTile(
              title: const Text("Get Transactions"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const TransactionsListScreen(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
