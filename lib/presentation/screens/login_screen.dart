import 'package:flutter/material.dart';
import 'package:splitwise/presentation/screens/forgot_password_screen.dart';
import 'package:splitwise/routes.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String _emailAddress = "";
  String _password = "";
  bool changeButton = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        elevation: 0,
        backgroundColor: Colors.white30,
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Text(
                "Welcome back to Hisabook!",
                style: Theme.of(context).textTheme.headline6,
              ),
              const SizedBox(height: 16.0),
              _buildEmailAddress(),
              _buildPassword(),
              const SizedBox(height: 40.0),
              _buildLogInButton(),
              const SizedBox(height: 32.0),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const ForgotPassword(),
                    ),
                  );
                },
                child: const Text(
                  "Forgot your password?",
                  textScaleFactor: 1.1,
                  style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  TextFormField _buildEmailAddress() {
    return TextFormField(
      autofocus: true,
      cursorColor: Colors.green,
      cursorWidth: 1.0,
      validator: (value) {
        const pattern =
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
        final regExp = RegExp(pattern);

        if (value == null) {
          return "Enter an email address";
        } else if (!regExp.hasMatch(value)) {
          return "Enter a valid email address";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _emailAddress = value ?? "";
      },
      decoration: const InputDecoration(
        labelText: "Email address",
        labelStyle: TextStyle(
          color: Colors.black54,
          fontWeight: FontWeight.bold,
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.green),
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
      ),
    );
  }

  TextFormField _buildPassword() {
    return TextFormField(
      autofocus: true,
      cursorColor: Colors.green,
      obscureText: true,
      cursorWidth: 1.0,
      validator: (value) {
        if (value == null) {
          return "Enter a password";
        } else if (value.isEmpty) {
          return "Enter a Password";
        } else if (value.length < 7) {
          return "Password must be atleast 7 characters long";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _password = value ?? "";
      },
      decoration: const InputDecoration(
        labelText: "Password",
        labelStyle: TextStyle(
          color: Colors.black54,
          fontWeight: FontWeight.bold,
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.green),
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
      ),
    );
  }

  Material _buildLogInButton() {
    return Material(
      color: Colors.green,
      borderRadius: BorderRadius.circular(changeButton ? 50 : 4),
      child: InkWell(
        onTap: () => moveToHomeScreen(context),
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 500),
          width: changeButton ? 50 : 150,
          height: 50,
          alignment: Alignment.center,
          child: changeButton
              ? const Icon(
                  Icons.done,
                  color: Colors.white,
                )
              : const Text(
                  "Log in",
                  textScaleFactor: 1.1,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
        ),
      ),
    );
  }

  moveToHomeScreen(BuildContext context) async {
    final bool isValid = _formKey.currentState?.validate() ?? false;
    FocusScope.of(context).unfocus();
    if (isValid) {
      _formKey.currentState?.save();
      setState(() {
        changeButton = true;
      });
      print("Email: $_emailAddress\nPassword: $_password");
      await Future.delayed(const Duration(milliseconds: 500));
      Navigator.pushNamed(context, MyRoutes.homePageRoute);
      await Future.delayed(const Duration(milliseconds: 500));
      setState(() {
        changeButton = false;
      });
    }
  }
}
