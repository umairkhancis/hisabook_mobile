import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:splitwise/presentation/bloc/add_more_customer/add_more_customer_cubit.dart';
import 'package:splitwise/routes.dart';

class VerifyContactInfoScreen extends StatefulWidget {
  final Contact contact;

  const VerifyContactInfoScreen({Key? key, required this.contact})
      : super(key: key);

  @override
  _VerifyContactInfoScreenState createState() =>
      _VerifyContactInfoScreenState();
}

class _VerifyContactInfoScreenState extends State<VerifyContactInfoScreen> {
  late AddMoreCustomerCubit cubit;

  @override
  void initState() {
    cubit = BlocProvider.of<AddMoreCustomerCubit>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AddMoreCustomerCubit, AddMoreCustomerState>(
      listener: _onStateListener,
      child: Scaffold(
        appBar: _buildAppBar(context),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              ListTile(
                leading: Container(
                  width: 60,
                  height: 60,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.green,
                  ),
                ),
                title: Text(
                  widget.contact.displayName ?? "",
                  style: const TextStyle(letterSpacing: 0.5),
                ),
                subtitle: Text(
                  widget.contact.phones?.first.value ?? "",
                  style: const TextStyle(letterSpacing: 0.5),
                ),
              ),
              const SizedBox(height: 20),
              Text(
                "Your customer will be added.\n"
                "You can start adding transactions right away.",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.caption,
              ),
              const SizedBox(height: 40),
              MaterialButton(
                minWidth: Size.infinite.width,
                height: 45,
                color: Colors.deepOrangeAccent,
                onPressed: () async {
                  await cubit.addContactAsCustomer(widget.contact);
                  Navigator.pop(context);
                },
                child: const Text(
                  "Finish",
                  textScaleFactor: 1.1,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          "Verify contact info",
          style: Theme.of(context).textTheme.headline6,
        ),
      );
  }

  void _onStateListener(BuildContext context, AddMoreCustomerState state) {
    if (state is NewContactAdded) {
      Navigator.pop(context);
      Navigator.pushNamed(context, MyRoutes.homePageRoute);
    }
  }
}
