import 'package:flutter/material.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          "Reset password",
          style: Theme.of(context).textTheme.headline6,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          children: [
            const Text(
              "Reset your password",
              textScaleFactor: 1.5,
              style: TextStyle(fontWeight: FontWeight.w900),
            ),
            const SizedBox(height: 20),
            Text(
              "Enter your email address or phone number and "
              "we'll send you a link to reset your password.",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.subtitle1,
            ),
            const SizedBox(height: 30),
            Text(
              "Your email address",
              style: Theme.of(context).textTheme.subtitle1,
            ),
            SizedBox(
              width: 260,
              height: 40,
              child: TextFormField(
                autofocus: true,
                cursorColor: Colors.green,
                cursorWidth: 1.0,
                cursorHeight: 24.0,
                validator: (value) {
                  const pattern =
                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
                  final regExp = RegExp(pattern);

                  if (value == null) {
                    return "Enter an email address";
                  } else if (!regExp.hasMatch(value)) {
                    return "Enter a valid email address";
                  } else {
                    return null;
                  }
                },
                /*onSaved: (value) {
                  _emailAddress = value ?? "";
                },*/
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.green),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
            MaterialButton(
              onPressed: () {},
              color: Colors.green,
              child: const Text(
                "Reset password",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
