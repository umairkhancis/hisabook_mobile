import 'package:flutter/material.dart';
import 'package:splitwise/domain/entity/customer.dart';

class CustomerWidget extends StatelessWidget {
  final Customer customer;
  final String statusText;
  final Color? statusTextColor;
  final Function() onTap;

  const CustomerWidget({
    Key? key,
    required this.customer,
    required this.statusText,
    required this.statusTextColor,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        onTap();
      },
      leading: Container(
        width: 45,
        height: 45,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.green,
        ),
      ),
      title: Text(customer.customerName),
      trailing: Text(
        statusText,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: statusTextColor,
          fontWeight: FontWeight.bold,
          fontSize: Theme.of(context).textTheme.subtitle1?.fontSize,
        ),
      ),
    );
  }
}
