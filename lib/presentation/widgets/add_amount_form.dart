import 'package:flutter/material.dart';

class AddAmountForm extends StatefulWidget {
  final Function(String) onAmountChanged;
  final TextEditingController amountController;
  final String amountInitialValue;
  final String? Function(String? value) validator;
  final String? Function(String? text) onSaved;

  const AddAmountForm({
    Key? key,
    required this.amountController,
    required this.onAmountChanged,
    required this.amountInitialValue,
    required this.validator,
    required this.onSaved,
  }) : super(key: key);

  @override
  State<AddAmountForm> createState() => _AddAmountFormState();
}

class _AddAmountFormState extends State<AddAmountForm> {
  final addAmountFormController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 50,
          height: 50,
          child: Center(
            child: Text(
              "PKR",
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black12),
            borderRadius: const BorderRadius.all(Radius.circular(4)),
            color: Colors.white,
            boxShadow: const [
              BoxShadow(offset: Offset(0, 2), color: Colors.grey),
            ],
          ),
        ),
        const SizedBox(width: 20),
        SizedBox(
          width: 250,
          height: 50,
          child: TextFormField(
            autofocus: true,
            onChanged: (text) => widget.onAmountChanged(text),
            keyboardType: TextInputType.number,
            cursorHeight: 30,
            controller: addAmountFormController
              ..text = widget.amountInitialValue,
            validator: (text) => widget.validator(text),
            onSaved: (text) => widget.onSaved(text),
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 30,
              color: Colors.black54,
            ),
            decoration: const InputDecoration(
              hintText: "0.00",
              hintStyle: TextStyle(
                color: Colors.grey,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
              enabledBorder: UnderlineInputBorder(),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
