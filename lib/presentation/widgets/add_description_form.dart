import 'package:flutter/material.dart';

class AddDescriptionForm extends StatefulWidget {
  final TextEditingController descriptionController;
  final Function(String) onDescriptionChanged;
  final String descriptionInitialValue;
  // final String? Function(String? value) validator;
  final String? Function(String? text) onSaved;

  const AddDescriptionForm({
    Key? key,
    required this.descriptionController,
    required this.onDescriptionChanged,
    required this.descriptionInitialValue,
    // required this.validator,
    required this.onSaved,
  }) : super(key: key);

  @override
  State<AddDescriptionForm> createState() => _AddDescriptionFormState();
}

class _AddDescriptionFormState extends State<AddDescriptionForm> {
  final addDescriptionFormController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 50,
          height: 50,
          child: const Icon(
            Icons.notes,
            color: Colors.black,
          ),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black12),
            borderRadius: const BorderRadius.all(Radius.circular(4)),
            boxShadow: const [
              BoxShadow(offset: Offset(0, 2), color: Colors.grey),
            ],
            color: Colors.white,
          ),
        ),
        const SizedBox(width: 20),
        SizedBox(
          width: 250,
          height: 50,
          child: TextFormField(
            controller: addDescriptionFormController
              ..text = widget.descriptionInitialValue,
            onChanged: (text) => widget.onDescriptionChanged(text),
            // validator: (text) => widget.validator(text),
            onSaved: (text) => widget.onSaved(text),
            autocorrect: true,
            decoration: const InputDecoration(
              hintText: "Enter a description",
              hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
              enabledBorder: UnderlineInputBorder(),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
