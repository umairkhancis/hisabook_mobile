import 'package:flutter/material.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/presentation/dto/AddTransactionArgs.dart';
import 'package:splitwise/routes.dart';

class AddTransactionButton extends StatelessWidget {
  final Customer? customer;

  const AddTransactionButton({Key? key, this.customer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        Navigator.pushNamed(
          context,
          MyRoutes.addTransactionRoute,
          arguments: AddTransactionArgs(
              customer: customer, type: TransactionType.DEBIT),
        );
        // Navigator.pushNamed(context, MyRoutes.addTransactionRoute););
      },
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.green[700]),
        shape: MaterialStateProperty.all(const StadiumBorder()),
        fixedSize: MaterialStateProperty.all(const Size(190, 50)),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Row(
          children: const [
            Icon(Icons.sticky_note_2_outlined),
            Text(
              "  Add transaction",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
