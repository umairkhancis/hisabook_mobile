import 'package:flutter/material.dart';
import 'package:splitwise/routes.dart';

class AddMoreCustomersButton extends StatelessWidget {
  const AddMoreCustomersButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {
        Navigator.pushNamed(context, MyRoutes.addMoreCustomersRoute);
      },
      style: ButtonStyle(
        side: MaterialStateProperty.all(
          const BorderSide(
            color: Colors.green,
            width: 1,
          ),
        ),
        fixedSize: MaterialStateProperty.all(const Size(230, 40)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(
            Icons.person_add_alt,
            color: Colors.green,
          ),
          SizedBox(width: 10),
          Text(
            "Add more customers",
            style: TextStyle(
              color: Colors.green,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
