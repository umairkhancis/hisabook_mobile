import 'package:flutter/material.dart';
import 'package:splitwise/domain/dto/account_transaction_report_item_dto.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/presentation/dto/TransactionDetailsScreenArgs.dart';
import 'package:splitwise/routes.dart';

class TransactionsListing extends StatelessWidget {
  final List<AccountTransactionReportItemDto> transactions;

  const TransactionsListing({
    Key? key,
    required this.transactions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (transactions.isNotEmpty) {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: transactions.length,
        // physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) =>
          _buildListItem(context, transactions[index]),
        // children: _buildTransactionsList(context, transactions),
      );
    } else {
      return Center(
        child: Text(
          "No transactions here yet.",
          style: Theme.of(context).textTheme.subtitle1,
        ),
      );
    }
  }

  List<Widget> _buildTransactionsList(
    BuildContext context,
    List<AccountTransactionReportItemDto> transactions,
  ) {
    return transactions.map((item) => _buildListItem(context, item)).toList();
  }

  Widget _buildListItem(
    BuildContext context,
    AccountTransactionReportItemDto item,
  ) {
    return Column(
      children: [
        (item.transactionStatus == TransactionStatus.SETTLED)
            ? _buildSettledTransactionListItem(context, item)
            : _buildUnsettledTransactionListItem(context, item),
        const Divider(height: 8.0),
      ],
    );
  }

  ListTile _buildUnsettledTransactionListItem(
    BuildContext context,
    AccountTransactionReportItemDto item,
  ) {
    return ListTile(
      onTap: () {
        Navigator.pushNamed(
          context,
          MyRoutes.transactionDetailsRoute,
          arguments: TransactionDetailsScreenArgs(id: item.id),
        );
      },
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    item.dateText,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.caption,
                  ),
                  const SizedBox(width: 16),
                  Container(
                    width: 50,
                    height: 50,
                    color: Colors.black12,
                    child: const Icon(Icons.notes),
                  ),
                ],
              ),
              const SizedBox(width: 16),
              SizedBox(
                width: 130,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.description,
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Text(
                      item.subtext,
                      style: const TextStyle(color: Colors.grey, fontSize: 14),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Text(
            item.transactionStatusText,
            textAlign: TextAlign.end,
            style:
                TextStyle(color: item.transactionStatusColor, fontSize: 16.0),
          ),
        ],
      ),
    );
  }

  ListTile _buildSettledTransactionListItem(
    BuildContext context,
    AccountTransactionReportItemDto item,
  ) {
    return ListTile(
      onTap: () {
        Navigator.pushNamed(
          context,
          MyRoutes.transactionDetailsRoute,
          arguments: TransactionDetailsScreenArgs(id: item.id),
        );
      },
      title: Row(
        children: [
          Text(
            item.dateText,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.caption,
          ),
          const SizedBox(width: 16),
          const SizedBox(
            width: 50,
            height: 50,
            child: Icon(Icons.payments_outlined, color: Colors.green),
          ),
          const SizedBox(width: 16),
          Text(
            item.subtext,
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}
