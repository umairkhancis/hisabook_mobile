import 'package:flutter/widgets.dart';
import 'package:splitwise/main.dart';

class RouteAwareness extends StatefulWidget {
  final Widget child;
  final Function onForeground;
  final Function onBackground;

  const RouteAwareness({
    Key? key,
    required this.child,
    required this.onForeground,
    required this.onBackground,
  }) : super(key: key);

  @override
  _RouteAwarenessState createState() => _RouteAwarenessState();
}

class _RouteAwarenessState extends State<RouteAwareness> with RouteAware {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  @override
  void didPush() {}

  @override
  void didPushNext() {
    /// Called when a new route has been pushed, and the current route is no longer visible; onPause
    widget.onBackground();
  }

  @override
  void didPopNext() {
    /// Called when the top route has been popped off, and the current route shows up; onResume
    widget.onForeground();
  }

  @override
  void didPop() {}

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}
