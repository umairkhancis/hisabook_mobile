import 'package:flutter/material.dart';
import 'package:splitwise/domain/dto/account_balance_report_item_dto.dart';
import 'package:splitwise/presentation/dto/AccountDetailsScreenArgs.dart';
import 'package:splitwise/routes.dart';

import 'account_widget.dart';

class CustomersList extends StatelessWidget {
  final List<CustomerBalanceReportItemDto> items;

  const CustomersList({Key? key, required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: _buildList(context, items));
  }

  List<Widget> _buildList(
      BuildContext context, List<CustomerBalanceReportItemDto> items) {
    return items.map((item) => _buildListItem(context, item)).toList();
  }

  Widget _buildListItem(
      BuildContext context, CustomerBalanceReportItemDto item) {
    return Column(
      children: [
        CustomerWidget(
          customer: item.customer,
          statusText: item.balanceStatusText,
          statusTextColor: item.balanceStatusTextColor,
          onTap: () => Navigator.pushNamed(
            context,
            MyRoutes.customerDetailsRoute,
            arguments: CustomerDetailsScreenArgs(customer: item.customer),
          ),
        ),
        const SizedBox(height: 8.0),
      ],
    );
  }
}
