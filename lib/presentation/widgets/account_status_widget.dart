import 'package:flutter/material.dart';

class AccountStatusWidget extends StatelessWidget {
  final String overallSurplusAccountStatusText;
  final String overallDebtAccountStatusText;
  final String overallSettledAcountStatusText;

  const AccountStatusWidget({
    Key? key,
    required this.overallSurplusAccountStatusText,
    required this.overallDebtAccountStatusText,
    required this.overallSettledAcountStatusText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: (MediaQuery.of(context).size.width / 2) - 24,
          height: 70,
          decoration: BoxDecoration(
            color: Colors.red[50],
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.all(8.0 * 2),
                child: const Icon(
                  Icons.arrow_circle_down_outlined,
                  color: Colors.red,
                  size: 24,
                ),
              ),
              Text(
                overallDebtAccountStatusText,
                style: const TextStyle(
                  fontSize: 14.0,
                  // fontWeight: FontWeight.bold,
                  color: Colors.red,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: (MediaQuery.of(context).size.width / 2) - 24,
          height: 70,
          decoration: BoxDecoration(
            color: Colors.green[50],
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.all(8.0 * 2),
                child: const Icon(
                  Icons.arrow_circle_up_outlined,
                  color: Colors.green,
                  size: 24,
                ),
              ),
              Text(
                overallSurplusAccountStatusText,
                style: const TextStyle(
                  fontSize: 14.0,
                  // fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
/*Visibility(
              visible: overallDebtAccountStatusText.isNotEmpty,
              child: Text(
                overallDebtAccountStatusText,
                style: const TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red,
                ),
              ),
            ),
            Visibility(
              visible: overallSurplusAccountStatusText.isNotEmpty,
              child: Text(
                overallSurplusAccountStatusText,
                style: const TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            ),
            Visibility(
              visible: overallSettledAcountStatusText.isNotEmpty,
              child: Text(
                overallSettledAcountStatusText,
                style: const TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),*/
