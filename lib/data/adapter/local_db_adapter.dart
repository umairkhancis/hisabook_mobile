import 'dart:async';

import 'package:path/path.dart' as p;
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:sqflite/sqflite.dart';

abstract class LocalDatabase {
  Future init();

  Future<List> get(String query);

  Future<int?> insert(String query);

  Future update(String query);

  Future delete(String query);

  Future transaction(Function block);

  void close();
}

class SqfliteLocalDatabase extends LocalDatabase {
  Database? _database;

  SqfliteLocalDatabase();

  final DateTime _dateTime = DateTime.now();

  @override
  Future init() async {
    if (_database == null) {
      final path = await getDatabasesPath();
      final dbFile = p.join(path, 'ledger.db');
      _database =
          await openDatabase(dbFile, version: 1, onCreate: _onDatabaseCreate);
    }
  }

  @override
  Future<List> get(String query) async {
    final resultSet = await _database?.rawQuery(query);
    final list = resultSet?.map((item) => item).toList() ?? [];
    return list;
  }

  @override
  void close() {
    LogManager.info("closingDatabase");
    _database?.close();
  }

  FutureOr<void> _onDatabaseCreate(Database db, int version) async {
    LogManager.info("onDatabaseCreate");
    await _createDbSchema(db);
    await _seedInitialData(db);
  }

  Future<void> _createDbSchema(Database db) async {
    LogManager.info("Creating Users Table");

    await db.execute(
      'CREATE TABLE Users (id INTEGER PRIMARY KEY, name TEXT, phoneNumber TEXT, initials TEXT)',
    );

    LogManager.info("Creating Accounts Table");

    await db.execute(
      'CREATE TABLE Accounts (id INTEGER PRIMARY KEY, title TEXT, holder INTEGER, status TEXT, signedUp BIT)',
    );

    LogManager.info("Creating Transactions Table");

    await db.execute(
      'CREATE TABLE Transactions (id INTEGER PRIMARY KEY, amount FLOAT, description TEXT, accountId INTEGER, customerId INTEGER, type TEXT, date TEXT, status TEXT, image TEXT)',
    );

    LogManager.info("Creating TransactionsAuditLog Table");

    await db.execute(
      'CREATE TABLE TransactionsAuditLog (id INTEGER PRIMARY KEY, transactionId INTEGER, type TEXT, userId INTEGER, date TEXT)',
    );

    LogManager.info("Creating Customers Table");

    await db.execute(
      'CREATE TABLE Customers (id INTEGER PRIMARY KEY, accountId INTEGER, customerName TEXT, phoneNumber TEXT)',
    );
  }

  Future<void> _seedInitialData(Database db) async {
    // await _seedUserData(db);
    // await _seedAccountData(db);
    // await _seedTransactionData(db);
  }

  Future<void> _seedUserData(Database db) async {
    await db.transaction((txn) async {
      int id1 = await txn.rawInsert(
        'INSERT INTO Users(name, phoneNumber, initials) '
        'VALUES("Usman", "+971 58 892 0465", "USK")',
      );

      LogManager.info('inserted user: $id1');

      int id2 = await txn.rawInsert(
        'INSERT INTO Users(name, phoneNumber, initials) '
        'VALUES("Umair", "+971 50 932 9306", "UMK")',
      );

      LogManager.info('inserted user: $id2');

      int id3 = await txn.rawInsert(
        'INSERT INTO Users(name, phoneNumber, initials) '
        'VALUES("Aamir", "+1 745 890 3469", "AAK")',
      );

      LogManager.info('inserted user: $id3');
    });
  }

  Future<void> _seedAccountData(Database db) async {
    await db.transaction((txn) async {
      int id1 = await txn.rawInsert(
        'INSERT INTO Accounts(title, holder, status) VALUES("Usman Ahmed Khan", 1, ACTIVE)',
      );

      LogManager.info('inserted account: $id1');
    });

    await db.transaction((txn) async {
      int id2 = await txn.rawInsert(
        'INSERT INTO Accounts(title, holder, status) VALUES("Umair Ahmed Khan", 2, ACTIVE)',
      );

      LogManager.info('inserted account: $id2');
    });

    await db.transaction((txn) async {
      int id3 = await txn.rawInsert(
        'INSERT INTO Accounts(title, holder, status) VALUES("Aamir Ahmed Khan", 3, ACTIVE)',
      );

      LogManager.info('inserted account: $id3');
    });
  }

  Future<void> _seedTransactionData(Database db) async {
    await db.transaction((txn) async {
      int id1 = await txn.rawInsert(
        'INSERT INTO Transactions(amount, description, accountId, customerId, type, date, status, image) '
        'VALUES(100.0, "Chapli Kabab", 1, 1, "CREDIT", "$_dateTime", "SURPLUS", "")',
      );

      await txn.rawInsert(
        'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
        'VALUES(1, "INSERT", 1, "$_dateTime")',
      );

      LogManager.info('inserted transaction: $id1');
    });

    await db.transaction((txn) async {
      int id2 = await txn.rawInsert(
        'INSERT INTO Transactions(amount, description, accountId, customerId, type, date, status, image) '
        'VALUES(100.0, "Pizza Hut", 1, 2, "CREDIT", "$_dateTime", "SURPLUS", "")',
      );

      await txn.rawInsert(
        'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
        'VALUES(2, "INSERT", 1, "$_dateTime")',
      );

      LogManager.info('inserted transaction: $id2');
    });

    await db.transaction((txn) async {
      int id3 = await txn.rawInsert(
        'INSERT INTO Transactions(amount, description, accountId, customerId, type, date, status, image) '
        'VALUES(200.0, "Hardees", 1, 1, "DEBIT", "$_dateTime", "DEBT", "")',
      );

      await txn.rawInsert(
        'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
        'VALUES(3, "INSERT", 1, "$_dateTime")',
      );

      LogManager.info('inserted transaction: $id3');
    });

    await db.transaction((txn) async {
      int id4 = await txn.rawInsert(
        'INSERT INTO Transactions(amount, description, account, customer, type, date, status, image) '
        'VALUES(500.0, "Kababjees", 1, 2, "DEBIT", "$_dateTime", "DEBT", "")',
      );

      await txn.rawInsert(
        'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
        'VALUES(4, "INSERT", 1, "$_dateTime")',
      );

      LogManager.info('inserted transaction: $id4');
    });
  }

  @override
  Future<int?> insert(String query) async {
    return await _database?.transaction((txn) async {
      int id1 = await txn.rawInsert(query);
      LogManager.info('inserted transaction: $id1');
      return id1;
    });
  }

  @override
  Future<void> delete(String query) async {
    await _database?.transaction((txn) async {
      int id1 = await txn.rawDelete(query);

      LogManager.info('deleted transaction: $id1');
    });
  }

  @override
  Future<void> update(String query) async {
    await _database?.transaction((txn) async {
      int id1 = await txn.rawUpdate(query);

      LogManager.info('updated transaction: $id1');
    });
  }

  @override
  Future transaction(Function block) async {
    await _database?.transaction((txn) async {
      block();
    });
  }
}
