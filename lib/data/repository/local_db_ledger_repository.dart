import 'dart:core';
import 'dart:io';

import 'package:contacts_service/contacts_service.dart';
import 'package:splitwise/data/adapter/local_db_adapter.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/audit_log_type.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_audit.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/entity/user.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';

int _currentAccountUserId = 1;

class LocalDbLedgerRepository extends LedgerRepository {
  LocalDatabase? _database;

  @override
  Future init({LocalDatabase? db}) async {
    if (_database == null) {
      _database = db ?? SqfliteLocalDatabase();
      await _database?.init();
    }
  }

  @override
  Future<List<TransactionAudit>> getAuditLogById(int id) async {
    final List resultSet = await _database?.get(
            'SELECT * FROM TransactionsAuditLog WHERE transactionId = $id') ??
        [];
    final List<TransactionAudit> transactionsAuditLog = [];

    for (var item in resultSet) {
      final t = await _mapResultSetRowToTransactionAudit(item);
      transactionsAuditLog.add(t);
    }

    LogManager.info("TransactionsAuditLog = $transactionsAuditLog");

    return transactionsAuditLog;
  }

  @override
  Future addTransaction(
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  ) async {
    Account? account = await getCurrentUserAccount();
    // Account? destAccount = await getAccountByName(accountHolderName);
    final Customer? customer = await getCustomerByName(customerName);

    await _database?.transaction(() async {
      final transactionId = await _database?.insert(
        "INSERT INTO Transactions(amount, description, accountId, customerId, type, date, status, image) "
        "VALUES($amount, \"$description\", ${account?.id}, ${customer?.id}, \"${type.name}\", \"$date\", \"${status.name}\", \"${image?.path ?? ""}\")",
      );

      await _database?.insert(
        'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
        'VALUES($transactionId, "INSERT", ${account?.user.id}, "${DateTime.now()}")',
      );
    });
  }

  @override
  Future updateTransaction(
    int id,
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  ) async {
    Account? srcAccount = await getCurrentUserAccount();

    await _database?.insert(
      "UPDATE Transactions SET amount = $amount, description = \"$description\", type = \"${type.name}\", status = \"${status.name}\", date = \"$date\", image = \"${image?.path ?? ""}\" WHERE id = $id",
    );

    await _database?.insert(
      'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
      'VALUES($id, "UPDATE", ${srcAccount?.user.id}, "${DateTime.now()}")',
    );
  }

  @override
  Future deleteTransaction(int id, DateTime date) async {
    await _database?.insert(
      "UPDATE Transactions SET status = \"DELETED\", date = \"$date\" WHERE id = $id",
    );
  }

  @override
  Future addContact(Contact selectedContact) async {
    await _database?.insert(
      ' INSERT INTO Users(name, phoneNumber, initials) VALUES("${selectedContact.displayName}", "${selectedContact.phones?.first.value}", "${selectedContact.initials()}") ',
    );

    final User? user = await getUserByName(selectedContact.displayName ?? "");

    await _database?.insert(
      'INSERT INTO Accounts(title, holder) VALUES("${selectedContact.displayName}", (SELECT id FROM Users WHERE name = "${user?.name}"))',
    );
  }

  @override
  Future<double> getBalanceByCustomer(Customer customer) async {
    final accountTransactions = await getTransactionsByCustomer(customer);
    if (accountTransactions.isEmpty) return 0;

    final accountBalance = accountTransactions
        .map((t) => _mapTransactionTypeToSignedAmount(t))
        .reduce((accumlation, amount) => accumlation + amount);

    return accountBalance;
  }

  @override
  Future<Account?> getCurrentUserAccount() async {
    return getAccountById(_currentAccountUserId);
  }

  @override
  Future<List<Contact>> getPhoneContacts() async {
    final List<Contact> contacts =
        await ContactsService.getContacts(withThumbnails: true);
    return contacts;
  }

  User _mapResultSetRowToUser(user) {
    return User(
      id: user["id"],
      name: user["name"],
      phoneNumber: user["phoneNumber"],
      initials: user["initials"],
    );
  }

  Future<Account> _mapResultSetRowToAccount(account) async {
    final User? user = await getUserById(account["holder"]);
    return Account(
      id: account["id"],
      title: account["title"],
      user: User(
        id: user?.id ?? 0,
        name: user?.name ?? "",
        phoneNumber: user?.phoneNumber ?? "",
        initials: user?.initials ?? "",
      ),
      status: account["status"],
      signedUp: true,
    );
  }

  Future<Customer> _mapResultSetRowToCustomer(customer) async {
    final Account? account = await getAccountById(customer["accountId"]);

    if (account == null) throw Exception("Account not found!");

    return Customer(
      id: customer["id"],
      customerName: customer["customerName"],
      phoneNumber: customer["phoneNumber"],
      account: account,
    );
  }

  Future<Transaction> _mapResultSetRowToTransaction(transaction) async {
    final srcAccount = await getAccountById(transaction["srcAccount"]);
    final customer = await getCustomerById(transaction["customer"]);

    if (srcAccount == null || customer == null) {
      throw Exception("Src account or dest account not found!");
    }

    return Transaction(
      id: transaction["id"],
      amount: transaction["amount"],
      description: transaction["description"],
      account: srcAccount,
      customer: customer,
      type: mapToTransactionType(transaction["type"]),
      timeStamp: DateTime.parse(transaction["date"]),
      // timeStamp: DateTime.now(),
      status: mapToTransactionStatus(transaction["status"]),
      image: File(transaction["image"]),
    );
  }

  @override
  Future deleteAccount(int customerId) async {
    final Account? account = await getAccountById(customerId);
    if (account == null) {
      throw Exception("Account not found!");
    }
    User? user = await getUserByAccount(customerId);
    LogManager.info("user = $user");

    await _database?.delete("DELETE FROM Accounts WHERE id = $customerId");
    LogManager.info("Account deleted");
    await _database
        ?.delete("DELETE FROM Transactions WHERE destAccount = $customerId");
    LogManager.info("Transactions deleted");
    await _database?.delete("DELETE FROM Users WHERE id = ${user?.id}");
    LogManager.info("User deleted");
  }

  @override
  Future<List<Transaction>> getAllTransactions() async {
    final List resultSet =
        await _database?.get('SELECT * FROM Transactions') ?? [];
    final List<Transaction> transactions = [];

    for (var item in resultSet) {
      final t = await _mapResultSetRowToTransaction(item);
      transactions.add(t);
    }

    LogManager.info("Transactions = $transactions");

    return transactions;
  }

  @override
  Future<Transaction?> getTransactionById(int id) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Transactions WHERE id = $id') ?? [];

    if (resultSet.isEmpty) return null;

    final transaction = await _mapResultSetRowToTransaction(resultSet[0]);

    return transaction;
  }

  @override
  Future<List<Transaction>> getTransactionsByCustomer(Customer customer) async {
    final List resultSet = await _database?.get(
            'SELECT * FROM Transactions WHERE destAccount = ${customer.id} AND status != "DELETED" ') ??
        [];

    if (resultSet.isEmpty) return [];

    final List<Transaction> transactionsByAccount = [];
    for (var item in resultSet) {
      final t = await _mapResultSetRowToTransaction(item);
      transactionsByAccount.add(t);
    }

    return transactionsByAccount;
  }

  @override
  Future<Account?> getAccountById(int id) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Accounts WHERE id = $id') ?? [];

    if (resultSet.isEmpty) return null;

    final account = await _mapResultSetRowToAccount(resultSet[0]);

    return account;
  }

  @override
  Future<Account?> getAccountByName(String name) async {
    final User? user = await getUserByName(name);

    final List resultSet = await _database
            ?.get('SELECT * FROM Accounts WHERE holder = ${user?.id}') ??
        [];

    if (resultSet.isEmpty) return null;

    final account = await _mapResultSetRowToAccount(resultSet[0]);

    return account;
  }

  @override
  Future<List<User>> getUsers() async {
    final List resultSet = await _database?.get('SELECT * FROM Users') ?? [];

    final List<User> users =
        resultSet.map((row) => _mapResultSetRowToUser(row)).toList();

    LogManager.info("Users = $users");

    return users;
  }

  @override
  Future<User?> getUserById(int id) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Users WHERE id = $id') ?? [];
    if (resultSet.isEmpty) return null;

    return _mapResultSetRowToUser(resultSet[0]);
  }

  @override
  Future<User?> getUserByName(String name) async {
    final List resultSet =
        await _database?.get("SELECT * FROM Users WHERE name = \"$name\" ") ??
            [];
    if (resultSet.isEmpty) return null;

    return _mapResultSetRowToUser(resultSet[0]);
  }

  double _mapTransactionTypeToSignedAmount(Transaction t) {
    return t.type == TransactionType.DEBIT ? -t.amount : t.amount;
  }

  Future<User?> getUserByAccount(int accountId) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Accounts WHERE id = $accountId') ??
            [];
    if (resultSet.isEmpty) return null;

    final List<Account> accounts = [];

    for (var item in resultSet) {
      final account = await _mapResultSetRowToAccount(item);
      accounts.add(account);
    }

    User user = accounts.first.user;

    return user;
  }

  Future<TransactionAudit> _mapResultSetRowToTransactionAudit(
    transactionAudit,
  ) async {
    final User? srcUser = await getUserById(transactionAudit["userId"]);
    // final destAccount = await getAccountById(transaction["destAccount"]);

    if (srcUser == null) {
      throw Exception("Src User not found!");
    }

    Transaction? transaction = await getTransactionById(
      transactionAudit["transactionId"],
    );

    if (transaction == null) {
      throw Exception("Transaction not found!");
    }

    return TransactionAudit(
      id: transactionAudit["id"],
      transaction: transaction,
      type: mapToTransactionAudit(transactionAudit["type"]),
      user: srcUser,
      timeStamp: DateTime.parse(transactionAudit["date"]),
    );
  }

  @override
  Future loginOrSignupUser(
    String fullName,
    String accountName,
    String phoneNumber,
  ) {
    // TODO: implement loginOrSignupUser
    throw UnimplementedError();
  }

  @override
  int getCurrentAccountUserId() => _currentAccountUserId;

  @override
  Future<void> sendPhoneNumberRequest(String phoneNumber) {
    // TODO: implement sendPhoneNumberRequest
    throw UnimplementedError();
  }

  @override
  Future<Customer?> getCustomerById(int id) async {
    final List resultSet = await _database
            ?.get('SELECT * FROM Customers WHERE customerName = $id') ??
        [];

    if (resultSet.isEmpty) return null;

    final customer = await _mapResultSetRowToCustomer(resultSet[0]);

    return customer;
  }

  @override
  Future<List<Customer>> getCustomers() async {
    final List resultSet = await _database?.get('SELECT * FROM Accounts') ?? [];
    final List<Customer> customers = [];

    for (var item in resultSet) {
      final Customer customer = await _mapResultSetRowToCustomer(item);
      customers.add(customer);
    }

    LogManager.info("Customers = $customers");

    return customers;
  }

  @override
  Future<List<Customer>> getCustomersByName(String customerName) async {
    final List<Customer> customers = [];

    final List resultSet = await _database?.get(
          'SELECT * FROM Customers WHERE accountId = $_currentAccountUserId AND customerName LIKE "$customerName"',
        ) ??
        [];

    if (resultSet.isNotEmpty) {
      for (var item in resultSet) {
        LogManager.info("customerItem = $item");
        final Customer customer = await _mapResultSetRowToCustomer(item);
        LogManager.info("customerInstance = $customer");
        customers.add(customer);
      }
    }

    return customers;
  }

  Future<Customer?> getCustomerByName(String customerName) async {
    final List resultSet = await _database?.get(
            'SELECT * FROM Customers WHERE customerName = "$customerName"') ??
        [];

    if (resultSet.isEmpty) return null;

    final customer = await _mapResultSetRowToCustomer(resultSet[0]);

    return customer;
  }

  @override
  Future<bool> doesUserExist(String phoneNumber) {
    // TODO: implement doesUserExist
    throw UnimplementedError();
  }
}
