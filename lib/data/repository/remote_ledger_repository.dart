import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:contacts_service/contacts_service.dart';
import 'package:http/http.dart' as http;
import 'package:splitwise/data/adapter/local_db_adapter.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/audit_log_type.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_audit.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/entity/user.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';

int _currentAccountUserId = 1;

// 192.168.8.109
// http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/

class RemoteLedgerRepository extends LedgerRepository {
  LocalDatabase? _database;

  @override
  Future init({LocalDatabase? db}) async {
    LogManager.info("init::_currentAccountUserId = $_currentAccountUserId");
    if (_database == null) {
      _database = db ?? SqfliteLocalDatabase();
      await _database?.init();
    }

    await _fetchAndSave();
  }

  @override
  int getCurrentAccountUserId() => _currentAccountUserId;

  @override
  Future<List<TransactionAudit>> getAuditLogById(int id) async {
    final List resultSet = await _database?.get(
          'SELECT * FROM TransactionsAuditLog WHERE transactionId = $id',
        ) ??
        [];
    final List<TransactionAudit> transactionsAuditLog = [];

    for (var item in resultSet) {
      final t = await _mapResultSetRowToTransactionAudit(item);
      transactionsAuditLog.add(t);
    }

    return transactionsAuditLog;
  }

  @override
  Future addTransaction(
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  ) async {
    Account? account = await getCurrentUserAccount();
    final Customer? customer = await getCustomerByName(customerName);

    String stringDate = date.toString();
    String stringTimeUserSent = stringDate.substring(10);

    DateTime now = DateTime.now();
    String timeStampNow = now.toString();
    String stringTimeNow = timeStampNow.substring(10);

    String updatedtimeStamp = stringDate.replaceAll(
      stringTimeUserSent,
      stringTimeNow,
    );

    LogManager.info("updatedtimeStamp = $updatedtimeStamp");

    // 2022-01-07T17:18:40.280375
    // 2022-01-07 17:18:40.280375

    var url =
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/transaction";
    // "http://192.168.8.103:8081/transaction";
    var encode = jsonEncode(<String, dynamic>{
      "amount": double.parse(amount),
      "description": description,
      "type": type.name,
      "status": status.name,
      "timestamp": updatedtimeStamp,
      "imagePath": image?.path ?? "",
      "accountId": account?.id,
      "customerId": customer?.id ?? 0,
    });

    LogManager.info("encode: $encode");
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{"Content-Type": "application/json"},
      body: encode,
    );

    await _database?.transaction(() async {
      final transactionId = await _database?.insert(
        'INSERT INTO Transactions(amount, description, accountId, customerId, type, date, status, image) '
        'VALUES($amount, "$description", ${account?.id}, ${customer?.id}, "${type.name}", "$updatedtimeStamp", "${status.name}", "${image?.path ?? ""}")',
      );

      await _database?.insert(
        'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
        'VALUES($transactionId, "INSERT", ${account?.user.id}, "${DateTime.now()}")',
      );
    });
    LogManager.info(response.body);
  }

  @override
  Future updateTransaction(
    int id,
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  ) async {
    Account? account = await getCurrentUserAccount();
    Customer? customer = await getCustomerByName(customerName);

    // 01234567890
    // 2022-01-26 15:18:31.015652
    // test2 -> add -> 2022-02-02 14:48:18.728902

    String stringDate = date.toString();
    String stringTimeUserSent = stringDate.substring(10);

    print("stringTimeUserSent = $stringTimeUserSent");

    DateTime now = DateTime.now();
    String timeStampNow = now.toString();
    String stringTimeNow = timeStampNow.substring(10);

    print("stringTimeNow = $stringTimeNow");

    String updatedtimeStamp = stringDate.replaceAll(
      stringTimeUserSent,
      stringTimeNow,
    );

    print("updateTransaction::transactionTimeStamp = $updatedtimeStamp");

    var url = Uri.parse(
      "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/transaction/$id",
    );
    await http.put(
      url,
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, dynamic>{
        "amount": double.parse(amount),
        "description": description,
        "type": type.name,
        "timestamp": updatedtimeStamp,
        "status": status.name,
        "imagePath": image?.path ?? "",
        "customerId": customer?.id,
      }),
    );

    await _database?.update(
      'UPDATE Transactions SET amount = $amount, description = "$description", type = "${type.name}", status = "${status.name}", date = "$updatedtimeStamp", image = "${image?.path ?? ""}" WHERE id = $id',
    );

    await _database?.insert(
      'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
      'VALUES($id, "UPDATE", ${account?.user.id}, "$now")',
    );
  }

  @override
  Future deleteTransaction(int id, DateTime date) async {
    Transaction? transaction = await getTransactionById(id);

    Account? account = await getCurrentUserAccount();
    Customer? customer = transaction!.customer;

    // 01234567890
    // "2022-02-02 19:06:41.322311"
    // test2 -> add -> 2022-02-02 14:48:18.728902

    String stringDate = date.toString();
    String stringTimeUserSent = stringDate.substring(10);

    DateTime now = DateTime.now();
    String timeStampNow = now.toString();
    String stringTimeNow = timeStampNow.substring(10);

    String updatedtimeStamp = stringDate.replaceAll(
      stringTimeUserSent,
      stringTimeNow,
    );

    var url = Uri.parse(
      "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/transaction/$id",
    );
    var response = await http.put(
      url,
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, dynamic>{
        "amount": transaction.amount,
        "description": transaction.description,
        "type": transaction.type.name,
        "timestamp": updatedtimeStamp,
        "status": "DELETED",
        "imagePath": transaction.image?.path ?? "",
        "customerId": customer.id,
      }),
    );

    await _database?.update(
      'UPDATE Transactions SET '
      'amount = ${transaction.amount}, '
      'description = "${transaction.description}", '
      'type = "${transaction.type.name}", '
      'status = "DELETED", '
      'date = "$updatedtimeStamp", '
      'image = "${transaction.image?.path ?? ""}" WHERE id = $id',
    );

    await _database?.insert(
      'INSERT INTO TransactionsAuditLog(transactionId, type, userId, date) '
      'VALUES($id, "DELETE", ${account?.user.id}, "$now")',
    );

    String responseString = response.body;
    LogManager.info("responseString = $responseString");
  }

  @override
  Future addContact(Contact selectedContact) async {
    final String name = selectedContact.displayName ?? "_";
    final String phoneNumber = selectedContact.phones?.first.value ?? "_";
    // final String initials = selectedContact.initials();

    await _addCustomer(name, phoneNumber);
  }

  @override
  Future<bool> doesUserExist(String phoneNumber) async {
    final List resultSet = await _database?.get(
          'SELECT * FROM Users WHERE phoneNumber = "$phoneNumber"',
        ) ??
        [];

    if (resultSet.isNotEmpty) {
      LogManager.info("User does exist");
      final User user = _mapResultSetRowToUser(resultSet[0]);
      final Account? account = await getAccountByUserId(user.id);
      await _loginUser(
        user.name,
        account?.title ?? "_",
        phoneNumber,
        user.initials,
      );
      return true;
    } else {
      LogManager.info("User does not exist");
      // await _signUpUser(fullName, accountName, phoneNumber, initials);
      return false;
    }
  }

  @override
  Future loginOrSignupUser(
    String fullName,
    String accountName,
    String phoneNumber,
  ) async {
    final String initials =
        (fullName.isNotEmpty == true ? getInitials(fullName) : "")
            .toUpperCase();

    // _fetchAndSave();

    final List resultSet = await _database?.get(
          'SELECT * FROM Users WHERE phoneNumber = "$phoneNumber"',
        ) ??
        [];

    if (resultSet.isNotEmpty) {
      // final User user = _mapResultSetRowToUser(resultSet[0]);
      await _loginUser(fullName, accountName, phoneNumber, initials);
      LogManager.info("_currentAccountUserId = $_currentAccountUserId");
    } else {
      await _signUpUser(fullName, accountName, phoneNumber, initials);
    }
  }

  Future _loginUser(
    String fullName,
    String accountName,
    String phoneNumber,
    String initials,
  ) async {
    final User? user = await getUserByPhoneNumber(phoneNumber);
    var url =
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/user/${user?.id}";
    await http.put(
      Uri.parse(url),
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, String>{
        "name": fullName,
        "phone_number": phoneNumber,
        "initials": initials,
      }),
    );

    await _database?.update(
      'UPDATE Users SET name = "$fullName", phoneNumber = "$phoneNumber", initials = "$initials" WHERE id = ${user?.id}',
    );

    await _loginAccountByUser(user, accountName);
  }

  Future _signUpUser(
    String fullName,
    String accountName,
    String phoneNumber,
    String initials,
  ) async {
    var url =
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/user";
    await http.post(
      Uri.parse(url),
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, String>{
        "name": fullName,
        "phoneNumber": phoneNumber,
        "initials": initials,
      }),
    );

    await _database?.insert(
      ' INSERT INTO Users(name, phoneNumber, initials) '
      'VALUES("$fullName", "$phoneNumber", "$initials") ',
    );

    final User? user = await getUserByPhoneNumber(phoneNumber);

    await _signUpAccountByUser(user, accountName);

    _currentAccountUserId = user?.id ?? 0;
  }

  Future<void> _loginAccountByUser(User? user, String accountName) async {
    final Account? account = await getAccountByUserId(user?.id ?? 0);
    var url =
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/account/${account?.id}";
    await http.put(
      Uri.parse(url),
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, dynamic>{
        // "userId": user?.id ?? 0,
        "title": accountName,
        "status": "ACTIVE",
        "sign_up": true,
      }),
    );

    await _database?.update(
      'UPDATE Accounts SET title = "$accountName", holder = ${user?.id}, status = "ACTIVE", signedUp = 1 WHERE id = ${account?.id}',
    );

    _currentAccountUserId = account?.id ?? 0;
  }

  Future<void> _signUpAccountByUser(User? user, String accountName) async {
    var url =
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/account";
    await http.post(
      Uri.parse(url),
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, dynamic>{
        "userId": user?.id ?? 0,
        "title": accountName,
        "status": "ACTIVE",
        "signedUp": true,
      }),
    );

    await _database?.insert(
      'INSERT INTO Accounts(title, holder, status, signedUp) '
      'VALUES("$accountName", ${user?.id ?? 0}, "ACTIVE", 1)',
    );

    final Account? account = await getAccountByUserId(user?.id ?? 0);

    _currentAccountUserId = account?.id ?? 0;
  }

  Future _addCustomer(String name, String phoneNumber) async {
    final List? resultSet = await _database?.get(
      'SELECT * FROM Customers WHERE phoneNumber = "$phoneNumber"',
    );

    if (resultSet == null) return;

    if (resultSet.isEmpty) {
      var url =
          "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/customer";
      await http.post(
        Uri.parse(url),
        headers: <String, String>{"Content-Type": "application/json"},
        body: jsonEncode(<String, dynamic>{
          "accountId": _currentAccountUserId,
          "customerName": name,
          "phoneNumber": phoneNumber,
        }),
      );

      await _database?.insert(
        'INSERT INTO Customers(accountId, customerName, phoneNumber) '
        'VALUES ($_currentAccountUserId, "$name", "$phoneNumber")',
      );
    }
  }

  @override
  Future<void> sendPhoneNumberRequest(String phoneNumber) async {
    var url =
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/phoneNumber";
    await http.post(
      Uri.parse(url),
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(<String, String>{
        "phoneNumber": phoneNumber,
      }),
    );
  }

  @override
  Future<double> getBalanceByCustomer(Customer customer) async {
    final accountTransactions = await getTransactionsByCustomer(customer);
    if (accountTransactions.isEmpty) return 0;

    final accountBalance = accountTransactions
        .map((t) => _mapTransactionTypeToSignedAmount(t))
        .reduce((accumlation, amount) => accumlation + amount);

    return accountBalance;
  }

  @override
  Future<Account?> getCurrentUserAccount() async {
    return getAccountById(_currentAccountUserId);
  }

  @override
  Future<List<Contact>> getPhoneContacts() async {
    final List<Contact> contacts =
        await ContactsService.getContacts(withThumbnails: false);
    return contacts;
  }

  User _mapResultSetRowToUser(user) {
    return User(
      id: user["id"],
      name: user["name"],
      phoneNumber: user["phoneNumber"],
      initials: user["initials"],
    );
  }

  Future<Account> _mapResultSetRowToAccount(account) async {
    final User? user = await getUserById(account["holder"]);
    return Account(
      id: account["id"],
      title: account["title"],
      user: User(
        id: user?.id ?? 0,
        name: user?.name ?? "",
        phoneNumber: user?.phoneNumber ?? "",
        initials: user?.initials ?? "",
      ),
      status: mapToAccountStatus(account["status"]),
      signedUp: _mapToSignedUpBool(account["signedUp"]),
    );
  }

  Future<Customer> _mapResultSetRowToCustomer(customer) async {
    final Account? account = await getAccountById(customer["accountId"]);

    if (account == null) {
      throw Exception("Account not found!");
    }

    return Customer(
      id: customer["id"],
      account: account,
      customerName: customer["customerName"],
      phoneNumber: customer["phoneNumber"],
    );
  }

  Future<Transaction> _mapResultSetRowToTransaction(transaction) async {
    final srcAccount = await getAccountById(transaction["accountId"]);
    final customer = await getCustomerById(transaction["customerId"]);

    if (srcAccount == null || customer == null) {
      throw Exception("Src account or dest account not found!");
    }

    return Transaction(
      id: transaction["id"],
      amount: transaction["amount"],
      description: transaction["description"],
      account: srcAccount,
      customer: customer,
      type: mapToTransactionType(transaction["type"]),
      timeStamp: DateTime.parse(transaction["date"]),
      status: mapToTransactionStatus(transaction["status"]),
      image: File(transaction["image"]),
    );
  }

  bool _mapToSignedUpBool(int value) {
    if (value == 1) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future deleteAccount(int customerId) async {
    final Customer? customer = await getCustomerById(customerId);
    if (customer == null) {
      throw Exception("Account not found!");
    }
    User? user = await getUserByAccount(customerId);

    List<Transaction> transactions = await getTransactionsByCustomer(customer);

    // await deleteTransactionsByAccount(transactions, accountId);
    for (var t in transactions) {
      await deleteTransaction(t.id, DateTime.now());
    }

    var url = Uri.parse(
      "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/customer/${customer.id}",
    );
    var response = await http.put(
      url,
      headers: <String, String>{"Content-Type": "application/json"},
      body: jsonEncode(customer),
    );

    await _database?.update(
      'UPDATE Accounts SET status = "NOT_ACTIVE" WHERE id = $customerId',
    );
    LogManager.info(response.body);

    await deleteUser(user);
  }

  /*Future<void> deleteTransactionsByAccount(
    List<Transaction> transactions,
    int accountId,
  ) async {
    for (var t in transactions) {
      deleteTransaction(t.id, DateTime.now());
    }
  }*/

  Future<void> deleteUser(User? user) async {
    final url = Uri.parse(
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/users/${user?.id}");
    final request = http.Request("DELETE", url);
    request.headers
        .addAll(<String, String>{"Content-type": "application/json"});
    await request.send();

    await _database?.delete("DELETE FROM Users WHERE id = ${user?.id}");
  }

  @override
  Future<List<Transaction>> getAllTransactions() async {
    final List resultSet = await _database?.get(
          'SELECT * FROM Transactions WHERE accountId = $_currentAccountUserId',
        ) ??
        [];
    final List<Transaction> transactions = [];

    for (var item in resultSet) {
      final t = await _mapResultSetRowToTransaction(item);
      print(t.timeStamp);
      transactions.add(t);
    }

    return transactions;
  }

  @override
  Future<Transaction?> getTransactionById(int id) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Transactions WHERE id = $id') ?? [];

    if (resultSet.isEmpty) return null;

    final transaction = await _mapResultSetRowToTransaction(resultSet[0]);

    return transaction;
  }

  @override
  Future<List<Transaction>> getTransactionsByCustomer(Customer customer) async {
    final List resultSet = await _database?.get(
          'SELECT * FROM Transactions WHERE customerId = ${customer.id} AND status != "DELETED" ',
        ) ??
        [];

    if (resultSet.isEmpty) return [];

    final List<Transaction> transactionsByAccount = [];
    for (var item in resultSet) {
      final Transaction t = await _mapResultSetRowToTransaction(item);
      transactionsByAccount.add(t);
    }

    return transactionsByAccount;
  }

  @override
  Future<List<Customer>> getCustomers() async {
    final List<Customer> customers = [];

    final List resultSet = await _database?.get(
          'SELECT * FROM Customers WHERE accountId = $_currentAccountUserId',
        ) ??
        [];

    if (resultSet.isNotEmpty) {
      for (var item in resultSet) {
        final Customer customer = await _mapResultSetRowToCustomer(item);
        customers.add(customer);
      }
    }

    return customers;
  }

  @override
  Future<Customer?> getCustomerById(int id) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Customers WHERE id = $id') ?? [];

    if (resultSet.isEmpty) return null;

    final Customer customer = await _mapResultSetRowToCustomer(resultSet[0]);

    return customer;
  }

  @override
  Future<List<Customer>> getCustomersByName(String customerName) async {
    final List<Customer> customers = [];

    final List resultSet = await _database?.get(
          'SELECT * FROM Customers WHERE accountId = $_currentAccountUserId AND customerName LIKE "%$customerName%"',
        ) ??
        [];

    if (resultSet.isNotEmpty) {
      for (var item in resultSet) {
        final Customer customer = await _mapResultSetRowToCustomer(item);
        customers.add(customer);
      }
    }

    return customers;
  }

  Future<Customer?> getCustomerByName(String customerName) async {
    final List resultSet = await _database?.get(
            'SELECT * FROM Customers WHERE customerName = "$customerName"') ??
        [];

    if (resultSet.isEmpty) return null;

    final Customer customer = await _mapResultSetRowToCustomer(resultSet[0]);

    return customer;
  }

  @override
  Future<Account?> getAccountById(int id) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Accounts WHERE id = $id') ?? [];

    if (resultSet.isEmpty) return null;

    final Account account = await _mapResultSetRowToAccount(resultSet[0]);

    return account;
  }

  Future<Account?> getAccountByUserId(int userId) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Accounts WHERE holder = $userId') ??
            [];

    if (resultSet.isEmpty) return null;

    final account = await _mapResultSetRowToAccount(resultSet[0]);

    return account;
  }

  @override
  Future<Account?> getAccountByName(String name) async {
    final User? user = await getUserByName(name);

    final List resultSet = await _database?.get(
          'SELECT * FROM Accounts WHERE holder = ${user?.id}',
        ) ??
        [];

    if (resultSet.isEmpty) return null;

    return _mapResultSetRowToAccount(resultSet[0]);
  }

  @override
  Future<List<User>> getUsers() async {
    List<User> users = [];

    return users;
  }

  @override
  Future<User?> getUserById(int id) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Users WHERE id = $id') ?? [];
    if (resultSet.isEmpty) return null;

    return _mapResultSetRowToUser(resultSet[0]);
  }

  @override
  Future<User?> getUserByName(String name) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Users WHERE name = "$name" ') ?? [];
    if (resultSet.isEmpty) return null;

    return _mapResultSetRowToUser(resultSet[0]);
  }

  Future<User?> getUserByPhoneNumber(String phoneNumber) async {
    final List resultSet = await _database?.get(
          'SELECT * FROM Users WHERE phoneNumber = "$phoneNumber"',
        ) ??
        [];
    if (resultSet.isEmpty) return null;

    return _mapResultSetRowToUser(resultSet[0]);
  }

  double _mapTransactionTypeToSignedAmount(Transaction t) {
    return t.type == TransactionType.DEBIT ? -t.amount : t.amount;
  }

  Future<User?> getUserByAccount(int accountId) async {
    final List resultSet =
        await _database?.get('SELECT * FROM Accounts WHERE id = $accountId') ??
            [];
    if (resultSet.isEmpty) return null;

    final List<Account> accounts = [];

    for (var item in resultSet) {
      final account = await _mapResultSetRowToAccount(item);
      accounts.add(account);
    }

    User user = accounts.first.user;

    return user;
  }

  Future<TransactionAudit> _mapResultSetRowToTransactionAudit(
    transactionAudit,
  ) async {
    final User? srcUser = await getUserById(transactionAudit["userId"]);
    // final destAccount = await getAccountById(transaction["destAccount"]);

    if (srcUser == null) {
      throw Exception("Src User not found!");
    }

    Transaction? transaction = await getTransactionById(
      transactionAudit["transactionId"],
    );

    if (transaction == null) {
      throw Exception("Transaction not found!");
    }

    return TransactionAudit(
      id: transactionAudit["id"],
      transaction: transaction,
      type: mapToTransactionAudit(transactionAudit["type"]),
      user: srcUser,
      timeStamp: DateTime.parse(transactionAudit["date"]),
    );
  }

  Future fetchAndSaveUsers() async {
    var url = Uri.parse(
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/user/");
    var data = await http.get(url);
    var jsonData = json.decode(data.body);

    List<User> users = _mapUsersFromJson(jsonData);

    await _insertUsersIntoDb(users);
  }

  List<User> _mapUsersFromJson(dynamic jsonData) {
    List<User> users = [];

    for (var u in jsonData) {
      User user = User(
        id: u["id"],
        name: u["name"],
        phoneNumber: u["phoneNumber"],
        initials: u["initials"],
      );
      users.add(user);
    }
    return users;
  }

  Future _insertUsersIntoDb(List<User> users) async {
    for (final user in users) {
      await _database?.transaction(() async {
        final resultSet = await _database?.get(
          'Select * FROM Users WHERE id = "${user.id}"',
        );

        if (resultSet?.isEmpty == true) {
          await _database?.insert(
            'INSERT INTO Users(name, phoneNumber, initials) '
            'VALUES("${user.name}", "${user.phoneNumber}", "${user.initials}")',
          );
        }
      });
    }
  }

  Future fetchAndSaveAccounts() async {
    var url = Uri.parse(
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/account/");
    var response = await http.get(url);
    var jsonData = json.decode(response.body);

    List<Account> accounts = _mapAccountFromJson(jsonData);

    await _insertAccountsIntoDb(accounts);
  }

  List<Account> _mapAccountFromJson(dynamic jsonData) {
    List<Account> accounts = [];
    for (var acc in jsonData) {
      Account account = Account(
        id: acc["id"],
        title: acc["title"],
        user: User(
          id: acc["user"]["id"],
          name: acc["user"]["name"],
          phoneNumber: acc["user"]["phoneNumber"],
          initials: acc["user"]["initials"],
        ),
        status: mapToAccountStatus(acc["status"]),
        signedUp: acc["signUp"],
      );

      accounts.add(account);
    }

    return accounts;
  }

  Future _insertAccountsIntoDb(List<Account> accounts) async {
    for (final account in accounts) {
      await _database?.transaction(() async {
        final resultSet = await _database?.get(
          'Select * FROM Accounts WHERE id = "${account.id}"',
        );

        if (resultSet?.isEmpty == true) {
          await _database?.insert(
            'INSERT INTO Accounts(title, holder, status, signedUp) '
            'VALUES("${account.title}", ${account.user.id}, "${account.status.name}", ${_mapSignedUpboolToInt(account.signedUp)})',
          );
        }
      });
    }
  }

  Future fetchAndSaveCustomers() async {
    var url = Uri.parse(
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/customer/");
    var response = await http.get(url);
    var jsonData = json.decode(response.body);

    List<Customer> customers = _mapCustomersFromJson(jsonData);

    await _insertCustomersIntoDb(customers);
  }

  List<Customer> _mapCustomersFromJson(dynamic jsonData) {
    List<Customer> customers = [];

    for (var c in jsonData) {
      Customer customer = Customer(
        id: c["id"],
        customerName: c["customerName"],
        phoneNumber: c["phoneNumber"],
        account: Account(
          id: c["account"]["id"],
          title: c["account"]["title"],
          status: mapToAccountStatus(c["account"]["status"]),
          signedUp: c["account"]["signUp"],
          user: User(
            id: c["account"]["user"]["id"],
            name: c["account"]["user"]["name"],
            phoneNumber: c["account"]["user"]["phoneNumber"],
            initials: c["account"]["user"]["initials"],
          ),
        ),
      );

      customers.add(customer);
    }
    return customers;
  }

  Future _insertCustomersIntoDb(List<Customer> customers) async {
    for (final customer in customers) {
      await _database?.transaction(() async {
        final resultSet = await _database?.get(
          'Select * FROM Customers WHERE id = ${customer.id}',
        );

        if (resultSet?.isEmpty == true) {
          await _database?.insert(
            'INSERT INTO Customers(accountId, customerName, phoneNumber) '
            'VALUES(${customer.account.id}, "${customer.customerName}", "${customer.phoneNumber}")',
          );
        }
      });
    }
  }

  Future fetchAndSaveTransactions() async {
    var url = Uri.parse(
        "http://hisabook-env-1.eba-xjnnibfm.eu-west-1.elasticbeanstalk.com/transaction/");
    var response = await http.get(url);
    var jsonData = json.decode(response.body);

    List<Transaction> transactions = _mapTransactionsFromJson(jsonData);

    await _insertTransactionsIntoDb(transactions);
  }

  List<Transaction> _mapTransactionsFromJson(dynamic jsonData) {
    List<Transaction> transactions = [];

    for (var t in jsonData) {
      String creationDate = t["creationDate"];
      DateTime timeStamp = DateTime.parse(creationDate.replaceFirst('T', ' '));
      print("transaction date: $timeStamp");
      Transaction transaction = Transaction(
        id: t["id"],
        amount: t["amount"],
        description: t["description"],
        type: mapToTransactionType(t["type"]),
        status: mapToTransactionStatus(t["status"]),
        timeStamp: timeStamp,
        account: Account(
          id: t["account"]["id"],
          title: t["account"]["title"],
          user: User(
            id: t["account"]["user"]["id"],
            name: t["account"]["user"]["name"],
            phoneNumber: t["account"]["user"]["phoneNumber"],
            initials: t["account"]["user"]["initials"],
          ),
          status: mapToAccountStatus(t["account"]["status"]),
          signedUp: t["account"]["signUp"],
        ),
        customer: Customer(
          id: t["customer"]["id"],
          account: Account(
            id: t["customer"]["account"]["id"],
            title: t["customer"]["account"]["title"],
            user: User(
              id: t["customer"]["account"]["user"]["id"],
              name: t["customer"]["account"]["user"]["name"],
              phoneNumber: t["customer"]["account"]["user"]["phoneNumber"],
              initials: t["customer"]["account"]["user"]["initials"],
            ),
            status: mapToAccountStatus(t["account"]["status"]),
            signedUp: t["customer"]["account"]["signUp"],
          ),
          customerName: t["customer"]["customerName"],
          phoneNumber: t["customer"]["phoneNumber"],
        ),
      );

      transactions.add(transaction);
    }
    return transactions;
  }

  Future _insertTransactionsIntoDb(List<Transaction> transactions) async {
    for (final transaction in transactions) {
      await _database?.transaction(() async {
        final resultSet = await _database?.get(
          'Select * FROM Transactions WHERE id = ${transaction.id}',
        );

        if (resultSet?.isEmpty == true) {
          await _database?.insert(
            'INSERT INTO Transactions(amount, description, accountId, customerId, type, date, status, image) '
            'VALUES(${transaction.amount}, "${transaction.description}", ${transaction.account.id}, ${transaction.customer.id}, "${transaction.type.name}", "${transaction.timeStamp.toString()}", "${transaction.status.name}", "")',
          );
        }
      });
    }
  }

  Future _fetchAndSave() async {
    await fetchAndSaveUsers();
    await fetchAndSaveAccounts();
    await fetchAndSaveCustomers();
    await fetchAndSaveTransactions();
  }

  String getInitials(String fullName) => fullName.isNotEmpty
      ? fullName.trim().split(RegExp(' +')).map((s) => s[0]).take(2).join()
      : '';

  int _mapSignedUpboolToInt(bool value) {
    if (value == true) {
      return 1;
    } else {
      return 0;
    }
  }
}
