import 'dart:core';
import 'dart:io';

import 'package:contacts_service/contacts_service.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_audit.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/entity/user.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

int _currentAccountUserId = 1;

List<Transaction> _transactions = [];

List<Account> _accounts = [];

List<User> _users = [];

List<Customer> _customers = [];

class InMemoryLedgerRepository extends LedgerRepository {
  @override
  Future init() async {
    _currentAccountUserId = 1;

    _transactions = [
      Transaction(
        id: 1,
        amount: 100,
        description: "Chapli Kabab",
        account: const Account(
          id: 1,
          title: "Usman Ahmed Khan",
          user: User(
            id: 1,
            name: "Usman",
            phoneNumber: "+971 58 892 0465",
            initials: "USK",
          ),
          status: CustomerStatus.ACTIVE,
          signedUp: true,
        ),
        customer: const Customer(
          id: 1,
          customerName: "Umair Ahmed Khan",
          account: Account(
            id: 1,
            title: "Usman Ahmed Khan",
            user: User(
              id: 2,
              name: "Usman",
              phoneNumber: "+923462498393",
              initials: "USK",
            ),
            status: CustomerStatus.ACTIVE,
            signedUp: true,
          ),
          phoneNumber: "+971 50 932 9306",
        ),
        type: TransactionType.CREDIT,
        status: TransactionStatus.SURPLUS,
        timeStamp: DateTime.now(),
      ),
      Transaction(
        id: 2,
        amount: 100,
        description: "Pizza Hut",
        account: const Account(
          id: 1,
          title: "Usman Ahmed Khan",
          user: User(
            id: 1,
            name: "Usman",
            phoneNumber: "+971 58 892 0465",
            initials: "USK",
          ),
          status: CustomerStatus.ACTIVE,
          signedUp: true,
        ),
        customer: const Customer(
          id: 2,
          customerName: "Aamir Ahmed Khan",
          phoneNumber: "+971 50 932 9306",
          account: Account(
            id: 1,
            title: "Usman Ahmed Khan",
            user: User(
              id: 2,
              name: "Usman",
              phoneNumber: "+923462498393",
              initials: "USK",
            ),
            status: CustomerStatus.ACTIVE,
            signedUp: true,
          ),
        ),
        type: TransactionType.CREDIT,
        status: TransactionStatus.SURPLUS,
        timeStamp: DateTime.now(),
      ),
      Transaction(
        id: 3,
        amount: 200,
        description: "Hardees",
        account: const Account(
          id: 1,
          title: "Usman Ahmed Khan",
          user: User(
            id: 1,
            name: "Usman",
            phoneNumber: "+971 58 892 0465",
            initials: "USK",
          ),
          status: CustomerStatus.ACTIVE,
          signedUp: true,
        ),
        customer: const Customer(
          id: 1,
          customerName: "Umair Ahmed Khan",
          phoneNumber: "+971 50 932 9306",
          account: Account(
            id: 1,
            title: "Usman Ahmed Khan",
            user: User(
              id: 2,
              name: "Usman",
              phoneNumber: "+923462498393",
              initials: "USK",
            ),
            status: CustomerStatus.ACTIVE,
            signedUp: true,
          ),
        ),
        type: TransactionType.DEBIT,
        status: TransactionStatus.DEBT,
        timeStamp: DateTime.now(),
      ),
      Transaction(
        id: 4,
        amount: 500,
        description: "Kababjees",
        account: const Account(
          id: 1,
          title: "Usman Ahmed Khan",
          user: User(
            id: 1,
            name: "Usman",
            phoneNumber: "+971 58 892 0465",
            initials: "USK",
          ),
          status: CustomerStatus.ACTIVE,
          signedUp: true,
        ),
        customer: const Customer(
          id: 2,
          customerName: "Aamir Ahmed Khan",
          phoneNumber: "+971 50 932 9306",
          account: Account(
            id: 1,
            title: "Usman Ahmed Khan",
            user: User(
              id: 2,
              name: "Usman",
              phoneNumber: "+923462498393",
              initials: "USK",
            ),
            status: CustomerStatus.ACTIVE,
            signedUp: true,
          ),
        ),
        type: TransactionType.DEBIT,
        status: TransactionStatus.DEBT,
        timeStamp: DateTime.now(),
      ),
    ];

    _accounts = [
      const Account(
        id: 1,
        title: "Usman Ahmed Khan",
        user: User(
          id: 1,
          name: "Usman",
          phoneNumber: "+971 58 892 0465",
          initials: "USK",
        ),
        status: CustomerStatus.ACTIVE,
        signedUp: true,
      ),
      const Account(
        id: 2,
        title: "Umair Ahmed Khan",
        user: User(
          id: 2,
          name: "Umair",
          phoneNumber: "+971 50 932 9306",
          initials: "UMK",
        ),
        status: CustomerStatus.ACTIVE,
        signedUp: true,
      ),
      const Account(
        id: 3,
        title: "Aamir Ahmed Khan",
        user: User(
          id: 3,
          name: "Aamir",
          phoneNumber: "+1 745 890 3469",
          initials: "AAK",
        ),
        status: CustomerStatus.ACTIVE,
        signedUp: true,
      ),
    ];

    _customers = [
      const Customer(
        id: 1,
        account: Account(
          id: 1,
          title: "Usman Ahmed Khan",
          user: User(
            id: 1,
            name: "Usman",
            phoneNumber: "+923462498393",
            initials: "USK",
          ),
          status: CustomerStatus.ACTIVE,
          signedUp: true,
        ),
        customerName: "Umair Ahmed Khan",
        phoneNumber: "+1 745 890 3469",
      ),
      const Customer(
        id: 2,
        account: Account(
          id: 1,
          title: "Usman Ahmed Khan",
          user: User(
            id: 1,
            name: "Usman",
            phoneNumber: "+923462498393",
            initials: "USK",
          ),
          status: CustomerStatus.ACTIVE,
          signedUp: true,
        ),
        customerName: "Aamir Ahmed Khan",
        phoneNumber: "971 536 9306",
      ),
    ];

    _users = [
      const User(
        id: 1,
        name: "Usman",
        phoneNumber: "+971 58 892 0465",
        initials: "USK",
      ),
      const User(
        id: 2,
        name: "Umair",
        phoneNumber: "+971 50 932 9306",
        initials: "UMK",
      ),
      const User(
        id: 3,
        name: "Aamir",
        phoneNumber: "+1 745 890 3469",
        initials: "AAK",
      ),
    ];
  }

  @override
  Future<List<Transaction>> getAllTransactions() async => _transactions;

  Future<List<Account>> getCustomersAccounts() async => _accounts;

  @override
  Future<List<Transaction>> getTransactionsByCustomer(Customer customer) async {
    return _transactions.where((t) => t.customer == customer).toList();
  }

  @override
  Future<double> getBalanceByCustomer(Customer customer) async {
    final accountTransactions = await getTransactionsByCustomer(customer);
    if (accountTransactions.isEmpty) return 0;

    final accountBalance = accountTransactions
        .map((t) => _mapTransactionTypeToSignedAmount(t))
        .reduce((accumlation, amount) => accumlation + amount);

    return accountBalance;
  }

  @override
  Future<Transaction?> getTransactionById(int id) async {
    return _transactions.firstWhere((t) => t.id == id);
  }

  @override
  Future addTransaction(
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  ) async {
    final transaction = Transaction(
      id: (_transactions.isNotEmpty) ? _transactions.length + 1 : 1,
      amount: double.parse(amount),
      account: (await getCurrentUserAccount())!,
      customer: (await getCustomerByName(customerName))!,
      type: type,
      timeStamp: date,
      description: description,
      image: image,
      status: status,
    );

    _transactions.add(transaction);
  }

  @override
  Future updateTransaction(
    int id,
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  ) async {
    final transaction = Transaction(
      id: id,
      amount: double.parse(amount),
      account: (await getCurrentUserAccount())!,
      customer: (await getCustomerByName(customerName))!,
      type: type,
      status: status,
      timeStamp: date,
      description: description,
      image: image,
    );

    _transactions[_transactions.indexWhere((t) => t.id == transaction.id)] =
        transaction;
  }

  @override
  Future deleteTransaction(int id, DateTime date) async {
    _transactions.removeWhere((t) => (t.id == id));
  }

  double _mapTransactionTypeToSignedAmount(Transaction t) {
    return t.type == TransactionType.DEBIT ? -t.amount : t.amount;
  }

  @override
  Future<List<User>> getUsers() async {
    final List<User> existingUsers =
        _accounts.map((a) => a.user).toSet().toList();

    final List<User> allUsers = [];
    allUsers.addAll(existingUsers);

    return allUsers;
  }

  User _mapContactToUser(Contact contact) {
    return User(
      id: _users.length + 1,
      name: contact.displayName ?? "",
      phoneNumber: _getPhoneNumber(contact),
      initials: contact.initials(),
    );
  }

  String _getPhoneNumber(Contact contact) {
    if (contact.phones != null && contact.phones?.isNotEmpty == true) {
      return contact.phones?.first.value ?? "";
    } else {
      return "";
    }
  }

  @override
  Future addContact(Contact selectedContact) async {
    User newUser = _mapContactToUser(selectedContact);
    _users.add(newUser);
    _accounts.add(
      Account(
        id: _accounts.length + 1,
        title: newUser.name,
        user: newUser,
        status: CustomerStatus.ACTIVE,
        signedUp: true,
      ),
    );
  }

  @override
  Future<List<Contact>> getPhoneContacts() async {
    final contacts = await ContactsService.getContacts(withThumbnails: true);
    return contacts;
  }

  @override
  Future<Account?> getAccountById(int id) async {
    final accounts = (await getCustomersAccounts())
        .where((account) => account.id == id)
        .toList();
    return accounts.isNotEmpty ? accounts[0] : null;
  }

  @override
  Future<Account?> getAccountByName(String name) async {
    final accounts = (await getCustomersAccounts())
        .where((account) => account.user.name == name)
        .toList();
    return accounts.isNotEmpty ? accounts[0] : null;
  }

  @override
  Future<Account?> getCurrentUserAccount() async {
    final accounts = (await getCustomersAccounts())
        .where((account) => account.id == _currentAccountUserId)
        .toList();
    return accounts.isNotEmpty ? accounts[0] : null;
  }

  @override
  Future deleteAccount(int customerId) async {
    _accounts.removeWhere((acc) => (acc.id == customerId));
    _transactions.removeWhere((t) => (t.customer.id == customerId));
  }

  @override
  Future<User?> getUserByName(String name) async {
    return _users.firstWhere((u) => (u.name == name));
  }

  @override
  Future<User?> getUserById(int id) async {
    return _users.firstWhere((u) => (u.id == id));
  }

  @override
  Future<List<TransactionAudit>> getAuditLogById(int id) async {
    return [];
  }

  @override
  Future loginOrSignupUser(
    String fullName,
    String accountName,
    String phoneNumber,
  ) {
    // TODO: implement loginOrSignupUser
    throw UnimplementedError();
  }

  @override
  int getCurrentAccountUserId() => _currentAccountUserId;

  @override
  Future<void> sendPhoneNumberRequest(String phoneNumber) {
    // TODO: implement sendPhoneNumberRequest
    throw UnimplementedError();
  }

  @override
  Future<Customer?> getCustomerById(int id) async {
    final customers = _customers.where((c) => c.id == id).toList();
    return customers.isNotEmpty ? customers[0] : null;
  }

  @override
  Future<List<Customer>> getCustomers() async {
    return _customers;
  }

  @override
  Future<List<Customer>> getCustomersByName(String customerName) async {
    final customers =
        _customers.where((c) => c.customerName == customerName).toList();
    return customers.isNotEmpty ? customers : [];
  }

  Future<Customer?> getCustomerByName(customerName) async {
    final customers =
    _customers.where((c) => c.customerName == customerName).toList();
    return customers.isNotEmpty ? customers[0] : null;
  }

  @override
  Future<bool> doesUserExist(String phoneNumber) {
    // TODO: implement doesUserExist
    throw UnimplementedError();
  }
}
