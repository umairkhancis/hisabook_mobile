import 'dart:io';

import 'package:contacts_service/contacts_service.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_audit.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/entity/user.dart';

abstract class LedgerRepository {
  Future init();

  int getCurrentAccountUserId();

  Future<List<Transaction>> getAllTransactions();

  Future<Account?> getAccountById(int id);

  Future<List<Customer>> getCustomers();

  Future<List<Customer>> getCustomersByName(String customerName);

  Future<Customer?> getCustomerById(int id);

  Future<Account?> getAccountByName(String name);

  Future<Account?> getCurrentUserAccount();

  Future<List<Transaction>> getTransactionsByCustomer(Customer customer);

  Future<bool> doesUserExist(String phoneNumber);

  Future<double> getBalanceByCustomer(Customer customer);

  Future<void> sendPhoneNumberRequest(String phoneNumber);

  Future addTransaction(
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  );

  Future updateTransaction(
    int id,
    String customerName,
    String amount,
    TransactionType type,
    TransactionStatus status,
    String description,
    DateTime date,
    File? image,
  );

  Future loginOrSignupUser(String fullName, String accountName, String phoneNumber);

  Future<Transaction?> getTransactionById(int id);

  Future<List<User>> getUsers();

  Future deleteTransaction(int id, DateTime date);

  Future<List<Contact>> getPhoneContacts();

  Future addContact(Contact selectedContact);

  Future deleteAccount(int customerId);

  Future<User?> getUserByName(String name);

  Future<User?> getUserById(int id);

  Future<List<TransactionAudit>> getAuditLogById(int id);
}
