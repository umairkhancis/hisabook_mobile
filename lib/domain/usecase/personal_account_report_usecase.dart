import 'package:splitwise/domain/dto/personal_account_report_dto.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

class PersonalAccountReportUseCase {
  final LedgerRepository repository;

  const PersonalAccountReportUseCase({required this.repository});

  Future<PersonalAccountReportDto> getPersonalAccountDetailsReport() async {
    final Account? personalAccount =
        await repository.getCurrentUserAccount();

    return PersonalAccountReportDto(
      accountName: personalAccount?.title ?? "_",
      userName: personalAccount?.user.name ?? "_",
      phoneNumber: personalAccount?.user.phoneNumber ?? "_",
    );
  }
}
