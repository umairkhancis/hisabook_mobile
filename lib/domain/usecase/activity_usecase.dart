import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:splitwise/domain/dto/activity_item_dto.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

class ActivityUseCase {
  final LedgerRepository repository;

  ActivityUseCase({required this.repository});

  Future<List<ActivityItemDto>> getActivityItems() async {
    List<Transaction> allTransactions = await repository.getAllTransactions();

    allTransactions = allTransactions..sort((a, b) => a.id.compareTo(b.id));

    allTransactions = List.from(allTransactions.reversed);

    List<ActivityItemDto> activityItems =
        allTransactions.map((t) => _mapTransactionToDto(t)).toList();

    return activityItems;
  }

  ActivityItemDto _mapTransactionToDto(Transaction t) {
    RichText transactionAmountText = _mapTransactionAmountRichText(t);
    Color transactionAmountColor = _mapTransactionAmountColor(t);

    return ActivityItemDto(
      activityRichText: transactionAmountText,
      transactionAmountColor: transactionAmountColor,
      dateText: _mapDateText(t.timeStamp),
      description: t.description,
      transactionStatus: t.status,
    );
  }

  MaterialColor _mapTransactionAmountColor(Transaction t) =>
      (t.type == TransactionType.CREDIT) ? Colors.green : Colors.red;

  String _mapDateText(DateTime timeStamp) {
    final DateFormat formatter = DateFormat('dd MMM y, hh:mm aaa');
    final String formatted = formatter.format(timeStamp);
    return formatted;
  }

  /*String _mapActivityRichText(Transaction t) {
    if (t.status == TransactionStatus.UPDATED) {
      return "updated";
    } else if (t.status == TransactionStatus.DELETED) {
      return "deleted";
    } else {
      return "added";
    }
  }*/

  RichText _mapTransactionAmountRichText(Transaction t) {
    final isDeleted = t.status == TransactionStatus.DELETED;
    if (t.status == TransactionStatus.SETTLED &&
        t.type == TransactionType.DEBIT) {
      return RichText(
        textScaleFactor: 1.1,
        text: TextSpan(
          text: "I gave PKR ${t.amount}\nto ",
          style: TextStyle(
            color: _mapTransactionAmountColor(t),
            fontFamily: GoogleFonts.montserrat().fontFamily,
            decoration: isDeleted ? TextDecoration.lineThrough : null,
            decorationColor: isDeleted ? Colors.red : null,
            decorationStyle: isDeleted ? TextDecorationStyle.solid : null,
            decorationThickness: isDeleted ? 2.0 : 0.0,
          ),
          children: [
            TextSpan(
              text: t.customer.customerName,
              style: const TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.w200,
              ),
            ),
          ],
        ),
      );
      // return "I gave PKR ${t.amount}\nto ${t.customer.customerName}";
    } else if (t.status == TransactionStatus.SETTLED &&
        t.type == TransactionType.CREDIT) {
      return RichText(
        textScaleFactor: 1.1,
        text: TextSpan(
          text: "I got PKR ${t.amount}\nfrom ",
          style: TextStyle(
            color: _mapTransactionAmountColor(t),
            fontFamily: GoogleFonts.montserrat().fontFamily,
            decoration: isDeleted ? TextDecoration.lineThrough : null,
            decorationColor: isDeleted ? Colors.red : null,
            decorationStyle: isDeleted ? TextDecorationStyle.solid : null,
            decorationThickness: isDeleted ? 2.0 : 0.0,
          ),
          children: [
            TextSpan(
              text: t.customer.customerName,
              style: const TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.w200,
              ),
            ),
          ],
        ),
      );
      // return "I got PKR ${t.amount}\nfrom ${t.customer.customerName}";
    } else if (t.type == TransactionType.CREDIT) {
      return RichText(
        textScaleFactor: 1.1,
        text: TextSpan(
          text: "I got PKR ${t.amount}\nfrom ",
          style: TextStyle(
            color: _mapTransactionAmountColor(t),
            fontFamily: GoogleFonts.montserrat().fontFamily,
            decoration: isDeleted ? TextDecoration.lineThrough : null,
            decorationColor: isDeleted ? Colors.red : null,
            decorationStyle: isDeleted ? TextDecorationStyle.solid : null,
            decorationThickness: isDeleted ? 2.0 : 0.0,
          ),
          children: [
            TextSpan(
              text: t.customer.customerName,
              style: const TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.w200,
              ),
            ),
          ],
        ),
      );
      // return "I got PKR ${t.amount}\nfrom ${t.customer.customerName}";
    } else if (t.type == TransactionType.DEBIT) {
      return RichText(
        textScaleFactor: 1.1,
        text: TextSpan(
          text: "I gave PKR ${t.amount}\nto ",
          style: TextStyle(
            color: _mapTransactionAmountColor(t),
            fontFamily: GoogleFonts.montserrat().fontFamily,
            decoration: isDeleted ? TextDecoration.lineThrough : null,
            decorationColor: isDeleted ? Colors.red : null,
            decorationStyle: isDeleted ? TextDecorationStyle.solid : null,
            decorationThickness: isDeleted ? 2.0 : 0.0,
          ),
          children: [
            TextSpan(
              text: t.customer.customerName,
              style: const TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.w200,
              ),
            ),
          ],
        ),
      );
      // return "I gave PKR ${t.amount}\nto ${t.customer.customerName}";
    } else {
      return RichText(text: const TextSpan(text: ""));
    }
  }
}
