import 'package:intl/intl.dart';
import 'package:splitwise/domain/dto/transaction_details_report_dto.dart';
import 'package:splitwise/domain/entity/audit_log_type.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_audit.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

class TransactionDetailsReportUseCase {
  final LedgerRepository repository;

  TransactionDetailsReportUseCase(this.repository);

  Future<TransactionDetailsReport> getTransactionDetailsReport(int id) async {
    final Transaction? transaction = await repository.getTransactionById(id);

    if (transaction == null) {
      throw Exception("Transaction not found with id = $id");
    }

    return TransactionDetailsReport(
      transaction: transaction,
      image: transaction.image,
      descriptionText: transaction.description,
      amountText: "PKR ${transaction.amount}",
      breakDownText: _mapBreakDownTextList(transaction),
      dateText: _mapDateText(transaction.timeStamp),
    );
  }

  Future<List<String>> getAuditLog(Transaction transaction) async {
    List<TransactionAudit> auditLog =
        await repository.getAuditLogById(transaction.id);

    List<String> auditTexts =
        auditLog.map((a) => _mapAuditLogToText(a)).toList();

    return auditTexts;
    // return [
    //   "Added by ${transaction.srcAccount.holder.name} on ${mapDateText(transaction.timeStamp)}",
    //   "Updated by ${transaction.srcAccount.holder.name} on ${mapDateText(transaction.timeStamp)}",
    // ];
  }

  String _mapDateText(DateTime t) {
    final DateFormat formatter = DateFormat('dd MMMM yyyy');
    final String formatted = formatter.format(t);
    return formatted;
  }

  String _mapBreakDownTextList(Transaction t) {
    String text = "";
    if (t.type == TransactionType.CREDIT) {
      text = "You gave ${t.amount}";
    } else if (t.type == TransactionType.DEBIT) {
      text = "You got ${t.amount}";
    }
    return text;
  }

  String _mapAuditLogToText(TransactionAudit a) {
    if (a.type == AuditLogType.INSERT) {
      return "Added by ${a.transaction.account.user.name} on ${_mapDateText(a.timeStamp)}";
    } else if (a.type == AuditLogType.UPDATE) {
      return "Updated by ${a.transaction.account.user.name} on ${_mapDateText(a.timeStamp)}";
    } else {
      return "Deleted by ${a.transaction.account.user.name} on ${_mapDateText(a.timeStamp)}";
    }
  }
}
