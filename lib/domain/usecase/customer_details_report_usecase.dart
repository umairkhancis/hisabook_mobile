import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:splitwise/domain/dto/account_details_report_dto.dart';
import 'package:splitwise/domain/dto/account_transaction_report_item_dto.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

class AccountDetailsReportUseCase {
  final LedgerRepository repository;

  AccountDetailsReportUseCase(this.repository);

  Future<AccountDetailsReport> getCustomerDetailsReport(
      Customer customer) async {
    List<Transaction> accountTransactions =
        await repository.getTransactionsByCustomer(customer);

    accountTransactions = accountTransactions
      ..sort((a, b) => a.id.compareTo(b.id));

    accountTransactions = List.from(accountTransactions.reversed);

    final List<AccountTransactionReportItemDto> tranactionsReportItems =
        accountTransactions.map((t) => _mapTransactionToDto(t)).toList();

    final double accountBalance =
        await repository.getBalanceByCustomer(customer);

    final Account? currentAccount = await repository.getCurrentUserAccount();

    return AccountDetailsReport(
      transactions: tranactionsReportItems,
      overallAccountBalance: accountBalance,
      customerStatus: _mapBalanceToAccountStatus(accountBalance),
      balanceStatusTextColor: _mapBalanceStatusTextColor(accountBalance),
      balanceStatusText: _mapBalanceValueToUserFriendlyValue(accountBalance),
      currentAccount: currentAccount!,
    );
  }

  AccountTransactionReportItemDto _mapTransactionToDto(Transaction t) {
    return AccountTransactionReportItemDto(
      id: t.id,
      dateText: _mapDateText(t.timeStamp),
      description: t.description,
      subtext: _mapTransactionSubText(t),
      transactionStatusText: _mapTransactionStatus(t.amount, t.type),
      transactionStatusColor: _mapTransactionStatusColor(t.type),
      transactionType: t.type,
      transactionStatus: t.status,
    );
  }

  String _mapDateText(DateTime timeStamp) {
    final DateFormat formatter = DateFormat('MMM\ndd');
    final String formatted = formatter.format(timeStamp);
    return formatted;
  }

  String _mapBalanceValueToUserFriendlyValue(double accountBalance) {
    if (accountBalance > 0) {
      return "I have to give ${accountBalance.abs()}";
    } else if (accountBalance < 0) {
      return "I have to get ${accountBalance.abs()}";
    } else {
      return "All Settled";
    }
  }

  Color? _mapBalanceStatusTextColor(double accountBalance) {
    if (accountBalance > 0) {
      return Colors.green;
    } else if (accountBalance < 0) {
      return Colors.red;
    } else {
      return Colors.grey;
    }
  }

  String _mapTransactionStatus(double amount, TransactionType type) {
    if (type == TransactionType.CREDIT) {
      return "I got\n$amount";
    } else if (type == TransactionType.DEBIT) {
      return "I gave\n$amount";
    } else {
      return "settled up";
    }
  }

  String _mapTransactionSubText(Transaction transaction) {
    if (transaction.type == TransactionType.CREDIT) {
      return "I got ${transaction.amount}";
    } else if (transaction.type == TransactionType.DEBIT) {
      return "I gave ${transaction.amount}";
    } else {
      return "no balance";
    }
  }

  Color? _mapTransactionStatusColor(TransactionType transactionType) {
    if (transactionType == TransactionType.DEBIT) {
      return Colors.red;
    } else if (transactionType == TransactionType.CREDIT) {
      return Colors.green;
    } else {
      return Colors.grey;
    }
  }

  _mapBalanceToAccountStatus(double accountBalance) {
    if (accountBalance > 0) {
      return CustomerStatus.SURPLUS;
    } else if (accountBalance < 0) {
      return CustomerStatus.DEBT;
    } else {
      return CustomerStatus.SETTLED;
    }
  }
}
