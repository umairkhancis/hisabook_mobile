import 'package:splitwise/domain/dto/settle_up_report.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

class SettleUpReportUseCase {
  final LedgerRepository repository;

  SettleUpReportUseCase(this.repository);

  Future<SettleUpReport> getSettleUpReport(Customer customer) async {
    final double accountBalance = await repository.getBalanceByCustomer(
      customer,
    );
    return SettleUpReport(
      info: getInfoText(customer, accountBalance),
      status: _mapBalanceToAccountStatus(accountBalance),
      amount: "${accountBalance.abs()}",
    );
  }

  String getInfoText(Customer customer, double accountBalance) {
    if (accountBalance > 0) {
      return "I gave to ${customer.customerName}";
    } else if (accountBalance < 0) {
      return "I got from ${customer.customerName}";
    } else {
      return "No balance to settle up";
    }
  }

  CustomerStatus _mapBalanceToAccountStatus(double accountBalance) {
    if (accountBalance > 0) {
      return CustomerStatus.SURPLUS;
    } else if (accountBalance < 0) {
      return CustomerStatus.DEBT;
    } else {
      return CustomerStatus.SETTLED;
    }
  }
}
