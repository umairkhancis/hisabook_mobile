import 'package:flutter/material.dart';
import 'package:splitwise/app_localization.dart';
import 'package:splitwise/domain/dto/account_balance_report_item_dto.dart';
import 'package:splitwise/domain/dto/accounts_balance_report.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';

class CustomersListingReportUseCase {
  final LedgerRepository repository;

  final IAppLocalizations appLocalizations;

  CustomersListingReportUseCase({
    required this.repository,
    required this.appLocalizations,
  });

  Future<AccountsBalanceReport> getBalanceReport(customerName) async {
    final List<Customer> customers;
    if (customerName.isNotEmpty) {
      customers = await repository.getCustomersByName(customerName);
    } else {
      customers = await repository.getCustomers();
    }

    List<CustomerBalanceReportItemDto> customersBalanceListingItems = [];

    for (var customer in customers) {
      final CustomerBalanceReportItemDto item =
          await _mapTransactionsToCustomerBalanceReportItem(customer);
      customersBalanceListingItems.add(item);
    }

    double debtBalance = 0;
    double surplusBalance = 0;

    for (var customer in customers) {
      final balance = await repository.getBalanceByCustomer(customer);
      if (balance > 0) {
        surplusBalance += balance;
      } else {
        debtBalance += balance;
      }
    }

    final overallSurplusAcountStatusText =
        _mapSurplusAccountStatusText(surplusBalance);
    final overallDebtAcountStatusText = _mapDebtAccountStatusText(debtBalance);
    final overallSettledAcountStatusText =
        _mapSettledAccountStatusText(surplusBalance, debtBalance);

    return AccountsBalanceReport(
      overallDebtAccountStatusText: overallDebtAcountStatusText,
      overallSurplusAccountStatusText: overallSurplusAcountStatusText,
      overallSettledAccountStatusText: overallSettledAcountStatusText,
      items: customersBalanceListingItems,
    );
  }

  Future<CustomerBalanceReportItemDto>
      _mapTransactionsToCustomerBalanceReportItem(
    Customer customer,
  ) async {
    final accountBalance = await repository.getBalanceByCustomer(customer);
    return CustomerBalanceReportItemDto(
      customer: customer,
      balanceStatusText: _mapBalanceValueToUserFriendlyValue(accountBalance),
      balanceStatusTextColor:
          _mapColorForCustomerStatusText(getAccountStatus(accountBalance)),
    );
  }

  String _mapBalanceValueToUserFriendlyValue(double accountBalance) {
    if (accountBalance > 0) {
      return "PKR ${accountBalance.abs()}";
    } else if (accountBalance < 0) {
      return "PKR ${accountBalance.abs()}";
    } else {
      return "PKR 0";
    }
  }

  CustomerStatus getAccountStatus(double accountBalance) {
    if (accountBalance > 0) {
      return CustomerStatus.SURPLUS;
    } else if (accountBalance < 0) {
      return CustomerStatus.DEBT;
    } else {
      return CustomerStatus.SETTLED;
    }
  }

  String _mapDebtAccountStatusText(double debtBalance) {
    final accountStatusString = appLocalizations.translate("recievable.account.status");
    return "$accountStatusString\nPKR ${debtBalance.abs()}";
  }

  String _mapSurplusAccountStatusText(double surplusBalance) {
    final accountStatusString = appLocalizations.translate("payable.account.status");
    return "$accountStatusString\nPKR ${surplusBalance.abs()}";
  }

  String _mapSettledAccountStatusText(
    double surplusBalance,
    double debtBalance,
  ) {
    if (surplusBalance == 0 && debtBalance == 0) {
      return "You are all settled";
    } else {
      return "";
    }
  }

  Color? _mapColorForCustomerStatusText(CustomerStatus customerStatus) {
    if (customerStatus == CustomerStatus.SURPLUS) {
      return Colors.green;
    } else if (customerStatus == CustomerStatus.DEBT) {
      return Colors.red;
    } else {
      return Colors.green;
    }
  }
}
