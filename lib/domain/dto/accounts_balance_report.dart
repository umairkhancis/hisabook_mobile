import 'package:splitwise/domain/dto/account_balance_report_item_dto.dart';

class AccountsBalanceReport {
  final String overallDebtAccountStatusText;
  final String overallSurplusAccountStatusText;
  final String overallSettledAccountStatusText;
  final List<CustomerBalanceReportItemDto> items;

  AccountsBalanceReport({
    required this.overallDebtAccountStatusText,
    required this.overallSurplusAccountStatusText,
    required this.overallSettledAccountStatusText,
    required this.items,
  });
}
