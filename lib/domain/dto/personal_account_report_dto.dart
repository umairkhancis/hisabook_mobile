class PersonalAccountReportDto {
  final String accountName;
  final String userName;
  final String phoneNumber;

  PersonalAccountReportDto({
    required this.accountName,
    required this.userName,
    required this.phoneNumber,
  });
}
