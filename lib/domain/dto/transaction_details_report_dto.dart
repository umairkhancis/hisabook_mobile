import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/transaction.dart';

class TransactionDetailsReport extends Equatable {
  final Transaction transaction;
  final File? image;
  final String descriptionText;
  final String amountText;
  final String dateText;
  final String breakDownText;

  const TransactionDetailsReport({
    this.image,
    required this.transaction,
    required this.descriptionText,
    required this.amountText,
    required this.dateText,
    required this.breakDownText,
  });

  @override
  List<Object?> get props => [
        image,
        transaction,
        descriptionText,
        amountText,
        dateText,
        breakDownText,
      ];
}
