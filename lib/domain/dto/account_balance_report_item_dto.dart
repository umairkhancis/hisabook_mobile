import 'package:flutter/material.dart';
import 'package:splitwise/domain/entity/customer.dart';

class CustomerBalanceReportItemDto {
  final Customer customer;
  final String balanceStatusText;
  final Color? balanceStatusTextColor;

  CustomerBalanceReportItemDto({
    required this.customer,
    required this.balanceStatusText,
    required this.balanceStatusTextColor,
  });
}
