import 'package:flutter/material.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';

class AccountTransactionReportItemDto {
  final int id;
  final String dateText;
  final String description;
  final String subtext;
  final TransactionStatus transactionStatus;
  final String transactionStatusText;
  final Color? transactionStatusColor;
  final TransactionType transactionType;

  AccountTransactionReportItemDto({
    required this.id,
    required this.dateText,
    required this.description,
    required this.subtext,
    required this.transactionStatusText,
    required this.transactionStatusColor,
    required this.transactionType,
    required this.transactionStatus,
  });
}
