import 'package:splitwise/domain/entity/account_status.dart';

class SettleUpReport {
  final String info;
  final String amount;
  final CustomerStatus status;

  SettleUpReport({
    required this.info,
    required this.amount,
    required this.status,
  });
}
