import 'package:flutter/material.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';

class ActivityItemDto {
  final RichText activityRichText;
  final TransactionStatus transactionStatus;
  final String description;
  final Color transactionAmountColor;
  final String dateText;

  ActivityItemDto({
    required this.activityRichText,
    required this.transactionStatus,
    required this.description,
    required this.transactionAmountColor,
    required this.dateText,
  });
}
