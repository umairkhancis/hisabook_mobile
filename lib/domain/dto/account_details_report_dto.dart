import 'package:flutter/material.dart';
import 'package:splitwise/domain/dto/account_transaction_report_item_dto.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/account_status.dart';

class AccountDetailsReport {
  final String balanceStatusText;
  final Color? balanceStatusTextColor;
  final double overallAccountBalance;
  final CustomerStatus customerStatus;
  final Account currentAccount;
  final List<AccountTransactionReportItemDto> transactions;

  AccountDetailsReport({
    required this.balanceStatusText,
    required this.balanceStatusTextColor,
    required this.transactions,
    required this.customerStatus,
    required this.currentAccount,
    required this.overallAccountBalance,
  });
}
