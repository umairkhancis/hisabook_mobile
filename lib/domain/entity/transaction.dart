import 'dart:convert';
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/account.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';

class Transaction extends Equatable {
  final int id;
  final double amount;
  final Account account;
  final Customer customer;
  final TransactionType type;
  final TransactionStatus status;
  final String description;
  final DateTime timeStamp;
  final String notes;
  final File? image;

  const Transaction({
    required this.id,
    required this.amount,
    required this.account,
    required this.customer,
    required this.type,
    required this.status,
    required this.timeStamp,
    this.image,
    this.description = "",
    this.notes = "",
  });

  @override
  List<Object?> get props => [id];

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
        id: json["id"],
        amount: json["amount"],
        account: json["account"],
        customer: json["customer"],
        type: json["type"],
        status: json["status"],
        timeStamp: json["timeStamp"],
        image: json["image"],
        description: json["description"],
        notes: json["notes"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "amount": amount,
        "account": account,
        "customer": customer,
        "type": type,
        "status": status,
        "timeStamp": timeStamp,
        "image": image,
        "description": description,
        "notes": notes,
      };

  Account accountJson(String string) => Account.fromJson(json.decode(string));

  String accountToJson(Account account) => json.encode(account.toJson());
}
