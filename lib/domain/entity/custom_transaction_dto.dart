import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';

class CustomTransactionDto extends Equatable {
  final double amount;
  final String description;
  final TransactionType type;
  final TransactionStatus status;
  final String notes;
  final String imagePath;
  final int srcAccountId;
  final int destAccountId;

  const CustomTransactionDto({
    required this.amount,
    required this.description,
    required this.type,
    required this.status,
    required this.notes,
    required this.imagePath,
    required this.srcAccountId,
    required this.destAccountId,
  });

  @override
  List<Object?> get props => [];

  factory CustomTransactionDto.fromJson(Map<String, dynamic> json) =>
      CustomTransactionDto(
        amount: json["amount"],
        description: json["description"],
        type: json["type"],
        status: json["status"],
        notes: json["notes"],
        imagePath: json["imagePath"],
        srcAccountId: json["srcAccountId"],
        destAccountId: json["destAccountId"],
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
        "description": description,
        "type": type,
        "status": status,
        "notes": notes,
        "imagePath": imagePath,
        "srcAccountId": srcAccountId,
        "destAccountId": destAccountId,
      };

  CustomTransactionDto accountJson(String string) =>
      CustomTransactionDto.fromJson(json.decode(string));

  String accountToJson(CustomTransactionDto account) =>
      json.encode(account.toJson());
}
