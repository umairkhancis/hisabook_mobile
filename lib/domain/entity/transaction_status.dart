enum TransactionStatus {
  SURPLUS,
  DEBT,
  SETTLED,
  DELETED,
  UPDATED,
}

TransactionStatus mapToTransactionStatus(String value) {
  return TransactionStatus.values.firstWhere((type) => type.name == value);
}
