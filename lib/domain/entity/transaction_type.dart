enum TransactionType {
  CREDIT,
  DEBIT,
}

TransactionType mapToTransactionType(String value) {
  return TransactionType.values.firstWhere((type) => type.name == value);
}