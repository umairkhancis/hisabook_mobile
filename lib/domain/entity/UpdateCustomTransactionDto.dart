import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/transaction_status.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';

class UpdateCustomTransactionDto extends Equatable {
  final int transactionId;
  final double amount;
  final String description;
  final TransactionType type;
  final TransactionStatus status;
  final String notes;
  final String imagePath;

  const UpdateCustomTransactionDto({
    required this.transactionId,
    required this.amount,
    required this.description,
    required this.type,
    required this.status,
    required this.notes,
    required this.imagePath,
  });

  @override
  List<Object?> get props => [];

  factory UpdateCustomTransactionDto.fromJson(Map<String, dynamic> json) =>
      UpdateCustomTransactionDto(
        transactionId: json["transactionId"],
        amount: json["amount"],
        description: json["description"],
        type: json["type"],
        status: json["status"],
        notes: json["notes"],
        imagePath: json["imagePath"],
      );

  Map<String, dynamic> toJson() => {
    "transactionId": transactionId,
    "amount": amount,
    "description": description,
    "type": type,
    "status": status,
    "notes": notes,
    "imagePath": imagePath,
  };

  UpdateCustomTransactionDto updateTransactionJson(String string) =>
      UpdateCustomTransactionDto.fromJson(json.decode(string));

  String updateTransactionToJson(UpdateCustomTransactionDto updateTransactionDto) =>
      json.encode(updateTransactionDto.toJson());
}
