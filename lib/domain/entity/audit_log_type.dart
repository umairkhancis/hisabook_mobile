enum AuditLogType {
  INSERT,
  UPDATE,
  DELETE,
}

AuditLogType mapToTransactionAudit(String value) {
  return AuditLogType.values.firstWhere((type) => type.name == value);
}