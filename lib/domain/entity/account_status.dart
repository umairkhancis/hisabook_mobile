enum CustomerStatus {
  SURPLUS,
  DEBT,
  SETTLED,
  NOT_ACTIVE,
  ACTIVE,
}

/*extension AccountStatusExtension on AccountStatus {
  String get name {
    switch (this) {
      case AccountStatus.SURPLUS:
        return "Owes you";
      case AccountStatus.DEBT:
        return "You owe";
      default:
        return "All settled";
    }
  }
}*/

CustomerStatus mapToAccountStatus(String value) {
  return CustomerStatus.values.firstWhere((status) => status.name == value);
}
