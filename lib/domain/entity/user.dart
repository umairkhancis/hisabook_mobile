import 'dart:convert';

import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String phoneNumber;
  final String initials;

  const User({
    required this.id,
    required this.name,
    required this.phoneNumber,
    required this.initials,
  });

  @override
  List<Object?> get props => [id];

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        phoneNumber: json["phoneNumber"],
        initials: json["initials"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phoneNumber": phoneNumber,
        "initials": initials,
      };

  User userJson(String string) => User.fromJson(json.decode(string));

  String userToJson(User user) => json.encode(user.toJson());
}
