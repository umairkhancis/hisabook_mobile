import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/audit_log_type.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/user.dart';

class TransactionAudit extends Equatable {
  final int id;
  final Transaction transaction;
  final AuditLogType type;
  final User user;
  final DateTime timeStamp;

  const TransactionAudit({
    required this.id,
    required this.transaction,
    required this.type,
    required this.user,
    required this.timeStamp,
  });

  @override
  List<Object?> get props => [id];
}
