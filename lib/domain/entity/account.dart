import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/user.dart';

class Account extends Equatable {
  final int id;
  final User user;
  final String title;
  final CustomerStatus status;
  final bool signedUp;

  const Account({
    required this.id,
    required this.title,
    required this.user,
    required this.status,
    required this.signedUp,
  });

  @override
  List<Object?> get props => [id];

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        id: json["id"],
        title: json["title"],
        user: json["holder"],
        status: json["status"],
        signedUp: json["signedUp"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user": user,
        "title": title,
        "status": status,
        "signedUp": signedUp,
      };

  Account accountJson(String string) => Account.fromJson(json.decode(string));

  String accountToJson(Account account) => json.encode(account.toJson());
}
