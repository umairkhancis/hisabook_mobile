import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:splitwise/domain/entity/account.dart';

class Customer extends Equatable {
  final int id;
  final Account account;
  final String customerName;
  final String phoneNumber;

  const Customer({
    required this.id,
    required this.account,
    required this.customerName,
    required this.phoneNumber,
  });

  @override
  List<Object?> get props => [id];

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"],
        account: json["accountId"],
        customerName: json["customerName"],
        phoneNumber: json["phoneNumber"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "accountId": account,
        "customerName": customerName,
        "phoneNumber": phoneNumber,
      };

  Customer customerJson(String string) =>
      Customer.fromJson(json.decode(string));

  String customerToJson(Customer customer) => json.encode(customer.toJson());
}
