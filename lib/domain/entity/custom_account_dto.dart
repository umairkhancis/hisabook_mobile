import 'dart:convert';

import 'package:equatable/equatable.dart';

class CustomAccountDto extends Equatable {
  final int accountId;
  final String title;

  const CustomAccountDto({required this.accountId, required this.title});

  @override
  List<Object?> get props => [accountId];

  factory CustomAccountDto.fromJson(Map<String, dynamic> json) =>
      CustomAccountDto(
        accountId: json["id"],
        title: json["title"],
      );

  Map<String, dynamic> toJson() => {
        "id": accountId,
        "title": title,
      };

  CustomAccountDto accountJson(String string) =>
      CustomAccountDto.fromJson(json.decode(string));

  String accountToJson(CustomAccountDto account) =>
      json.encode(account.toJson());
}
