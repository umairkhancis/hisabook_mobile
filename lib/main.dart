import 'package:contacts_service/contacts_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:splitwise/app_localization.dart';
import 'package:splitwise/data/repository/in_memory_ledger_repository.dart';
import 'package:splitwise/data/repository/remote_ledger_repository.dart';
import 'package:splitwise/domain/entity/account_status.dart';
import 'package:splitwise/domain/entity/customer.dart';
import 'package:splitwise/domain/entity/transaction.dart';
import 'package:splitwise/domain/entity/transaction_type.dart';
import 'package:splitwise/domain/repository/ledger_repository.dart';
import 'package:splitwise/domain/usecase/activity_usecase.dart';
import 'package:splitwise/domain/usecase/customer_details_report_usecase.dart';
import 'package:splitwise/domain/usecase/customers_listing_report_usecase.dart';
import 'package:splitwise/domain/usecase/personal_account_report_usecase.dart';
import 'package:splitwise/domain/usecase/settle_up_report_usecase.dart';
import 'package:splitwise/domain/usecase/transaction_details_report_usecase.dart';
import 'package:splitwise/language_model.dart';
import 'package:splitwise/libraries/logger/api/logger.dart';
import 'package:splitwise/presentation/bloc/activity/activity_cubit.dart';
import 'package:splitwise/presentation/bloc/add_more_customer/add_more_customer_cubit.dart';
import 'package:splitwise/presentation/bloc/add_transaction/add_transaction_cubit.dart';
import 'package:splitwise/presentation/bloc/customer_details/customer_details_cubit.dart';
import 'package:splitwise/presentation/bloc/customer_details_actions/account_details_actions_cubit.dart';
import 'package:splitwise/presentation/bloc/customers_listing/customers_listing_cubit.dart';
import 'package:splitwise/presentation/bloc/personal_account/personal_account_cubit.dart';
import 'package:splitwise/presentation/bloc/phone_number_otp/phone_number_cubit.dart';
import 'package:splitwise/presentation/bloc/settle_up_transaction/settle_up_transaction_cubit.dart';
import 'package:splitwise/presentation/bloc/signup_or_login/signup_or_login_cubit.dart';
import 'package:splitwise/presentation/bloc/transaction_details/transaction_details_cubit.dart';
import 'package:splitwise/presentation/dto/AccountDetailsActionsScreenArgs.dart';
import 'package:splitwise/presentation/dto/AccountDetailsScreenArgs.dart';
import 'package:splitwise/presentation/dto/AddTransactionArgs.dart';
import 'package:splitwise/presentation/dto/CustomersListingArgs.dart';
import 'package:splitwise/presentation/dto/FullNameAndShopNameScreenArgs.dart';
import 'package:splitwise/presentation/dto/OtpScreenArgs.dart';
import 'package:splitwise/presentation/dto/SettleUpTransactionArgs.dart';
import 'package:splitwise/presentation/dto/TransactionDetailsScreenArgs.dart';
import 'package:splitwise/presentation/dto/VerifyContactInfoScreenArgs.dart';
import 'package:splitwise/presentation/screens/activity_screen.dart';
import 'package:splitwise/presentation/screens/add_more_customer_screen.dart';
import 'package:splitwise/presentation/screens/add_transaction_screen.dart';
import 'package:splitwise/presentation/screens/customer_details_actions_screen.dart';
import 'package:splitwise/presentation/screens/customer_details_screen.dart';
import 'package:splitwise/presentation/screens/customers_listing.dart';
import 'package:splitwise/presentation/screens/full_name_shop_name_screen.dart';
import 'package:splitwise/presentation/screens/home_screen.dart';
import 'package:splitwise/presentation/screens/otp_screen.dart';
import 'package:splitwise/presentation/screens/personal_account_screen.dart';
import 'package:splitwise/presentation/screens/settle_up_transaction_screen.dart';
import 'package:splitwise/presentation/screens/signup_screen.dart';
import 'package:splitwise/presentation/screens/transaction_details_screen.dart';
import 'package:splitwise/presentation/screens/verify_contact_info_screen.dart';
import 'package:splitwise/routes.dart';

final RouteObserver<ModalRoute<void>> routeObserver =
    RouteObserver<ModalRoute<void>>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool hasInitialzed = false;

  final _supportedLocales = const [
    Locale('en', 'US'),
    Locale('ur', ''),
    // Locale('ckb', '')
  ];

  final _localizationsDelegates = const [
    // CkbMaterialLocalizations.delegate,
    // CkbWidgetLocalizations.delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate
  ];

  @override
  void initState() {
    super.initState();
    setup();
    initDB();
  }

  Future setup() async {
    AppLocalizations localizations =
        AppLocalizations.language(englishLanguageModel());
    await localizations.synchronousLoad();
    SharedAppLocalizations().appLocalizations = localizations;
  }

  @override
  Widget build(BuildContext context) {
    return hasInitialzed
        ? MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Hisabook',
            theme: ThemeData(
              primarySwatch: Colors.green,
              fontFamily: GoogleFonts.montserrat().fontFamily,
              textSelectionTheme: const TextSelectionThemeData(
                cursorColor: Colors.green,
              ),
            ),
            home: const InitializerWidget(),
            onGenerateRoute: _onGenerateRoute,
            navigatorObservers: [routeObserver],
          )
        : MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Hisabook',
            supportedLocales: _supportedLocales,
            localizationsDelegates: _localizationsDelegates,
            theme: ThemeData(
              primarySwatch: Colors.green,
              fontFamily: GoogleFonts.montserrat().fontFamily,
              textSelectionTheme: const TextSelectionThemeData(
                cursorColor: Colors.green,
              ),
            ),
            home: Scaffold(
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text("Loading"),
                    SizedBox(height: 20),
                    CircularProgressIndicator()
                  ],
                ),
              ),
            ),
          );
  }

  LanguageModel englishLanguageModel() {
    final LanguageModel languageModel = LanguageModel(
      name: 'English',
      isRTL: false,
      bundledFileCode: 'en',
      code: 'en-US',
    );
    return languageModel;
  }

  LanguageModel urduLanguageModel() {
    final LanguageModel languageModel = LanguageModel(
      name: 'Urdu',
      isRTL: true,
      bundledFileCode: 'ur',
      code: 'ur',
    );
    return languageModel;
  }

  BlocProvider<AddMoreCustomerCubit> _addMoreCustomerScreenRoute() {
    return BlocProvider(
      create: (context) => AddMoreCustomerCubit(repository: ledgerRepository),
      child: const AddCustomerScreen(),
    );
  }

  BlocProvider<AddTransactionCubit> _addTransactionScreen(
    Customer? customer,
    Transaction? transaction,
    TransactionType type,
  ) {
    return BlocProvider(
      create: (context) => AddTransactionCubit(repository: ledgerRepository),
      child: AddTransaction(
        customer: customer,
        transaction: transaction,
        type: type,
      ),
    );
  }

  BlocProvider<AddMoreCustomerCubit> _verifyContactInfoRoute(
      Contact newContact) {
    return BlocProvider(
      create: (context) => AddMoreCustomerCubit(repository: ledgerRepository),
      child: VerifyContactInfoScreen(contact: newContact),
    );
  }

  BlocProvider<CustomerDetailsCubit> _accountsDetailsScreen(Customer customer) {
    return BlocProvider(
      create: (context) => CustomerDetailsCubit(
        accountDetailsReportUseCase:
            AccountDetailsReportUseCase(ledgerRepository),
      ),
      child: CustomerDetailsScreen(customer: customer),
    );
  }

  BlocProvider<SettleUpTransactionCubit> _settleUpTransactionScreenRoute(
    Customer customer,
    CustomerStatus status,
    double overallAccountBalance,
  ) {
    return BlocProvider<SettleUpTransactionCubit>(
      create: (context) => SettleUpTransactionCubit(
        settleUpReportUseCase: SettleUpReportUseCase(ledgerRepository),
        repository: ledgerRepository,
      ),
      child: SettleUpTransactionScreen(
        customer: customer,
        status: status,
        accountBalance: overallAccountBalance,
      ),
    );
  }

  BlocProvider<TransactionDetailsCubit> _transactionDetailsScreenRoute(int id) {
    return BlocProvider(
      create: (context) => TransactionDetailsCubit(
        transactionDetailsReportUseCase:
            TransactionDetailsReportUseCase(ledgerRepository),
        repository: ledgerRepository,
      ),
      child: TransactionDetailsScreen(id: id),
    );
  }

  BlocProvider<CustomerDetailsActionsCubit> _accountDetailsActionsScreenRoute(
    Customer customer,
  ) {
    return BlocProvider(
      create: (context) => CustomerDetailsActionsCubit(ledgerRepository),
      child: CustomerDetailsActionsScreen(customer: customer),
    );
  }

  BlocProvider<SignupOrLoginCubit> _otpScreenRoute(
    String fullName,
    String phoneNumber,
  ) {
    return BlocProvider(
      create: (context) => SignupOrLoginCubit(repository: ledgerRepository),
      child: OtpScreen(fullName: fullName, phoneNumber: phoneNumber),
    );
  }

  BlocProvider<SignupOrLoginCubit> _fullNameAndShopNameRoute(
    String phoneNumber,
  ) {
    return BlocProvider(
      create: (context) => SignupOrLoginCubit(repository: ledgerRepository),
      child: FullNameAndShopNameScreen(phoneNumber: phoneNumber),
    );
  }

  Route? _onGenerateRoute(RouteSettings settings) {
    if (settings.name == MyRoutes.addTransactionRoute) {
      final args = settings.arguments as AddTransactionArgs;
      return MaterialPageRoute(
        builder: (context) => _addTransactionScreen(
          args.customer,
          args.transaction,
          args.type,
        ),
      );
    }
    if (settings.name == MyRoutes.customerListingRoute) {
      final args = settings.arguments as CustomersListingArgs;
      return MaterialPageRoute(
        builder: (context) => customersListingScreen(args.customerName),
      );
    }
    if (settings.name == MyRoutes.homePageRoute) {
      return MaterialPageRoute(
        builder: (context) => _homePageRoute(),
      );
    }
    if (settings.name == MyRoutes.customerDetailsRoute) {
      final args = settings.arguments as CustomerDetailsScreenArgs;
      return MaterialPageRoute(
        builder: (context) => _accountsDetailsScreen(args.customer),
      );
    }
    if (settings.name == MyRoutes.transactionDetailsRoute) {
      final args = settings.arguments as TransactionDetailsScreenArgs;
      return MaterialPageRoute(
        builder: (context) => _transactionDetailsScreenRoute(args.id),
      );
    }
    if (settings.name == MyRoutes.addMoreCustomersRoute) {
      return MaterialPageRoute(
        builder: (context) => _addMoreCustomerScreenRoute(),
      );
    }
    if (settings.name == MyRoutes.verifyContactInfoRoute) {
      final args = settings.arguments as VerifyContactInfoScreenArgs;
      return MaterialPageRoute(
        builder: (context) => _verifyContactInfoRoute(args.contact),
      );
    }
    if (settings.name == MyRoutes.settleUpTransactionRoute) {
      final args = settings.arguments as SettleUpTransactionArgs;
      return MaterialPageRoute(
        builder: (context) => _settleUpTransactionScreenRoute(
          args.customer,
          args.status,
          args.overallBalance,
        ),
      );
    }
    if (settings.name == MyRoutes.accountDetailsActionsRoute) {
      final args = settings.arguments as CustomerDetailsActionsScreenArgs;
      return MaterialPageRoute(
        builder: (context) => _accountDetailsActionsScreenRoute(args.customer),
      );
    }
    if (settings.name == MyRoutes.activityRoute) {
      return MaterialPageRoute(
        builder: (context) => activityScreen(),
      );
    }
    if (settings.name == MyRoutes.otpScreen) {
      final args = settings.arguments as OtpScreenArgs;
      return MaterialPageRoute(
        builder: (context) => _otpScreenRoute(
          args.fullName,
          args.phoneNumber,
        ),
      );
    }
    if (settings.name == MyRoutes.fullNameAndShopNameScreen) {
      final args = settings.arguments as FullNameAndShopNameScreenArgs;
      return MaterialPageRoute(
        builder: (context) => _fullNameAndShopNameRoute(args.phoneNumber),
      );
    }
    assert(false, 'Need to implement ${settings.name}');
    return null;
  }

  Future<void> initDB() async {
    await ledgerRepository.init();
    setState(() {
      LogManager.info('done init db');
      hasInitialzed = true;
    });
  }
}

class InitializerWidget extends StatefulWidget {
  const InitializerWidget({Key? key}) : super(key: key);

  @override
  _InitializerWidgetState createState() => _InitializerWidgetState();
}

class _InitializerWidgetState extends State<InitializerWidget> {
  late final FirebaseAuth _auth;

  /*User? _user;

  bool isLoading = true;*/

  @override
  void initState() {
    super.initState();
    /*_auth = FirebaseAuth.instance;
    if (_auth.currentUser == null) {
      LogManager.info("Current User is null");
      _signUpScreenRoute();
    } else {
      LogManager.info("Current User is not null");
      _user = _auth.currentUser!;
      // ledgerRepository.doesUserExist(_user?.phoneNumber ?? "");
    }
// 357959
    isLoading = false;*/
  }

  @override
  Widget build(BuildContext context) {
    /*if (isLoading) {*/
    /*return const Scaffold(
        body: Center(child: CircularProgressIndicator()),
      );
    } else if (_user == null) {
      LogManager.info("Initializer::User is null");
      return _signUpScreenRoute();
    } else {
      LogManager.info("Initializer::User is not null");
      return FutureBuilder<bool>(
          future: ledgerRepository.doesUserExist(_user?.phoneNumber ?? ""),
          builder: (
            BuildContext context,
            AsyncSnapshot<bool> snapshot,
          ) {
            if (!snapshot.hasData) {
              LogManager.info("Initializer::snapshot.hasData is false");
              return const Scaffold(
                body: Center(child: CircularProgressIndicator()),
              );
            } else {
              LogManager.info("Initializer::snapshot.hasData is true");*/
    return _homePageRoute();
    /*}
          });
    }*/
// +966 58 093 4959
  }
}

BlocProvider<PhoneNumberCubit> _signUpScreenRoute() {
  return BlocProvider(
    create: (context) => PhoneNumberCubit(repository: ledgerRepository),
    child: const SignupScreen(),
  );
}

BlocProvider<PersonalAccountCubit> _homePageRoute() {
  return BlocProvider(
    create: (context) => PersonalAccountCubit(
      personalAccountReportUseCase:
          PersonalAccountReportUseCase(repository: ledgerRepository),
    ),
    child: const HomeScreen(),
  );
}

BlocProvider<ActivityCubit> activityScreen() {
  return BlocProvider(
    create: (context) => ActivityCubit(
      activityUseCase: ActivityUseCase(repository: ledgerRepository),
    ),
    child: const ActivityScreen(),
  );
}

BlocProvider<CustomersListingCubit> customersListingScreen(
  String customerName,
) {
  return BlocProvider(
    create: (context) => CustomersListingCubit(
      customersListingReportUseCase: CustomersListingReportUseCase(
        repository: ledgerRepository,
        appLocalizations: AppLocalizations.get(),
      ),
    ),
    child: CustomersListing(customerName: customerName),
  );
}

BlocProvider<PersonalAccountCubit> personalAccountScreen() {
  return BlocProvider(
    create: (context) => PersonalAccountCubit(
      personalAccountReportUseCase:
          PersonalAccountReportUseCase(repository: ledgerRepository),
    ),
    child: const PersonalAccountScreen(),
  );
}

final LedgerRepository ledgerRepository =
    inMemory ? InMemoryLedgerRepository() : RemoteLedgerRepository();

// print("$ledgerRepository");

bool inMemory = false;
