abstract class Logger {
  void info(String message);

  void debug(String message);

  void error(String message, Exception e);
}

class DefaultLogger extends Logger {
  @override
  void debug(String message) {
    print('${DateTime.now()} $message');
  }

  @override
  void error(String message, Exception e) {
    print('${DateTime.now()} $message $e');
  }

  @override
  void info(String message) {
    print('${DateTime.now()} $message');
  }
}

class LogManager {
  static final DefaultLogger _logger = DefaultLogger();

  static void info(String message) {
    _logger.info(message);
  }

  static void debug(String message) {
    _logger.debug(message);
  }

  static void error(String message, Exception e) {
    _logger.error(message, e);
  }
}