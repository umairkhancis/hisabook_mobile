import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splitwise/language_model.dart'  ;

class AppLocalizations extends IAppLocalizations {
  late Locale locale;

  late final LanguageModel selectedLanguage;

  AppLocalizations(this.locale);

  AppLocalizations.language(this.selectedLanguage) {
    locale = Locale(selectedLanguage.bundledFileCode);
  }

  // Helper method to keep the code in the widgets concise
  // Localizations are accessed using an InheritedWidget "of" syntax
  static AppLocalizations of(BuildContext context) {
    return SharedAppLocalizations._singleton.appLocalizations;
  }

  static AppLocalizations get() {
    return SharedAppLocalizations._singleton.appLocalizations;
  }

  // Static member to have a simple access to the delegate from the MaterialApp
  static const LocalizationsDelegate<AppLocalizations> delegate =
      _AppLocalizationsDelegate();

  late Map<String, String> _localizedStrings;

  Future<bool> load() async {
    // Load the language JSON file from the "lang" folder
    String jsonString =
        await rootBundle.loadString('i18n/${locale.languageCode}.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);
    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });

    return true;
  }

  Future<bool> synchronousLoad() async {
    ByteData data = await rootBundle.load('i18n/${locale.languageCode}.json');
    final buffer = data.buffer;
    var list = buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    String jsonString = utf8.decode(list);
    Map<String, dynamic> jsonMap = json.decode(jsonString);
    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });
    return true;
  }

  // This method will be called from every widget which needs a localized text
  @override
  String translate(String key) {
    return _localizedStrings[key] ?? "";
  }

  TextDirection direction() {
    if (selectedLanguage != null) {
      return selectedLanguage.isRTL ? TextDirection.rtl : TextDirection.ltr;
    } else {
      // Fallback in case of selectedLanguage does not exists.
      // TODO:Remove dependency on this fallback by making selectedLanguage as mandatory
      return locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl;
    }
  }

  String languageRemoteCode() {
    if (selectedLanguage != null) {
      return selectedLanguage.code;
    } else {
      // Fallback in case of selectedLanguage does not exists.
      // TODO:Remove dependency on this fallback by making selectedLanguage as mandatory
      return locale.languageCode == 'en' ? 'en-US' : 'ar-KW';
    }
  }
}

// LocalizationsDelegate is a factory for a set of localized resources
// In this case, the localized strings will be gotten in an AppLocalizations object
class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  // This delegate instance will never change (it doesn't even have fields!)
  // It can provide a constant constructor.
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    // Include all of your supported language codes here
    return ['en', 'ar', 'ckb'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    // AppLocalizations class is where the JSON loading actually runs
    AppLocalizations localizations = AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

class SharedAppLocalizations {
  static final SharedAppLocalizations _singleton =
      SharedAppLocalizations._internal();
  late AppLocalizations appLocalizations;

  void setAppLocalization(AppLocalizations localizations) {
    appLocalizations = localizations;
  }

  factory SharedAppLocalizations() {
    return _singleton;
  }

  SharedAppLocalizations._internal();
}


abstract class IAppLocalizations {
  String translate(String key);
}