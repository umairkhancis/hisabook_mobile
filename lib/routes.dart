class MyRoutes {
  static String homePageRoute = "/homePage";
  static String customerDetailsRoute = "/customerDetailScreen";
  static String addTransactionRoute = "/addTransaction";
  static String customerListingRoute = "/customerListing";
  static String transactionDetailsRoute = "/transactionDetails";
  static String addMoreCustomersRoute = "/addMoreCustomers";
  static String verifyContactInfoRoute = "/verifyContactInfo";
  static String selectTransactionScreenRoute = "/selectTransactionScreen";
  static String settleUpTransactionRoute = "/addSettleUpTransactionScreen";
  static String activityRoute = "/activityScreen";
  static String accountDetailsActionsRoute = "/accountDetailsActionsScreen";
  static String otpScreen = "/otpScreen";
  static String fullNameAndShopNameScreen = "/fullNameAndShopNameScreen";
}
